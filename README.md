# Utiliser SicpaOpenData dans un projet DotNET

![Logo SICPA](https://germinal.toulouse.inra.fr/~theirman/img/CC_BY_INRAE_CatiSicpa.jpg)

Ce projet consiste en la création d'une librairie au format DLL pour faciliter les déploiements de sets de données vers les portails d'ouverture des données de l'INRAE

## Prérequis :
-	La librairie `SicpaOpenData.dll` *(je considère dans cette doc qu’elle se trouve dans le dossier lib du projet)*
-	La librairie `Newtonsoft.Json` *(pour sérialiser les données en json)*

## Installer Newtonsoft.Json

Dans l’explorateur de projet :
-	Cliquer-droit sur **Références**
-	Cliquer sur **Gérer les packages NuGet**
-	Dans l’onglet Parcourir, rechercher **Newtonsoft.Json**
-	Cliquer sur **Installer**

Newtonsoft est installé dans Visual Studio et ajouté à votre projet

## Ajouter SicpaOpenData.dll au projet

Dans l’explorateur de projet :
-	Cliquer-droit sur **Références**
-	Cliquer sur **Ajouter une référence**
-	En bas de la fenêtre, cliquer sur le bouton **Parcourir**
-	Sélectionner **SicpaOpenData.dll** dans le dossier lib de votre projet
-	Cliquer sur **Ajouter**

## Utiliser SicpaOpenData.dll dans votre projet DotNet

```csharp
// utiliser l’API SicpaOpenData.API
using SicpaOpenData.API;
// utiliser l’API SicpaOpenData.Metadata
using SicpaOpenData.Metadata;
```

Et pour la suite, consulter la documentation technique de l’API [ici](https://germinal.toulouse.inra.fr/~theirman/SicpaOpenData/DotNET/index.html)
