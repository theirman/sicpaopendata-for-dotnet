﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Avril 2021</li>
    ///         <li>Description : Classe détaillant les vocabulaires contrôlés</li>
    ///     </ul>
    /// </summary>
    public static class ControlledVocabulary
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          





        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             





        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //



        /// <summary><c>GetControlledVocabularies</c> est une méthode qui permet d'obtenir la liste des vocabulaires contrôlés</summary>
        /// <returns>une hashtable contenant la liste des vocabulaires contrôlés</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable cv = ControlledVocabulary.GetControlledVocabularies();
        ///     </code>
        /// </example>
        public static Hashtable GetControlledVocabularies()
        {
            Hashtable jsonContent   = new Hashtable();
            String url              = Properties.Settings.Default.controlledVocabularyWS;
            String response         = ControlledVocabulary.GetFromWS(url);

            foreach (DictionaryEntry categoryNode in JsonConvert.DeserializeObject<Hashtable>(response))
            {
                if (categoryNode.Value.GetType() == typeof(String)) continue;

                foreach (KeyValuePair<String, ArrayList> cvNode in JsonConvert.DeserializeObject<Dictionary<String, ArrayList>>(categoryNode.Value.ToString()))
                    jsonContent.Add(cvNode.Key, cvNode.Value);
            }

            return jsonContent;
        }

        private static String GetFromWS(String url)
        {
            String response = String.Empty;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json")); 
                var httpResponse = client.GetAsync(url).Result;

                if (httpResponse.IsSuccessStatusCode)
                   response = Task.Run(() => httpResponse.Content.ReadAsStringAsync()).Result;
            }

            return response;
        }


        /// <summary><c>ListValuesFor</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour un noeud donné</summary>
        /// <param name="node">le noeud pour lequel recupéré les valeurs autorisées</param>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesFor("language");
        ///     </code>
        /// </example>
        public static ArrayList ListValuesFor(String node)
        {
            String url = Properties.Settings.Default.controlledVocabularyWS + "/listValuesFor?key=" + node;
            String response = ControlledVocabulary.GetFromWS(url);
            return JsonConvert.DeserializeObject<ArrayList>(response);
        }

        /// <summary><c>ListValuesForAuthorIdentifierSchemes</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'authorIdentifierScheme'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForAuthorIdentifierSchemes();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForAuthorIdentifierSchemes()
        {
            return ControlledVocabulary.ListValuesFor("authorIdentifierScheme");
        }

        /// <summary><c>ListValuesForCollectionMode</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'collectionMode'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForCollectionMode();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForCollectionMode()
        {
            return ControlledVocabulary.ListValuesFor("authorIdentifierScheme");
        }

        /// <summary><c>ListValuesForContributorIdentifierScheme</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'contributorIdentifierScheme'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForContributorIdentifierScheme();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForContributorIdentifierScheme()
        {
            return ControlledVocabulary.ListValuesFor("contributorIdentifierScheme");
        }

        /// <summary><c>ListValuesForContributorType</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'contributorType'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForContributorType();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForContributorType()
        {
            return ControlledVocabulary.ListValuesFor("contributorType");
        }

        /// <summary><c>ListValuesForCountry</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'country'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForCountry();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForCountry()
        {
            return ControlledVocabulary.ListValuesFor("country");
        }

        /// <summary><c>ListValuesForDataOrigin</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'dataOrigin'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForDataOrigin();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForDataOrigin()
        {
            return ControlledVocabulary.ListValuesFor("dataOrigin");
        }

        /// <summary><c>ListValuesForDegree</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'degree'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForDegree();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForDegree()
        {
            return ControlledVocabulary.ListValuesFor("degree");
        }

        /// <summary><c>ListValuesForDesignForOntologyTask</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'designedForOntologyTask'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForDesignForOntologyTask();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForDesignForOntologyTask()
        {
            return ControlledVocabulary.ListValuesFor("designedForOntologyTask");
        }

        /// <summary><c>ListValuesForHasFormalityLevel</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'hasFormalityLevel'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForHasFormalityLevel();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForHasFormalityLevel()
        {
            return ControlledVocabulary.ListValuesFor("hasFormalityLevel");
        }

        /// <summary><c>ListValuesForHasOntologyLanguage</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'hasOntologyLanguage'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForHasOntologyLanguage();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForHasOntologyLanguage()
        {
            return ControlledVocabulary.ListValuesFor("hasOntologyLanguage");
        }

        /// <summary><c>ListValuesForJournalArticleType</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'journalArticleType'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForJournalArticleType();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForJournalArticleType()
        {
            return ControlledVocabulary.ListValuesFor("journalArticleType");
        }

        /// <summary><c>ListValuesForKindOfData</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'kindOfData'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForKindOfData();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForKindOfData()
        {
            return ControlledVocabulary.ListValuesFor("kindOfData");
        }

        /// <summary><c>ListValuesForLanguage</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'language'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForLanguage();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForLanguage()
        {
            return ControlledVocabulary.ListValuesFor("language");
        }

        /// <summary><c>ListValuesForLifeCycleStep</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'lifeCycleStep'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForLifeCycleStep();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForLifeCycleStep()
        {
            return ControlledVocabulary.ListValuesFor("lifeCycleStep");
        }

        /// <summary><c>ListValuesForPublicationIDType</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'publicationIDType'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForPublicationIDType();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForPublicationIDType()
        {
            return ControlledVocabulary.ListValuesFor("publicationIDType");
        }

        /// <summary><c>ListValuesForRelatedDatasetIDType</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'relatedDatasetIDType'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForRelatedDatasetIDType();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForRelatedDatasetIDType()
        {
            return ControlledVocabulary.ListValuesFor("relatedDatasetIDType");
        }

        /// <summary><c>ListValuesForSamplingProcedure</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'samplingProcedure'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForSamplingProcedure();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForSamplingProcedure()
        {
            return ControlledVocabulary.ListValuesFor("samplingProcedure");
        }

        /// <summary><c>ListValuesForStudyAssayMeasurementType</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayMeasurementType'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForStudyAssayMeasurementType();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForStudyAssayMeasurementType()
        {
            return ControlledVocabulary.ListValuesFor("studyAssayMeasurementType");
        }

        /// <summary><c>ListValuesForStudyAssayOrganism</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayOrganism'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForStudyAssayOrganism();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForStudyAssayOrganism()
        {
            return ControlledVocabulary.ListValuesFor("studyAssayOrganism");
        }

        /// <summary><c>ListValuesForStudyAssayPlatform</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayPlatform'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForStudyAssayPlatform();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForStudyAssayPlatform()
        {
            return ControlledVocabulary.ListValuesFor("studyAssayPlatform");
        }

        /// <summary><c>ListValuesForStudyAssayTechnologyType</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayTechnologyType'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForStudyAssayTechnologyType();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForStudyAssayTechnologyType()
        {
            return ControlledVocabulary.ListValuesFor("studyAssayTechnologyType");
        }

        /// <summary><c>ListValuesForStudyDesignType</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyDesignType'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForStudyDesignType();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForStudyDesignType()
        {
            return ControlledVocabulary.ListValuesFor("studyDesignType");
        }

        /// <summary><c>ListValuesForStudyFactorType</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyFactorType'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForStudyFactorType();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForStudyFactorType()
        {
            return ControlledVocabulary.ListValuesFor("studyFactorType");
        }

        /// <summary><c>ListValuesForSubject</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'subject'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForSubject();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForSubject()
        {
            return ControlledVocabulary.ListValuesFor("subject");
        }

        /// <summary><c>ListValuesForTimeMethod</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'timeMethod'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForTimeMethod();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForTimeMethod()
        {
            return ControlledVocabulary.ListValuesFor("timeMethod");
        }

        /// <summary><c>ListValuesForTypeOfSR</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'typeOfSR'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForTypeOfSR();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForTypeOfSR()
        {
            return ControlledVocabulary.ListValuesFor("typeOfSR");
        }

        /// <summary><c>ListValuesForUnitOfAnalysis</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'unitOfAnalysis'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForUnitOfAnalysis();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForUnitOfAnalysis()
        {
            return ControlledVocabulary.ListValuesFor("unitOfAnalysis");
        }

        /// <summary><c>ListValuesForVersionStatus</c> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'versionStatus'</summary>
        /// <returns>la liste de valeurs autorisées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         ArrayList values = ControlledVocabulary.ListValuesForVersionStatus();
        ///     </code>
        /// </example>
        public static ArrayList ListValuesForVersionStatus()
        {
            return ControlledVocabulary.ListValuesFor("versionStatus");
        }
    }
}
