﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Linq;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Mai 2021</li>
    ///         <li>Description : Classe implémentant le node socialscience</li>
    ///     </ul>
    /// </summary>
    public class SocialScienceNode : CategoryNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  


        /// <summary>Cette propriété recense la liste des fields requis pour ce node</summary>
        /// <value>Hashtable</value>
        private readonly Hashtable authorizedFields;

        /// <summary>Cette propriété recense la liste des fields requis pour ce node</summary>
        /// <value>Hashtable</value>
        private readonly Hashtable requiredFields;



        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /// <summary>Constructeur ne nécessitant pas de paramètres</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SocialScienceNode ssn = new SocialScienceNode();
        ///     </code>
        /// </example>
        public SocialScienceNode()
        {
            this.DisplayName = "Social Science and Humanities Metadata";
            this.Fields = new ArrayList();
            this.authorizedFields = AuthorizedFields.ListAuthorizedFieldsForSocialScienceNode();
            this.requiredFields = RequiredFields.ListRequiredFieldsForSocialScienceNode();
        }




        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /// <summary><c>AddField</c> est une méthode qui permet d'ajouter un noeud de type MultipleCompoundNode à la liste des fields du node</summary>
        /// <param name="node">node à ajouter</param>
        /// Voir <see cref="SicpaOpenData.Metadata.MultipleCompoundNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable value1 = new Hashtable();
        ///         value1.Add("name11", new SimplePrimitiveNode("name11", "value11"));
        ///         value1.Add("name12", new SimplePrimitiveNode("name12", "value12"));
        /// 
        ///         Hashtable value2 = new Hashtable();
        ///         value2.Add("name21", new SimplePrimitiveNode("name21", "value21"));
        ///         value2.Add("name22", new SimplePrimitiveNode("name22", "value22"));
        ///         
        ///         ArrayList values = new ArrayList() { value1, value2 };
        ///         
        ///         SocialScienceNode ssn = new SocialScienceNode();
        ///         ssn.AddField(new MultipleCompoundNode("name", values));
        ///     </code>
        /// </example>
        public Boolean AddField(MultipleCompoundNode node)
        {
            if (!node.IsValid() || !this.authorizedFields.ContainsKey(node.TypeName) || this.authorizedFields[node.TypeName].ToString() != "MultipleCompoundNode")
                return false;

            this.Fields.Add(node);
            return this.Fields.Contains(node);
        }

        /// <summary><c>AddField</c> est une méthode qui permet d'ajouter un noeud de type MultipleControlledVocabularyNode à la liste des fields du node</summary>
        /// <param name="node">node à ajouter</param>
        /// Voir <see cref="SicpaOpenData.Metadata.MultipleControlledVocabularyNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         List<String> values = new List<String>();
        ///         values.Add("controlledValue1");
        ///         values.Add("controlledValue2");
        ///         values.Add("controlledValue3");
        ///         
        ///         SocialScienceNode ssn = new SocialScienceNode();
        ///         ssn.AddField(new MultipleControlledVocabularyNode("name", values));
        ///     </code>
        /// </example>
        public Boolean AddField(MultipleControlledVocabularyNode node)
        {
            if (!node.IsValid() || !this.authorizedFields.ContainsKey(node.TypeName) || this.authorizedFields[node.TypeName].ToString() != "MultipleControlledVocabularyNode")
                return false;

            this.Fields.Add(node);
            return this.Fields.Contains(node);
        }

        /// <summary><c>AddField</c> est une méthode qui permet d'ajouter un noeud de type MultiplePrimitiveNode à la liste des fields du node</summary>
        /// <param name="node">node à ajouter</param>
        /// Voir <see cref="SicpaOpenData.Metadata.MultiplePrimitiveNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         List<String> values = new List<String>();
        ///         values.Add("value1");
        ///         values.Add("value2");
        ///         values.Add("value3");
        ///         
        ///         SocialScienceNode ssn = new SocialScienceNode();
        ///         ssn.AddField(new MultiplePrimitiveNode("name", values));
        ///     </code>
        /// </example>
        public Boolean AddField(MultiplePrimitiveNode node)
        {
            if (!node.IsValid() || !this.authorizedFields.ContainsKey(node.TypeName) || this.authorizedFields[node.TypeName].ToString() != "MultiplePrimitiveNode")
                return false;

            this.Fields.Add(node);
            return this.Fields.Contains(node);
        }

        /// <summary><c>AddField</c> est une méthode qui permet d'ajouter un noeud de type SimpleCompoundNode à la liste des fields du node</summary>
        /// <param name="node">node à ajouter</param>
        /// Voir <see cref="SicpaOpenData.Metadata.SimpleCompoundNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable ht = new Hashtable();
        ///         ht.Add("field1", new SimplePrimitiveNode("field1", "value1"));
        ///         ht.Add("field2", new SimpleControlledVocabularyNode("field2", "value2"));
        ///         ht.Add("field3", new SimplePrimitiveNode("field3", "value3"));
        ///         
        ///         SocialScienceNode ssn = new SocialScienceNode();
        ///         ssn.AddField(new SimpleCompoundNode("name", ht));
        ///     </code>
        /// </example>
        public Boolean AddField(SimpleCompoundNode node)
        {
            if (!node.IsValid() || !this.authorizedFields.ContainsKey(node.TypeName) || this.authorizedFields[node.TypeName].ToString() != "SimpleCompoundNode")
                return false;

            this.Fields.Add(node);
            return this.Fields.Contains(node);
        }

        /// <summary><c>AddField</c> est une méthode qui permet d'ajouter un noeud de type SimpleControlledVocabularyNode à la liste des fields du node</summary>
        /// <param name="node">node à ajouter</param>
        /// Voir <see cref="SicpaOpenData.Metadata.SimpleControlledVocabularyNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SocialScienceNode ssn = new SocialScienceNode();
        ///         ssn.AddField(new SimpleControlledVocabularyNode("name", "controlledValue"));
        ///     </code>
        /// </example>
        public Boolean AddField(SimpleControlledVocabularyNode node)
        {
            if (!node.IsValid() || !this.authorizedFields.ContainsKey(node.TypeName) || this.authorizedFields[node.TypeName].ToString() != "SimpleControlledVocabularyNode")
                return false;

            this.Fields.Add(node);
            return this.Fields.Contains(node);
        }

        /// <summary><c>AddField</c> est une méthode qui permet d'ajouter un noeud de type SimplePrimitiveNode à la liste des fields du node</summary>
        /// <param name="node">node à ajouter</param>
        /// Voir <see cref="SicpaOpenData.Metadata.SimplePrimitiveNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SocialScienceNode ssn = new SocialScienceNode();
        ///         ssn.AddField(new SimplePrimitiveNode("name", "value"));
        ///     </code>
        /// </example>
        public Boolean AddField(SimplePrimitiveNode node)
        {
            if (!node.IsValid() || !this.authorizedFields.ContainsKey(node.TypeName) || this.authorizedFields[node.TypeName].ToString() != "SimplePrimitiveNode")
                return false;

            this.Fields.Add(node);
            return this.Fields.Contains(node);
        }

        /// <summary><c>IsValid</c> est une méthode qui permet de s'assurer de la validité du node</summary>
        /// <returns>true si le node est valide, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(ssn.IsValid())
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        public Boolean IsValid()
        {
            Boolean isRequiredPresent = true;
            Boolean isPresentAuthorized = true;

            foreach (DictionaryEntry de in this.requiredFields)
            {
                if (!this.FieldNames().Contains(de.Key))
                {
                    isRequiredPresent = false;
                    break;
                }
            }

            foreach (BaseNode field in this.Fields)
            {
                if (!this.authorizedFields.ContainsKey(field.TypeName))
                {
                    isPresentAuthorized = false;
                    break;
                }

                if (!field.GetType().ToString().Contains(this.authorizedFields[field.TypeName].ToString()))
                {
                    isPresentAuthorized = false;
                    break;
                }
            }

            return this.DisplayName == "Social Science and Humanities Metadata" && isRequiredPresent && isPresentAuthorized;
        }

        /// <summary><c>ToJSON</c> est une méthode qui permet d'obtenir une réprésentation JSON du node</summary>
        /// <returns>la représentation du node sous forme de chaine JSON</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String json = ssn.ToJSON();
        ///     </code>
        /// </example>
        public String ToJSON()
        {
            if (!this.IsValid())
                return String.Empty;

            return JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }

        /// <summary><c>ToString</c> est une méthode qui permet d'obtenir une réprésentation textuelle du node (JSON minifié)</summary>
        /// <returns>la représentation du node sous forme de chaine JSON minifiée</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String nodeString = ssn.ToString();
        ///     </code>
        /// </example>
        override public String ToString()
        {
            if (!this.IsValid())
                return String.Empty;

            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }





        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //



    }
}
