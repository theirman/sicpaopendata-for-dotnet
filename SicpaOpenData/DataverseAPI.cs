﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Text;



namespace SicpaOpenData.API
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Juin 2021</li>
    ///         <li>Description : Classe implémentant les méthodes d'accès aux dataverses INRAE</li>
    ///     </ul>
    /// </summary>
    public static class DataverseAPI
    {
        /// <summary><c>GetClientWithHeaders</c> est une méthode qui permet de formaliser les headers HTTP de la requête HTTP</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <returns>un client de connexion HTTP</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///
        ///         HttpClient client = DataverseAPI.GetClientWithHeaders(adresseServeur, jetonAPI);
        ///     </code>
        /// </example>
        private static HttpClient GetClientWithHeaders(String serverBaseURI, String apiToken)
        {
            HttpClientHandler handler = new HttpClientHandler() { UseDefaultCredentials = false };
            HttpClient client = new HttpClient(handler);

            try
            {
                client.BaseAddress = new Uri(serverBaseURI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-Dataverse-key", apiToken);
            }
            catch (Exception e)
            {
                throw e;
            }

            return client;
        }

        /// <summary><c>GetDatasetContents</c> est une méthode qui permet de lister le contenu d'un dataset sous la forme d'une trame JSON</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="doi">DOI ciblé</param>
        /// <returns>trame JSON détaillant le contenu du dataset</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String doi = "doi:10.12345/ABCDEF";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         
        ///         String contenuDataset = DataverseAPI.GetDatasetContents(adresseServeur, jetonAPI, doi);
        ///     </code>
        /// </example>
        public static async Task<String> GetDatasetContents(String serverBaseURI, String apiToken, String doi)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je déclare et initialise mon endpoint
            String endpoint = "/api/datasets/:persistentId/?persistentId={PERSISTENT_IDENTIFIER}";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{PERSISTENT_IDENTIFIER}", doi);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.GetAsync(endpoint);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }

        /// <summary><c>GetDatasetFiles</c> est une méthode qui permet de</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="datasetID">identifiant numérique du dataset</param>
        /// <param name="datasetVersion">numéro de version du dataset</param>
        /// <returns>trame JSON listant les fichiers du dataset</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         String idDataset = "123456";
        ///         String versionDataset = "3.0";
        ///         
        ///         String listeFichiers = DataverseAPI.GetDatasetFiles(adresseServeur, jetonAPI, idDataset, versionDataset);
        ///     </code>
        /// </example>
        public static async Task<String> GetDatasetFiles(String serverBaseURI, String apiToken, String datasetID, String datasetVersion)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je déclare et initialise mon endpoint
            String endpoint = "/api/datasets/{DATASET_ID}/versions/{DATASET_VERSION}/files";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{DATASET_ID}", datasetID)
                               .Replace("{DATASET_VERSION}", datasetVersion);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.GetAsync(endpoint);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }

        /// <summary><c>GetDatasetMetadataExport</c> est une méthode qui permet de</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="doi">DOI ciblé</param>
        /// <param name="metadataFormat">format des métadonnées</param>
        /// <returns>trame JSON détaillant les métadonnées dans le format démandé</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         String doi = "doi:10.12345/ABCDEF";
        ///         String formatMetadonnee = "Datacite";
        ///         
        ///         String metadonnees = DataverseAPI.GetDatasetMetadataExport(adresseServeur, jetonAPI, doi, formatMetadonnee);
        ///     </code>
        /// </example>
        public static async Task<String> GetDatasetMetadataExport(String serverBaseURI, String apiToken, String doi, String metadataFormat)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je déclare et initialise mon endpoint
            String endpoint = "/api/datasets/export?exporter={METADATA_FORMAT}&persistentId={PERSISTENT_IDENTIFIER}";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{PERSISTENT_IDENTIFIER}", doi)
                               .Replace("{METADATA_FORMAT}", metadataFormat);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.GetAsync(endpoint);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }

        /// <summary><c>GetDatasetVersion</c> est une méthode qui permet de</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="datasetID">identifiant numérique du dataset</param>
        /// <param name="datasetVersion">numéro de version du dataset</param>
        /// <returns>trame JSON détaillant le contenu d'une version du dataset</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         String idDataset = "123456";
        ///         String versionDataset = "3.0";
        ///         
        ///         String versionDataset = DataverseAPI.GetDatasetVersion(adresseServeur, jetonAPI, idDataset, versionDataset);
        ///     </code>
        /// </example>
        public static async Task<String> GetDatasetVersion(String serverBaseURI, String apiToken, String datasetID, String datasetVersion)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je déclare et initialise mon endpoint
            String endpoint = "/api/datasets/{DATASET_ID}/versions/{DATASET_VERSION}";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{DATASET_ID}", datasetID)
                               .Replace("{DATASET_VERSION}", datasetVersion);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.GetAsync(endpoint);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }

        /// <summary><c>GetDatasetVersionList</c> est une méthode qui permet de</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="datasetID">identifiant numérique du dataset</param>
        /// <returns>trame JSON listant toutes les versions du dataset</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         String idDataset = "123456";
        ///         
        ///         String listeVersions = DataverseAPI.GetDatasetVersionList(adresseServeur, jetonAPI, idDataset);
        ///     </code>
        /// </example>
        public static async Task<String> GetDatasetVersionList(String serverBaseURI, String apiToken, String datasetID)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je déclare et initialise mon endpoint
            String endpoint = "/api/datasets/{DATASET_ID}/versions";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{DATASET_ID}", datasetID);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.GetAsync(endpoint);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }

        /// <summary><c>GetDataverseContents</c> est une méthode qui permet de</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="dataverseID">identifiant du dataverse</param>
        /// <returns>trame JSON détaillant le contenu du dataverse</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         String idDataverse = "INRAE Dataverse";
        ///         
        ///         String contenuDataverse = DataverseAPI.GetDataverseContents(adresseServeur, jetonAPI, idDataverse);
        ///     </code>
        /// </example>
        public static async Task<String> GetDataverseContents(String serverBaseURI, String apiToken, String dataverseID)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je déclare et initialise mon endpoint
            String endpoint = "/api/dataverses/{DATAVERSE_ID}/contents";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{DATAVERSE_ID}", dataverseID);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.GetAsync(endpoint);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }

        /// <summary><c>GetDataverseInfos</c> est une méthode qui permet de</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="dataverseID">identifiant du dataverse</param>
        /// <returns>trame JSON listant les infos concernant un dataverse</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         String idDataverse = "INRAE Dataverse";
        ///         
        ///         String infosDataverse = DataverseAPI.GetDataverseInfos(adresseServeur, jetonAPI, idDataverse);
        ///     </code>
        /// </example>
        public static async Task<String> GetDataverseInfos(String serverBaseURI, String apiToken, String dataverseID)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je déclare et initialise mon endpoint
            String endpoint = "/api/dataverses/{DATAVERSE_ID}";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{DATAVERSE_ID}", dataverseID);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.GetAsync(endpoint);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }

        /// <summary><c>GetDataverseMetadata</c> est une méthode qui permet de</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="dataverseID">identifiant du dataverse</param>
        /// <returns>trame JSON listant les métadonnées d'un dataverse</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         String idDataverse = "INRAE Dataverse";
        ///         
        ///         String metadonnees = DataverseAPI.GetDataverseMetadata(adresseServeur, jetonAPI, idDataverse);
        ///     </code>
        /// </example>
        public static async Task<String> GetDataverseMetadata(String serverBaseURI, String apiToken, String dataverseID)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je déclare et initialise mon endpoint
            String endpoint = "/api/dataverses/{DATAVERSE_ID}/metadatablocks";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{DATAVERSE_ID}", dataverseID);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.GetAsync(endpoint);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }

        /// <summary><c>PostDatasetAddFile</c> est une méthode qui permet de</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="doi">DOI ciblé</param>
        /// <param name="filePath">chemin vers le fichier à uploader</param>
        /// <returns>trame JSON détaillant le succès/l'échec de l'upload de fichier</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String doi = "doi:10.12345/ABCDEF";
        ///         String cheminFichier = @"c:\temp\fichier.csv";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         
        ///         String status = DataverseAPI.PostDatasetAddFile(adresseServeur, jetonAPI, doi, cheminFichier);
        ///     </code>
        /// </example>
        public static async Task<String> PostDatasetAddFile(String serverBaseURI, String apiToken, String doi, String filePath)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je crée le formulaire HTTP
            MultipartFormDataContent form = new MultipartFormDataContent();

            // je lis le contenu du fichier
            ByteArrayContent fileContent = new ByteArrayContent(File.ReadAllBytes(filePath));

            // j'ajoute le fichier au formulaire HTTP
            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
            form.Add(fileContent, "file", Path.GetFileName(filePath));

            // je déclare et initialise mon endpoint
            String endpoint = "/api/datasets/:persistentId/add?persistentId={PERSISTENT_IDENTIFIER}";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{PERSISTENT_IDENTIFIER}", doi);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.PostAsync(endpoint, form);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }

        /// <summary><c>PostDatasetCreate</c> est une méthode qui permet de</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="dataverseID">identifiant du dataverse</param>
        /// <param name="metadataJSON">métadonnées du dataset au format JSON</param>
        /// <returns>trame JSON détaillant le succès/l'échec de la création du dataset</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         String idDataverse = "INRAE Dataverse";
        ///         String metadata = File.ReadAllText(@"c:\temp\metadata.json", Encoding.UTF8);
        ///         
        ///         String status = DataverseAPI.PostDatasetCreate(adresseServeur, jetonAPI, idDataverse, metadata);
        ///     </code>
        /// </example>
        public static async Task<String> PostDatasetCreate(String serverBaseURI, String apiToken, String dataverseID, String metadataJSON)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je déclare et initialise mon endpoint
            String endpoint = "/api/dataverses/{DATAVERSE_ID}/datasets";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{DATAVERSE_ID}", dataverseID);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // je crée le contenu HTTP
                HttpContent content = new StringContent(metadataJSON, Encoding.UTF8);

                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.PostAsync(endpoint, content);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }

        /// <summary><c>PostDatasetPublish</c> est une méthode qui permet de</summary>
        /// <param name="serverBaseURI">adresse du serveur</param>
        /// <param name="apiToken">jeton Api de l'utilisateur</param>
        /// <param name="doi">DOI ciblé</param>
        /// <param name="versionType">le type de version {minor | major}</param>
        /// <returns>trame JSON détaillant le succès/l'échec de la publication du dataset</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String adresseServeur   = "http://localhost:57170";
        ///         String doi = "doi:10.12345/ABCDEF";
        ///         String jetonAPI = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
        ///         String typeVersion = "minor";
        ///         
        ///         String status = DataverseAPI.PostDatasetPublish(adresseServeur, jetonAPI, doi, typeVersion);
        ///     </code>
        /// </example>
        public static async Task<String> PostDatasetPublish(String serverBaseURI, String apiToken, String doi, String versionType)
        {
            // je déclare et initialise ma valeur retour
            String response = String.Empty;

            // je déclare et initialise mon endpoint
            String endpoint = "/api/datasets/:persistentId/actions/:publish?persistentId={PERSISTENT_IDENTIFIER}&type={VERSION_TYPE}";

            // je met à jour le endpoint avec le dataverse visé
            endpoint = endpoint.Replace("{PERSISTENT_IDENTIFIER}", doi)
                               .Replace("{VERSION_TYPE}", versionType);

            //je crée le client HTTP
            using (HttpClient client = DataverseAPI.GetClientWithHeaders(serverBaseURI, apiToken))
            {
                // j'appelle ma méthode GET en utilisant le endpoint mis à jour avec le dataverse visé
                HttpResponseMessage httpResponse = await client.PostAsync(endpoint, null);

                // je traite la réponse du webservice et la retourne
                return await httpResponse.Content.ReadAsStringAsync();
            }
        }
    }
}
