﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Avril 2021</li>
    ///         <li>Description : Classe implémentant les fonctionnalités support aux autres classes</li>
    ///     </ul>
    /// </summary>
    public static class Helper
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          





        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             





        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //

        /// <summary><c>IsJson</c> est une méthode qui permet de vérifier qu'une chaine contient bien du JSON</summary>
        /// <param name="str">la chaine à analyser</param>
        /// <returns>true si la contient du JSON, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(IsJson(chaineATester))
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        public static bool IsJson(String str)
        {
            try
            {
                JToken.Parse(str);
                return true;
            }
            catch (JsonReaderException)
            {
                return false;
            }
        }



        /// <summary><c>MergeHashtables</c> est une méthode qui permet de fusionner une hashtable dans une autre</summary>
        /// <param name="hashtable1">la hashtable dans laquelle ajouter les valeurs de <c>hashtable2</c></param>
        /// <param name="hashtable2">la hashtable contenant les valeurs à ajouter dans <c>hashtable1</c></param>
        /// <returns>retourne une Hashtable contenant les 2 hashtables fusionnées</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable newHashtable = Helper.MergeHashtables(hashtable1, hashtable2);
        ///     </code>
        /// </example>
        public static Hashtable MergeHashtables(Hashtable hashtable1, Hashtable hashtable2)
        {
            foreach (DictionaryEntry de in hashtable2)
                if (!hashtable1.ContainsKey(de.Key))
                    hashtable1.Add(de.Key, de.Value);

            return hashtable1;
        }
    }
}
