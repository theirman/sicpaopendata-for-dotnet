﻿using System;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Mars 2021</li>
    ///         <li>Description : Classe implémentant les nodes de base</li>
    ///     </ul>
    /// </summary>
    public abstract class BaseNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /// <summary>Cette propriété indique si l'on est en présence d'un noeud mono- ou multi-valeur(s)</summary>
        /// <value>Boolean</value>
        public Boolean Multiple { get; protected set; }

        /// <summary>Cette propriété indique le type de la valeur attendue</summary>
        /// <value>String</value>
        public String TypeClass { get; protected set; }

        /// <summary>Cette propriété identifie la valeur</summary>
        /// <value>String</value>
        public String TypeName { get; set; }
    }
}
