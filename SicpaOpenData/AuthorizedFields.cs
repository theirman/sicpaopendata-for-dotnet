﻿using System;
using System.Collections;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Mars 2021</li>
    ///         <li>Description : Classe détaillant les valeurs autorisées par nodes</li>
    ///     </ul>
    /// </summary>
    public static class AuthorizedFields
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          





        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             





        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //

        /// <summary><c>GetNodeType</c> est une méthode qui permet de récupérer le type d'un node</summary>
        /// <param name="node">le node pour lequel on souhaite récupérer le type</param>
        /// <returns>le type de node</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String nodeType = AuthorizedFields.GetNodeType("author");
        ///     </code>
        /// </example>
        public static String GetNodeType(String node)
        {
            switch(node)            
            {
                case "accessToSources":                     return "SimplePrimitiveNode";
                case "actionsToMinimizeLoss":               return "SimplePrimitiveNode";
                case "ageOfSource":                         return "MultiplePrimitiveNode";
                case "alternativeTitle":                    return "SimplePrimitiveNode";
                case "alternativeURL":                      return "SimplePrimitiveNode";
                case "author":                              return "MultipleCompoundNode";
                case "authorAffiliation":                   return "SimplePrimitiveNode";
                case "authorIdentifier":                    return "SimplePrimitiveNode";
                case "authorIdentifierScheme":              return "SimpleControlledVocabularyNode";
                case "authorName":                          return "SimplePrimitiveNode";
                case "bugDatabase":                         return "SimplePrimitiveNode";
                case "changes":                             return "SimplePrimitiveNode";
                case "characteristicOfSources":             return "SimplePrimitiveNode";
                case "citations":                           return "MultiplePrimitiveNode";
                case "city":                                return "SimplePrimitiveNode";
                case "cleaningOperations":                  return "SimplePrimitiveNode";
                case "collectionMode":                      return "SimpleControlledVocabularyNode";
                case "collectionModeOther":                 return "SimplePrimitiveNode";
                case "collectorTraining":                   return "SimplePrimitiveNode";
                case "conformity":                          return "MultipleCompoundNode";
                case "contributor":                         return "MultipleCompoundNode";
                case "contributorAffiliation":              return "SimplePrimitiveNode";
                case "contributorIdentifier":               return "SimplePrimitiveNode";
                case "contributorIdentifierScheme":         return "SimpleControlledVocabularyNode";
                case "contributorName":                     return "SimplePrimitiveNode";
                case "contributorType":                     return "SimpleControlledVocabularyNode";
                case "controlOperations":                   return "SimplePrimitiveNode";
                case "country":                             return "SimpleControlledVocabularyNode";
                case "dataCollectionSituation":             return "SimplePrimitiveNode";
                case "dataCollector":                       return "SimplePrimitiveNode";
                case "dataOrigin":                          return "MultipleControlledVocabularyNode";
                case "datasetContact":                      return "MultipleCompoundNode";
                case "datasetContactAffiliation":           return "SimplePrimitiveNode";
                case "datasetContactEmail":                 return "SimplePrimitiveNode";
                case "datasetContactName":                  return "SimplePrimitiveNode";
                case "datasetLevelErrorNotes":              return "SimplePrimitiveNode";
                case "dataSources":                         return "MultiplePrimitiveNode";
                case "dateOfCollection":                    return "MultipleCompoundNode";
                case "dateOfCollectionEnd":                 return "SimplePrimitiveNode";
                case "dateOfCollectionStart":               return "SimplePrimitiveNode";
                case "dateOfDeposit":                       return "SimplePrimitiveNode";
                case "degree":                              return "SimpleControlledVocabularyNode";
                case "depositor":                           return "SimplePrimitiveNode";
                case "designedForOntologyTask":             return "MultipleControlledVocabularyNode";
                case "deviationsFromSampleDesign":          return "SimplePrimitiveNode";
                case "distributionDate":                    return "SimplePrimitiveNode";
                case "distributor":                         return "MultipleCompoundNode";
                case "distributorAbbreviation":             return "SimplePrimitiveNode";
                case "distributorAffiliation":              return "SimplePrimitiveNode";
                case "distributorLogoURL":                  return "SimplePrimitiveNode";
                case "distributorName":                     return "SimplePrimitiveNode";
                case "distributorURL":                      return "SimplePrimitiveNode";
                case "dsDescription":                       return "MultipleCompoundNode";
                case "dsDescriptionDate":                   return "SimplePrimitiveNode";
                case "dsDescriptionValue":                  return "SimplePrimitiveNode";
                case "eastLongitude":                       return "SimplePrimitiveNode";
                case "endpoint":                            return "SimplePrimitiveNode";
                case "experimentNumber":                    return "MultiplePrimitiveNode";
                case "frequencyOfDataCollection":           return "SimplePrimitiveNode";
                case "geographicalReferential":             return "MultipleCompoundNode";
                case "geographicBoundingBox":               return "MultipleCompoundNode";
                case "geographicCoverage":                  return "MultipleCompoundNode";
                case "geographicUnit":                      return "MultiplePrimitiveNode";
                case "grantNumber":                         return "MultipleCompoundNode";
                case "grantNumberAgency":                   return "SimplePrimitiveNode";
                case "grantNumberValue":                    return "SimplePrimitiveNode";
                case "hasFormalityLevel":                   return "SimpleControlledVocabularyNode";
                case "hasOntologyLanguage":                 return "MultipleControlledVocabularyNode";
                case "imports":                             return "MultiplePrimitiveNode";
                case "journalArticleType":                  return "SimpleControlledVocabularyNode";
                case "journalIssue":                        return "SimplePrimitiveNode";
                case "journalPubDate":                      return "SimplePrimitiveNode";
                case "journalVolume":                       return "SimplePrimitiveNode";
                case "journalVolumeIssue":                  return "MultipleCompoundNode";
                case "keyword":                             return "MultipleCompoundNode";
                case "keywordTermURI":                      return "SimplePrimitiveNode";
                case "keywordValue":                        return "SimplePrimitiveNode";
                case "keywordVocabulary":                   return "SimplePrimitiveNode";
                case "keywordVocabularyURI":                return "SimplePrimitiveNode";
                case "kindOfData":                          return "MultipleControlledVocabularyNode";
                case "kindOfDataOther":                     return "MultiplePrimitiveNode";
                case "knownUsage":                          return "SimplePrimitiveNode";
                case "language":                            return "MultipleControlledVocabularyNode";
                case "level":                               return "SimplePrimitiveNode";
                case "lifeCycleStep":                       return "MultipleControlledVocabularyNode";
                case "lineage":                             return "MultiplePrimitiveNode";
                case "modificationDate":                    return "SimplePrimitiveNode";
                case "northLongitude":                      return "SimplePrimitiveNode";
                case "notesText":                           return "SimplePrimitiveNode";
                case "originOfSources":                     return "SimplePrimitiveNode";
                case "otherDataAppraisal":                  return "SimplePrimitiveNode";
                case "otherGeographicCoverage":             return "SimplePrimitiveNode";
                case "otherId":                             return "MultipleCompoundNode";
                case "otherIdAgency":                       return "SimplePrimitiveNode";
                case "otherIdValue":                        return "SimplePrimitiveNode";
                case "otherReferences":                     return "MultiplePrimitiveNode";
                case "priorVersion":                        return "MultiplePrimitiveNode";
                case "producer":                            return "MultipleCompoundNode";
                case "producerAbbreviation":                return "SimplePrimitiveNode";
                case "producerAffiliation":                 return "SimplePrimitiveNode";
                case "producerLogoURL":                     return "SimplePrimitiveNode";
                case "producerName":                        return "SimplePrimitiveNode";
                case "producerURL":                         return "SimplePrimitiveNode";
                case "productionDate":                      return "SimplePrimitiveNode";
                case "productionPlace":                     return "SimplePrimitiveNode";
                case "project":                             return "SimpleCompoundNode";
                case "projectAcronym":                      return "SimplePrimitiveNode";
                case "projectId":                           return "SimplePrimitiveNode";
                case "projectTask":                         return "SimplePrimitiveNode";
                case "projectTitle":                        return "SimplePrimitiveNode";
                case "projectURL":                          return "SimplePrimitiveNode";
                case "projectWorkPackage":                  return "SimplePrimitiveNode";
                case "publication":                         return "MultipleCompoundNode";
                case "publicationCitation":                 return "SimplePrimitiveNode";
                case "publicationIDNumber":                 return "SimplePrimitiveNode";
                case "publicationIDType":                   return "SimpleControlledVocabularyNode";
                case "publicationURL":                      return "SimplePrimitiveNode";
                case "qualityValidity":                     return "MultipleCompoundNode";
                case "relatedDataset":                      return "MultipleCompoundNode";
                case "relatedDatasetCitation":              return "SimplePrimitiveNode";
                case "relatedDatasetIDNumber":              return "SimplePrimitiveNode";
                case "relatedDatasetIDType":                return "SimpleControlledVocabularyNode";
                case "relatedDatasetURL":                   return "SimplePrimitiveNode";
                case "relatedMaterial":                     return "MultiplePrimitiveNode";
                case "researchInstrument":                  return "SimplePrimitiveNode";
                case "resourceVersion":                     return "SimpleCompoundNode";
                case "responseRate":                        return "SimplePrimitiveNode";
                case "samplingErrorEstimates":              return "SimplePrimitiveNode";
                case "samplingProcedure":                   return "SimpleControlledVocabularyNode";
                case "samplingProcedureOther":              return "SimplePrimitiveNode";
                case "series":                              return "SimpleCompoundNode";
                case "seriesInformation":                   return "SimplePrimitiveNode";
                case "seriesName":                          return "SimplePrimitiveNode";
                case "socialScienceNotes":                  return "SimpleCompoundNode";
                case "socialScienceNotesSubject":           return "SimplePrimitiveNode";
                case "socialScienceNotesText":              return "SimplePrimitiveNode";
                case "socialScienceNotesType":              return "SimplePrimitiveNode";
                case "software":                            return "MultipleCompoundNode";
                case "softwareName":                        return "SimplePrimitiveNode";
                case "softwareVersion":                     return "SimplePrimitiveNode";
                case "source":                              return "MultipleCompoundNode";
                case "southLongitude":                      return "SimplePrimitiveNode";
                case "spatialResolution":                   return "MultiplePrimitiveNode";
                case "specification":                       return "MultiplePrimitiveNode";
                case "state":                               return "SimplePrimitiveNode";
                case "studyAssayCellType":                  return "MultiplePrimitiveNode";
                case "studyAssayMeasurementType":           return "MultipleControlledVocabularyNode";
                case "studyAssayOrganism":                  return "MultipleControlledVocabularyNode";
                case "studyAssayOtherMeasurmentType":       return "MultiplePrimitiveNode";
                case "studyAssayOtherOrganism":             return "MultiplePrimitiveNode";
                case "studyAssayPlatform":                  return "MultipleControlledVocabularyNode";
                case "studyAssayPlatformOther":             return "MultiplePrimitiveNode";
                case "studyAssayTechnologyType":            return "MultipleControlledVocabularyNode";
                case "studyAssayTechnologyTypeOther":       return "MultiplePrimitiveNode";
                case "studyDesignType":                     return "MultipleControlledVocabularyNode";
                case "studyDesignTypeOther":                return "MultiplePrimitiveNode";
                case "studyFactorType":                     return "MultipleControlledVocabularyNode";
                case "studyFactorTypeOther":                return "MultiplePrimitiveNode";
                case "studyProtocolType":                   return "MultiplePrimitiveNode";
                case "studySampleType":                     return "MultiplePrimitiveNode";
                case "subject":                             return "MultipleControlledVocabularyNode";
                case "subtitle":                            return "SimplePrimitiveNode";
                case "targetSampleActualSize":              return "SimplePrimitiveNode";
                case "targetSampleSize":                    return "SimpleCompoundNode";
                case "targetSampleSizeFormula":             return "SimplePrimitiveNode";
                case "timeMethod":                          return "SimpleControlledVocabularyNode";
                case "timeMethodOther":                     return "SimplePrimitiveNode";
                case "timePeriodCovered":                   return "MultipleCompoundNode";
                case "timePeriodCoveredEnd":                return "SimplePrimitiveNode";
                case "timePeriodCoveredStart":              return "SimplePrimitiveNode";
                case "title":                               return "SimplePrimitiveNode";
                case "topicClassification":                 return "MultipleCompoundNode";
                case "topicClassValue":                     return "SimplePrimitiveNode";
                case "topicClassVocab":                     return "SimplePrimitiveNode";
                case "topicClassVocabURI":                  return "SimplePrimitiveNode";
                case "typeOfSource":                        return "MultiplePrimitiveNode";
                case "typeOfSR":                            return "SimpleControlledVocabularyNode";
                case "unitOfAnalysis":                      return "MultipleControlledVocabularyNode";
                case "unitOfAnalysisOther":                 return "SimplePrimitiveNode";
                case "universe":                            return "MultiplePrimitiveNode";
                case "URI":                                 return "MultiplePrimitiveNode";
                case "version":                             return "SimplePrimitiveNode";
                case "versionInfo":                         return "SimplePrimitiveNode";
                case "versionStatus":                       return "MultipleControlledVocabularyNode";
                case "weighting":                           return "SimplePrimitiveNode";
                case "westLongitude":                       return "SimplePrimitiveNode";
                default:                                    return String.Empty;
            }
        }

        /// <summary><c>ListAuthorizedFields</c> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le document des métadonnées</summary>
        /// <returns>la liste des fields autorisés</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable authorizedFields = AuthorizedFields.ListAuthorizedFields();
        ///     </code>
        /// </example>
        public static Hashtable ListAuthorizedFields()
        {
            Hashtable authorizedFields = new Hashtable();
            authorizedFields = Helper.MergeHashtables(authorizedFields, AuthorizedFields.ListAuthorizedFieldsForBiomedicalNode());
            authorizedFields = Helper.MergeHashtables(authorizedFields, AuthorizedFields.ListAuthorizedFieldsForCitationNode());
            authorizedFields = Helper.MergeHashtables(authorizedFields, AuthorizedFields.ListAuthorizedFieldsForDerivedTextNode());
            authorizedFields = Helper.MergeHashtables(authorizedFields, AuthorizedFields.ListAuthorizedFieldsForGeospatialNode());
            authorizedFields = Helper.MergeHashtables(authorizedFields, AuthorizedFields.ListAuthorizedFieldsForJournalNode());
            authorizedFields = Helper.MergeHashtables(authorizedFields, AuthorizedFields.ListAuthorizedFieldsForSemanticsNode());
            authorizedFields = Helper.MergeHashtables(authorizedFields, AuthorizedFields.ListAuthorizedFieldsForSocialScienceNode());
            return authorizedFields;
        }

        /// <summary><c>ListAuthorizedFieldsForCategoryNode</c> est une méthode qui permet de récupérer la liste de tous les fields autorisés pour un node donné</summary>
        /// <param name="node">le node pour lequel lister les champs autorisés</param>
        /// <returns>la liste des fields autorisés pour le node passé en paramètre</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable authorizedFields = AuthorizedFields.ListAuthorizedFieldsForCategoryNode("citation");
        ///     </code>
        /// </example>
        public static Hashtable ListAuthorizedFieldsForCategoryNode(String node)
        {
            //category node
            switch (node)
            {
                case "biomedical":      return AuthorizedFields.ListAuthorizedFieldsForBiomedicalNode();
                case "citation":        return AuthorizedFields.ListAuthorizedFieldsForCitationNode();
                case "Derived-text":    return AuthorizedFields.ListAuthorizedFieldsForDerivedTextNode();
                case "geospatial":      return AuthorizedFields.ListAuthorizedFieldsForGeospatialNode();
                case "journal":         return AuthorizedFields.ListAuthorizedFieldsForJournalNode();
                case "semantics":       return AuthorizedFields.ListAuthorizedFieldsForSemanticsNode();
                case "socialscience":   return AuthorizedFields.ListAuthorizedFieldsForSocialScienceNode();
                default:                return AuthorizedFields.ListAuthorizedFieldsForCompoundSubnode(node);
            }
        }

        /// <summary><c>ListAuthorizedFieldsForCompoundSubNode</c> est une méthode qui permet de récupérer la liste de tous les fields autorisés pour un subnode donné</summary>
        /// <param name="compoundSubnode">le subnode pour lequel lister les champs autorisés</param>
        /// <returns>la liste des fields autorisés pour le subnode passé en paramètre</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable authorizedFields = AuthorizedFields.ListAuthorizedFieldsForCompoundSubNode("author");
        ///     </code>
        /// </example>
        public static Hashtable ListAuthorizedFieldsForCompoundSubnode(String compoundSubnode)
        {
            //category node
            switch (compoundSubnode)
            {
                case "author":                      
                    return new Hashtable() 
                    { 
                        { "authorAffiliation",              "SimplePrimitiveNode"               }, 
                        { "authorIdentifier",               "SimplePrimitiveNode"               }, 
                        { "authorIdentifierScheme",         "SimpleControlledVocabularyNode"    }, 
                        { "authorName",                     "SimplePrimitiveNode"               }, 
                    };

                case "contributor":                 
                    return new Hashtable() 
                    { 
                        { "contributorAffiliation",         "SimplePrimitiveNode"               }, 
                        { "contributorIdentifier",          "SimplePrimitiveNode"               }, 
                        { "contributorIdentifierScheme",    "SimpleControlledVocabularyNode"    }, 
                        { "contributorName",                "SimplePrimitiveNode"               }, 
                        { "contributorType",                "SimpleControlledVocabularyNode"    }, 
                    };
                
                case "datasetContact":              
                    return new Hashtable() 
                    { 
                        { "datasetContactAffiliation",      "SimplePrimitiveNode"               }, 
                        { "datasetContactEmail",            "SimplePrimitiveNode"               }, 
                        { "datasetContactName",             "SimplePrimitiveNode"               }, 
                    };
                
                case "dateOfCollection":            
                    return new Hashtable() 
                    { 
                        { "dateOfCollectionStart",          "SimplePrimitiveNode"               }, 
                        { "dateOfCollectionEnd",            "SimplePrimitiveNode"               }, 
                    };
                
                case "distributor":                 
                    return new Hashtable() 
                    { 
                        { "distributorAbbreviation",        "SimplePrimitiveNode"               }, 
                        { "distributorAffiliation",         "SimplePrimitiveNode"               }, 
                        { "distributorLogoURL",             "SimplePrimitiveNode"               }, 
                        { "distributorName",                "SimplePrimitiveNode"               }, 
                        { "distributorURL",                 "SimplePrimitiveNode"               }, 
                    };
                
                case "dsDescription":               
                    return new Hashtable() 
                    { 
                        { "dsDescriptionDate",              "SimplePrimitiveNode"               }, 
                        { "dsDescriptionValue",             "SimplePrimitiveNode"               }, 
                    };
                
                case "grantNumber":                 
                    return new Hashtable() 
                    { 
                        { "grantNumberAgency",              "SimplePrimitiveNode"               }, 
                        { "grantNumberValue",               "SimplePrimitiveNode"               }, 
                    };
                
                case "keyword":                     
                    return new Hashtable() 
                    { 
                        { "keywordTermURI",                 "SimplePrimitiveNode"               }, 
                        { "keywordValue",                   "SimplePrimitiveNode"               }, 
                        { "keywordVocabulary",              "SimplePrimitiveNode"               }, 
                        { "keywordVocabularyURI",           "SimplePrimitiveNode"               }, 
                    };
                
                case "otherId":                     
                    return new Hashtable() 
                    { 
                        { "otherIdAgency",                  "SimplePrimitiveNode"               }, 
                        { "otherIdValue",                   "SimplePrimitiveNode"               }, 
                    };
                
                case "producer":                    
                    return new Hashtable() 
                    { 
                        { "producerAbbreviation",           "SimplePrimitiveNode"               }, 
                        { "producerAffiliation",            "SimplePrimitiveNode"               }, 
                        { "producerLogoURL",                "SimplePrimitiveNode"               }, 
                        { "producerName",                   "SimplePrimitiveNode"               }, 
                        { "producerURL",                    "SimplePrimitiveNode"               }, 
                    };
                
                case "project":                     
                    return new Hashtable() 
                    { 
                        { "projectAcronym",                 "SimplePrimitiveNode"               }, 
                        { "projectId",                      "SimplePrimitiveNode"               }, 
                        { "projectTask",                    "SimplePrimitiveNode"               }, 
                        { "projectTitle",                   "SimplePrimitiveNode"               }, 
                        { "projectURL",                     "SimplePrimitiveNode"               }, 
                        { "projectWorkPackage",             "SimplePrimitiveNode"               }, 
                    };
                
                case "publication":                 
                    return new Hashtable() 
                    { 
                        { "publicationCitation",            "SimplePrimitiveNode"               }, 
                        { "publicationIDNumber",            "SimplePrimitiveNode"               }, 
                        { "publicationIDType",              "SimpleControlledVocabularyNode"    }, 
                        { "publicationURL",                 "SimplePrimitiveNode"               }, 
                    };
                
                case "relatedDataset":              
                    return new Hashtable() 
                    { 
                        { "relatedDatasetCitation",         "SimplePrimitiveNode"               }, 
                        { "relatedDatasetIDNumber",         "SimplePrimitiveNode"               }, 
                        { "relatedDatasetIDType",           "SimpleControlledVocabularyNode"    }, 
                        { "relatedDatasetURL",              "SimplePrimitiveNode"               }, 
                    };
                
                case "series":                      
                    return new Hashtable() 
                    { 
                        { "seriesInformation",              "SimplePrimitiveNode"               }, 
                        { "seriesName",                     "SimplePrimitiveNode"               }, 
                    };
                
                case "software":                    
                    return new Hashtable() 
                    { 
                        { "softwareName",                   "SimplePrimitiveNode"               }, 
                        { "softwareVersion",                "SimplePrimitiveNode"               }, 
                    };
                
                case "timePeriodCovered":           
                    return new Hashtable() 
                    { 
                        { "timePeriodCoveredStart",         "SimplePrimitiveNode"               }, 
                        { "timePeriodCoveredEnd",           "SimplePrimitiveNode"               }, 
                    };
                
                case "topicClassification":         
                    return new Hashtable() 
                    { 
                        { "topicClassValue",                "SimplePrimitiveNode"               }, 
                        { "topicClassVocab",                "SimplePrimitiveNode"               }, 
                        { "topicClassVocabURI",             "SimplePrimitiveNode"               }, 
                    };
                
                case "conformity":                  
                    return new Hashtable() 
                    { 
                        { "degree",                         "SimpleControlledVocabularyNode"    }, 
                        { "specification",                  "SimplePrimitiveNode"               }, 
                    };
                
                case "geographicBoundingBox":       
                    return new Hashtable() 
                    { 
                        { "eastLongitude",                  "SimplePrimitiveNode"               }, 
                        { "northLongitude",                 "SimplePrimitiveNode"               }, 
                        { "southLongitude",                 "SimplePrimitiveNode"               }, 
                        { "westLongitude",                  "SimplePrimitiveNode"               }, 
                    };
                
                case "geographicCoverage":          
                    return new Hashtable() 
                    { 
                        { "city",                           "SimplePrimitiveNode"               }, 
                        { "country",                        "SimpleControlledVocabularyNode"    }, 
                        { "otherGeographicCoverage",        "SimplePrimitiveNode"               }, 
                        { "state",                          "SimplePrimitiveNode"               }, 
                    };
                
                case "qualityValidity":             
                    return new Hashtable() 
                    { 
                        { "lineage",                        "MultiplePrimitiveNode"             }, 
                        { "spatialResolution",              "MultiplePrimitiveNode"             }, 
                    };
                
                case "geographicalReferential":     
                    return new Hashtable() 
                    { 
                        { "level",                          "SimplePrimitiveNode"               }, 
                        { "version",                        "SimplePrimitiveNode"               }, 
                    };
                
                case "socialScienceNotes":          
                    return new Hashtable() 
                    { 
                        { "socialScienceNotesSubject",      "SimplePrimitiveNode"               }, 
                        { "socialScienceNotesText",         "SimplePrimitiveNode"               }, 
                        { "socialScienceNotesType",         "SimplePrimitiveNode"               }, 
                    };
                
                case "targetSampleSize":            
                    return new Hashtable() 
                    { 
                        { "targetSampleActualSize",         "SimplePrimitiveNode"               }, 
                        { "targetSampleSizeFormula",        "SimplePrimitiveNode"               }, 
                    };
                
                case "journalVolumeIssue":          
                    return new Hashtable() 
                    { 
                        { "journalIssue",                   "SimplePrimitiveNode"               }, 
                        { "journalPubDate",                 "SimplePrimitiveNode"               }, 
                        { "journalVolume",                  "SimplePrimitiveNode"               }, 
                    };
                
                case "source":                      
                    return new Hashtable() 
                    { 
                        { "ageOfSource",                    "MultiplePrimitiveNode"             }, 
                        { "citations",                      "MultiplePrimitiveNode"             }, 
                        { "experimentNumber",               "MultiplePrimitiveNode"             }, 
                        { "typeOfSource",                   "MultiplePrimitiveNode"             }, 
                    };
                
                case "resourceVersion":             
                    return new Hashtable() 
                    { 
                        { "changes",                        "SimplePrimitiveNode"               }, 
                        { "modificationDate",               "SimplePrimitiveNode"               }, 
                        { "priorVersion",                   "MultiplePrimitiveNode"             }, 
                        { "versionInfo",                    "SimplePrimitiveNode"               }, 
                        { "versionStatus",                  "MultipleControlledVocabularyNode"  }, 
                    };
                
                default:                            
                    return new Hashtable();
            }
        }

        /// <summary><c>ListAuthorizedFieldsForBiomedicalNode</c> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "biomedical"</summary>
        /// <returns>la liste des fields autorisés</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable authorizedFields = AuthorizedFields.ListAuthorizedFieldsForBiomedicalNode();
        ///     </code>
        /// </example>
        public static Hashtable ListAuthorizedFieldsForBiomedicalNode()
        {
            return new Hashtable()
            {
                { "studyAssayCellType",             "MultiplePrimitiveNode"                 },
                { "studyAssayMeasurementType",      "MultipleControlledVocabularyNode"      },
                { "studyAssayOrganism",             "MultipleControlledVocabularyNode"      },
                { "studyAssayOtherMeasurmentType",  "MultiplePrimitiveNode"                 },
                { "studyAssayOtherOrganism",        "MultiplePrimitiveNode"                 },
                { "studyAssayPlatform",             "MultipleControlledVocabularyNode"      },
                { "studyAssayPlatformOther",        "MultiplePrimitiveNode"                 },
                { "studyAssayTechnologyType",       "MultipleControlledVocabularyNode"      },
                { "studyAssayTechnologyTypeOther",  "MultiplePrimitiveNode"                 },
                { "studyDesignType",                "MultipleControlledVocabularyNode"      },
                { "studyDesignTypeOther",           "MultiplePrimitiveNode"                 },
                { "studyFactorType",                "MultipleControlledVocabularyNode"      },
                { "studyFactorTypeOther",           "MultiplePrimitiveNode"                 },
                { "studyProtocolType",              "MultiplePrimitiveNode"                 },
                { "studySampleType",                "MultiplePrimitiveNode"                 }
            };
        }

        /// <summary><c>ListAuthorizedFieldsForCitationNode</c> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "citation"</summary>
        /// <returns>la liste des fields autorisés</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable authorizedFields = AuthorizedFields.ListAuthorizedFieldsForCitationNode();
        ///     </code>
        /// </example>
        public static Hashtable ListAuthorizedFieldsForCitationNode()
        {
            return new Hashtable()
            {
                { "accessToSources",            "SimplePrimitiveNode"               },
                { "alternativeTitle",           "SimplePrimitiveNode"               },
                { "alternativeURL",             "SimplePrimitiveNode"               },
                { "author",                     "MultipleCompoundNode"              },
                { "characteristicOfSources",    "SimplePrimitiveNode"               },
                { "contributor",                "MultipleCompoundNode"              },
                { "dataOrigin",                 "MultipleControlledVocabularyNode"  },
                { "datasetContact",             "MultipleCompoundNode"              },
                { "dataSources",                "MultiplePrimitiveNode"             },
                { "dateOfCollection",           "MultipleCompoundNode"              },
                { "dateOfDeposit",              "SimplePrimitiveNode"               },
                { "depositor",                  "SimplePrimitiveNode"               },
                { "distributionDate",           "SimplePrimitiveNode"               },
                { "distributor",                "MultipleCompoundNode"              },
                { "dsDescription",              "MultipleCompoundNode"              },
                { "grantNumber",                "MultipleCompoundNode"              },
                { "keyword",                    "MultipleCompoundNode"              },
                { "kindOfData",                 "MultipleControlledVocabularyNode"  },
                { "kindOfDataOther",            "MultiplePrimitiveNode"             },
                { "language",                   "MultipleControlledVocabularyNode"  },
                { "lifeCycleStep",              "MultipleControlledVocabularyNode"  },
                { "notesText",                  "SimplePrimitiveNode"               },
                { "originOfSources",            "SimplePrimitiveNode"               },
                { "otherId",                    "MultipleCompoundNode"              },
                { "otherReferences",            "MultiplePrimitiveNode"             },
                { "producer",                   "MultipleCompoundNode"              },
                { "productionDate",             "SimplePrimitiveNode"               },
                { "productionPlace",            "SimplePrimitiveNode"               },
                { "project",                    "SimpleCompoundNode"                },
                { "publication",                "MultipleCompoundNode"              },
                { "relatedDataset",             "MultipleCompoundNode"              },
                { "relatedMaterial",            "MultiplePrimitiveNode"             },
                { "series",                     "SimpleCompoundNode"                },
                { "software",                   "MultipleCompoundNode"              },
                { "subject",                    "MultipleControlledVocabularyNode"  },
                { "subtitle",                   "SimplePrimitiveNode"               },
                { "timePeriodCovered",          "MultipleCompoundNode"              },
                { "title",                      "SimplePrimitiveNode"               },
                { "topicClassification",        "MultipleCompoundNode"              }
            };
        }

        /// <summary><c>ListAuthorizedFieldsForDerivedTextNode</c> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "Derived-text"</summary>
        /// <returns>la liste des fields autorisés</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable authorizedFields = AuthorizedFields.ListAuthorizedFieldsForDerivedTextNode();
        ///     </code>
        /// </example>
        public static Hashtable ListAuthorizedFieldsForDerivedTextNode()
        {
            return new Hashtable()
            {
                { "source", "MultipleCompoundNode" }
            };
        }

        /// <summary><c>ListAuthorizedFieldsForGeospatialNode</c> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "geospatial"</summary>
        /// <returns>la liste des fields autorisés</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable authorizedFields = AuthorizedFields.ListAuthorizedFieldsForGeospatialNode();
        ///     </code>
        /// </example>
        public static Hashtable ListAuthorizedFieldsForGeospatialNode()
        {
            return new Hashtable()
            {
                { "conformity",             "MultipleCompoundNode"  },
                { "geographicBoundingBox",  "MultipleCompoundNode"  },
                { "geographicCoverage",     "MultipleCompoundNode"  },
                { "geographicUnit",         "MultiplePrimitiveNode" },
                { "qualityValidity",        "MultipleCompoundNode"  }
            };
        }

        /// <summary><c>ListAuthorizedFieldsForJournalNode</c> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "journal"</summary>
        /// <returns>la liste des fields autorisés</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable authorizedFields = AuthorizedFields.ListAuthorizedFieldsForJournalNode();
        ///     </code>
        /// </example>
        public static Hashtable ListAuthorizedFieldsForJournalNode()
        {
            return new Hashtable()
            {
                { "journalArticleType", "SimpleControlledVocabularyNode"    },
                { "journalVolumeIssue", "MultipleCompoundNode"              }
            };
        }

        /// <summary><c>ListAuthorizedFieldsForSemanticsNode</c> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "semantics"</summary>
        /// <returns>la liste des fields autorisés</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable authorizedFields = AuthorizedFields.ListAuthorizedFieldsForSemanticsNode();
        ///     </code>
        /// </example>
        public static Hashtable ListAuthorizedFieldsForSemanticsNode()
        {
            return new Hashtable()
            {
                { "bugDatabase",                "SimplePrimitiveNode"               },
                { "designedForOntologyTask",    "MultipleControlledVocabularyNode"  },
                { "endpoint",                   "SimplePrimitiveNode"               },
                { "hasFormalityLevel",          "SimpleControlledVocabularyNode"    },
                { "hasOntologyLanguage",        "MultipleControlledVocabularyNode"    },
                { "knownUsage",                 "SimplePrimitiveNode"               },
                { "imports",                    "MultiplePrimitiveNode"             },
                { "resourceVersion",            "SimpleCompoundNode"                },
                { "typeOfSR",                   "SimpleControlledVocabularyNode"    },
                { "URI",                        "MultiplePrimitiveNode"             }
            };
        }

        /// <summary><c>ListAuthorizedFieldsForSocialScienceNode</c> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "socialscience"</summary>
        /// <returns>la liste des fields autorisés</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable authorizedFields = AuthorizedFields.ListAuthorizedFieldsForSocialScienceNode();
        ///     </code>
        /// </example>
        public static Hashtable ListAuthorizedFieldsForSocialScienceNode()
        {
            return new Hashtable()
            {
                { "actionsToMinimizeLoss",        "SimplePrimitiveNode"                 },
                { "cleaningOperations",           "SimplePrimitiveNode"                 },
                { "collectionMode",               "SimpleControlledVocabularyNode"      },
                { "collectionModeOther",          "SimplePrimitiveNode"                 },
                { "collectorTraining",            "SimplePrimitiveNode"                 },
                { "controlOperations",            "SimplePrimitiveNode"                 },
                { "dataCollectionSituation",      "SimplePrimitiveNode"                 },
                { "dataCollector",                "SimplePrimitiveNode"                 },
                { "datasetLevelErrorNotes",       "SimplePrimitiveNode"                 },
                { "deviationsFromSampleDesign",   "SimplePrimitiveNode"                 },
                { "frequencyOfDataCollection",    "SimplePrimitiveNode"                 },
                { "geographicalReferential",      "MultipleCompoundNode"                },
                { "otherDataAppraisal",           "SimplePrimitiveNode"                 },
                { "researchInstrument",           "SimplePrimitiveNode"                 },
                { "responseRate",                 "SimplePrimitiveNode"                 },
                { "samplingErrorEstimates",       "SimplePrimitiveNode"                 },
                { "samplingProcedure",            "SimpleControlledVocabularyNode"      },
                { "samplingProcedureOther",       "SimplePrimitiveNode"                 },
                { "socialScienceNotes",           "SimpleCompoundNode"                  },
                { "targetSampleSize",             "SimpleCompoundNode"                  },
                { "timeMethod",                   "SimpleControlledVocabularyNode"      },
                { "timeMethodOther",              "SimplePrimitiveNode"                 },
                { "unitOfAnalysis",               "MultipleControlledVocabularyNode"    },
                { "unitOfAnalysisOther",          "SimplePrimitiveNode"                 },
                { "universe",                     "MultiplePrimitiveNode"               },
                { "weighting",                    "SimplePrimitiveNode"                 }
            };
        }
    }
}
