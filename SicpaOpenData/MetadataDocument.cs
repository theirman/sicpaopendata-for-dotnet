﻿using Newtonsoft.Json;
using System;
using System.Collections;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Mai 2021</li>
    ///         <li>Description : Classe implémentant la construction du document JSON des métadonnées</li>
    ///     </ul>
    /// </summary>
    public class MetadataDocument
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /// <summary>Cette propriété indique la licence du dataset</summary>
        /// <value>String</value>
        public String License { get; set; }

        /// <summary>Cette propriété indique les termes d'utilisation du dataset</summary>
        /// <value>String</value>
        public String TermsOfUse { get; set; }

        /// <summary>Cette propriété permet de gérer la liste des category nodes du document</summary>
        /// <value>ArrayList</value>
        public Hashtable MetadataBlocks { get; set; }




        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /// <summary>Constructeur ne nécessitant pas de paramètres</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         MetadataDocument md = new MetadataDocument();
        ///     </code>
        /// </example>
        public MetadataDocument()
        {
            this.License        =   "NONE";
            this.TermsOfUse     =   "<img src=\"https://www.etalab.gouv.fr/wp-content/uploads/2011/10/licence-ouverte-open-licence.gif\" alt=\"Licence Ouverte\" height=\"100\">" +
                                    "<a href=\"https://www.etalab.gouv.fr/licence-ouverte-open-licence\">Licence Ouverte / Open Licence Version 2.0</a> " +
                                    "compatible CC BY";
            this.MetadataBlocks = new Hashtable();
        }

        /// <summary>Constructeur nécessitant deux paramètres : license et values</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         MetadataDocument md = new MetadataDocument("license", "termsOfUse");
        ///     </code>
        /// </example>
        public MetadataDocument(String license, String termsOfUse)
        {
            this.License        = license;
            this.TermsOfUse     = termsOfUse;
            this.MetadataBlocks = new Hashtable();
        }




        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             


        /// <summary><c>AddBiomedicalNode</c> est une méthode qui permet d'ajouter le node biomedical à la liste des category nodes</summary>
        /// <param name="biomedicalNode">la valeur du node biomedical</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         BiomedicalNode bn = new BiomedicalNode();
        ///         ...
        ///         ...
        ///         md.AddBiomedicalNode(bn);
        ///     </code>
        /// </example>
        public void AddBiomedicalNode(BiomedicalNode biomedicalNode)
        {
            this.MetadataBlocks.Add("biomedical", biomedicalNode);
        }

        /// <summary><c>AddCitationNode</c> est une méthode qui permet d'ajouter le node citation à la liste des category nodes</summary>
        /// <param name="citationNode">la valeur du node citation</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         CitationNode cn = new CitationNode();
        ///         ...
        ///         ...
        ///         md.AddCitationNode(cn);
        ///     </code>
        /// </example>
        public void AddCitationNode(CitationNode citationNode)
        {
            this.MetadataBlocks.Add("citation", citationNode);
        }

        /// <summary><c>AddDerivedTextNode</c> est une méthode qui permet d'ajouter le node derived-text à la liste des category nodes</summary>
        /// <param name="derivedTextNode">la valeur du node derived-text</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         DerivedTextNode dtn = new DerivedTextNode();
        ///         ...
        ///         ...
        ///         md.AddDerivedTextNode(dtn);
        ///     </code>
        /// </example>
        public void AddDerivedTextNode(DerivedTextNode derivedTextNode)
        {
            this.MetadataBlocks.Add("Derived-text", derivedTextNode);
        }

        /// <summary><c>AddGeospatialNode</c> est une méthode qui permet d'ajouter le node geospatial à la liste des category nodes</summary>
        /// <param name="geospatialNode">la valeur du node geospatial</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         GeospatialNode gn = new GeospatialNode();
        ///         ...
        ///         ...
        ///         md.AddGeospatialNode(gn);
        ///     </code>
        /// </example>
        public void AddGeospatialNode(GeospatialNode geospatialNode)
        {
            this.MetadataBlocks.Add("geospatial", geospatialNode);
        }

        /// <summary><c>AddJournalNode</c> est une méthode qui permet d'ajouter le node journal à la liste des category nodes</summary>
        /// <param name="journalNode">la valeur du node journal</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         JournalNode jn = new JournalNode();
        ///         ...
        ///         ...
        ///         md.AddJournalNode(jn);
        ///     </code>
        /// </example>
        public void AddJournalNode(JournalNode journalNode)
        {
            this.MetadataBlocks.Add("journal", journalNode);
        }

        /// <summary><c>AddSemanticsNode</c> est une méthode qui permet d'ajouter le node semantics à la liste des category nodes</summary>
        /// <param name="semanticsNode">la valeur du node semantics</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SemanticsNode sn = new SemanticsNode();
        ///         ...
        ///         ...
        ///         md.AddSemanticsNode(bn);
        ///     </code>
        /// </example>
        public void AddSemanticsNode(SemanticsNode semanticsNode)
        {
            this.MetadataBlocks.Add("semantics", semanticsNode);
        }

        /// <summary><c>AddSocialScienceNode</c> est une méthode qui permet d'ajouter le node socialscience à la liste des category nodes</summary>
        /// <param name="socialScienceNode">la valeur du node socialscience</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SocialScienceNode ssn = new SocialScienceNode();
        ///         ...
        ///         ...
        ///         md.AddSocialScienceNode(ssn);
        ///     </code>
        /// </example>
        public void AddSocialScienceNode(SocialScienceNode socialScienceNode)
        {
            this.MetadataBlocks.Add("socialscience", socialScienceNode);
        }

        /// <summary><c>ToJSON</c> est une méthode qui permet d'obtenir une réprésentation JSON du document</summary>
        /// <returns>la représentation du node sous forme de chaine JSON</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String json = md.ToJSON();
        ///     </code>
        /// </example>
        public String ToJSON()
        {
            Hashtable fields = new Hashtable()
            {
                { "license",        this.License        },
                { "termsOfUse",      this.TermsOfUse    },
                { "metadataBlocks", this.MetadataBlocks }
            };

            Hashtable document = new Hashtable()
            {
                { "datasetVersion", fields }
            };

            return JsonConvert.SerializeObject(document, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }

        /// <summary><c>ToString</c> est une méthode qui permet d'obtenir une réprésentation textuelle du document (JSON minifié)</summary>
        /// <returns>la représentation du node sous forme de chaine JSON minifiée</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String nodeString = md.ToString();
        ///     </code>
        /// </example>
        override public String ToString()
        {
            Hashtable fields = new Hashtable()
            {
                { "license",        this.License        },
                { "termsOfUse",      this.TermsOfUse    },
                { "metadataBlocks", this.MetadataBlocks }
            };

            Hashtable document = new Hashtable()
            {
                { "datasetVersion", fields }
            };

            return JsonConvert.SerializeObject(document, Formatting.None, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }







        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //



    }
}
