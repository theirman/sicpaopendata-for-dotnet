﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SicpaOpenData.Metadata;

namespace SicpaOpenData.Examples
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Juin 2021</li>
    ///         <li>Description : Classe montrant comment utiliser les classes de <c>SicpaOpenData.Metadata</c></li>
    ///     </ul>
    /// </summary>
    public static class MetadataExamples
    {
        /// <summary><c>HowToBuildABiomedicalNode</c> montre la manière de construire un <c>BiomedicalNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         // je crée un node biomedical
        ///         BiomedicalNode bn = new BiomedicalNode();
        ///         bn.AddField(new MultipleControlledVocabularyNode("studyDesignType", new List<String>() { "Case Control", "Cross Sectional" }));
        ///         bn.AddField(new MultiplePrimitiveNode("studyDesignTypeOther", new List<String>() { "studyDesignTypeOther1", "studyDesignTypeOther2" }));
        ///         bn.AddField(new MultipleControlledVocabularyNode("studyFactorType", new List<String>() { "Age", "Biomarkers" }));
        ///         bn.AddField(new MultiplePrimitiveNode("studyFactorTypeOther", new List<String>() { "studyFactorTypeOther1", "studyFactorTypeOther2" }));
        ///         bn.AddField(new MultipleControlledVocabularyNode("studyAssayOrganism", new List<String>() { "Arabidopsis thaliana", "Bos taurus" }));
        ///         bn.AddField(new MultiplePrimitiveNode("studyAssayOtherOrganism", new List<String>() { "studyAssayOtherOrganism1", "studyAssayOtherOrganism2" }));
        ///         bn.AddField(new MultipleControlledVocabularyNode("studyAssayMeasurementType", new List<String>() { "cell counting", "cell sorting" }));
        ///         bn.AddField(new MultiplePrimitiveNode("studyAssayOtherMeasurmentType", new List<String>() { "studyAssayOtherMeasurmentType1", "studyAssayOtherMeasurmentType2" }));
        ///         bn.AddField(new MultipleControlledVocabularyNode("studyAssayTechnologyType", new List<String>() { "culture based drug susceptibility testing, single concentration", "culture based drug susceptibility testing, two concentrations" }));
        ///         bn.AddField(new MultiplePrimitiveNode("studyAssayTechnologyTypeOther", new List<String>() { "studyAssayTechnologyTypeOther1", "studyAssayTechnologyTypeOther2" }));
        ///         bn.AddField(new MultipleControlledVocabularyNode("studyAssayPlatform", new List<String>() { "210-MS GC Ion Trap (Varian)", "220-MS GC Ion Trap (Varian)" }));
        ///         bn.AddField(new MultiplePrimitiveNode("studyAssayPlatformOther", new List<String>() { "studyAssayPlatformOther1", "studyAssayPlatformOther2" }));
        ///         bn.AddField(new MultiplePrimitiveNode("studyAssayCellType", new List<String>() { "studyAssayCellType1", "studyAssayCellType2" }));
        ///         bn.AddField(new MultiplePrimitiveNode("studySampleType", new List<String>() { "studySampleType1", "studySampleType2" }));
        ///         bn.AddField(new MultiplePrimitiveNode("studyProtocolType", new List<String>() { "studyProtocolType1", "studyProtocolType2" }));
        ///         
        ///         Console.Write(bn.ToJSON();
        ///     </code>
        /// </example>
        public static void HowToBuildABiomedicalNode()
        {
            Console.Write
            (
                Environment.NewLine + "// je crée un node biomedical" +
                Environment.NewLine + "BiomedicalNode bn = new BiomedicalNode();" +
                Environment.NewLine + "bn.AddField(new MultipleControlledVocabularyNode(\"studyDesignType\", new List<String>() { \"Case Control\", \"Cross Sectional\" }));" +
                Environment.NewLine + "bn.AddField(new MultiplePrimitiveNode(\"studyDesignTypeOther\", new List<String>() { \"studyDesignTypeOther1\", \"studyDesignTypeOther2\" }));" +
                Environment.NewLine + "bn.AddField(new MultipleControlledVocabularyNode(\"studyFactorType\", new List<String>() { \"Age\", \"Biomarkers\" }));" +
                Environment.NewLine + "bn.AddField(new MultiplePrimitiveNode(\"studyFactorTypeOther\", new List<String>() { \"studyFactorTypeOther1\", \"studyFactorTypeOther2\" }));" +
                Environment.NewLine + "bn.AddField(new MultipleControlledVocabularyNode(\"studyAssayOrganism\", new List<String>() { \"Arabidopsis thaliana\", \"Bos taurus\" }));" +
                Environment.NewLine + "bn.AddField(new MultiplePrimitiveNode(\"studyAssayOtherOrganism\", new List<String>() { \"studyAssayOtherOrganism1\", \"studyAssayOtherOrganism2\" }));" +
                Environment.NewLine + "bn.AddField(new MultipleControlledVocabularyNode(\"studyAssayMeasurementType\", new List<String>() { \"cell counting\", \"cell sorting\" }));" +
                Environment.NewLine + "bn.AddField(new MultiplePrimitiveNode(\"studyAssayOtherMeasurmentType\", new List<String>() { \"studyAssayOtherMeasurmentType1\", \"studyAssayOtherMeasurmentType2\" }));" +
                Environment.NewLine + "bn.AddField(new MultipleControlledVocabularyNode(\"studyAssayTechnologyType\", new List<String>() { \"culture based drug susceptibility testing, single concentration\", \"culture based drug susceptibility testing, two concentrations\" }));" +
                Environment.NewLine + "bn.AddField(new MultiplePrimitiveNode(\"studyAssayTechnologyTypeOther\", new List<String>() { \"studyAssayTechnologyTypeOther1\", \"studyAssayTechnologyTypeOther2\" }));" +
                Environment.NewLine + "bn.AddField(new MultipleControlledVocabularyNode(\"studyAssayPlatform\", new List<String>() { \"210-MS GC Ion Trap (Varian)\", \"220-MS GC Ion Trap (Varian)\" }));" +
                Environment.NewLine + "bn.AddField(new MultiplePrimitiveNode(\"studyAssayPlatformOther\", new List<String>() { \"studyAssayPlatformOther1\", \"studyAssayPlatformOther2\" }));" +
                Environment.NewLine + "bn.AddField(new MultiplePrimitiveNode(\"studyAssayCellType\", new List<String>() { \"studyAssayCellType1\", \"studyAssayCellType2\" }));" +
                Environment.NewLine + "bn.AddField(new MultiplePrimitiveNode(\"studySampleType\", new List<String>() { \"studySampleType1\", \"studySampleType2\" }));" +
                Environment.NewLine + "bn.AddField(new MultiplePrimitiveNode(\"studyProtocolType\", new List<String>() { \"studyProtocolType1\", \"studyProtocolType2\" }));" +
                Environment.NewLine + "" +
                Environment.NewLine + "Console.Write(bn.ToJSON();"
            );
        }

        /// <summary><c>HowToBuildACitationNode</c> montre la manière de construire un <c>CitationNode</c> complet</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         // je crée les auteurs du dataset
        ///         Hashtable htAuteur1 = new Hashtable();
        ///         htAuteur1.Add("authorAffiliation", new SimplePrimitiveNode("authorAffiliation", "authorAffiliation1"));
        ///         htAuteur1.Add("authorIdentifier", new SimplePrimitiveNode("authorIdentifier", "authorIdentifier1"));
        ///         htAuteur1.Add("authorIdentifierScheme", new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"));
        ///         htAuteur1.Add("authorName", new SimplePrimitiveNode("authorName", "authorName1"));
        ///
        ///         Hashtable htAuteur2 = new Hashtable();
        ///         htAuteur2.Add("authorAffiliation", new SimplePrimitiveNode("authorAffiliation", "authorAffiliation2"));
        ///         htAuteur2.Add("authorIdentifier", new SimplePrimitiveNode("authorIdentifier", "authorIdentifier2"));
        ///         htAuteur2.Add("authorIdentifierScheme", new SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL"));
        ///         htAuteur2.Add("authorName", new SimplePrimitiveNode("authorName", "authorName2"));
        ///
        ///         // je crée les autres identifiants
        ///         Hashtable htAutreId1 = new Hashtable();
        ///         htAutreId1.Add("otherIdAgency", new SimplePrimitiveNode("otherIdAgency", "otherIdAgency1"));
        ///         htAutreId1.Add("otherIdValue", new SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier1"));
        ///
        ///         Hashtable htAutreId2 = new Hashtable();
        ///         htAutreId2.Add("otherIdAgency", new SimplePrimitiveNode("otherIdAgency", "otherIdAgency2"));
        ///         htAutreId2.Add("otherIdValue", new SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier2"));
        ///
        ///         // je crée les contacts du dataset
        ///         Hashtable htContact1 = new Hashtable();
        ///         htContact1.Add("datasetContactAffiliation", new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1"));
        ///         htContact1.Add("datasetContactEmail", new SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com"));
        ///         htContact1.Add("datasetContactName", new SimplePrimitiveNode("datasetContactName", "contactName1"));
        ///
        ///         Hashtable htContact2 = new Hashtable();
        ///         htContact2.Add("datasetContactAffiliation", new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2"));
        ///         htContact2.Add("datasetContactEmail", new SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com"));
        ///         htContact2.Add("datasetContactName", new SimplePrimitiveNode("datasetContactName", "contactName2"));
        ///
        ///         // je crée les contributeurs du dataset
        ///         Hashtable htContributeur1 = new Hashtable();
        ///         htContributeur1.Add("contributorAffiliation", new SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation1"));
        ///         htContributeur1.Add("contributorIdentifier", new SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier1"));
        ///         htContributeur1.Add("contributorIdentifierScheme", new SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID"));
        ///         htContributeur1.Add("contributorName", new SimplePrimitiveNode("contributorName", "contributorName1"));
        ///         htContributeur1.Add("contributorType", new SimpleControlledVocabularyNode("contributorType", "Data collector"));
        ///
        ///         Hashtable htContributeur2 = new Hashtable();
        ///         htContributeur2.Add("contributorAffiliation", new SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation2"));
        ///         htContributeur2.Add("contributorIdentifier", new SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier2"));
        ///         htContributeur2.Add("contributorIdentifierScheme", new SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID"));
        ///         htContributeur2.Add("contributorName", new SimplePrimitiveNode("contributorName", "contributorName2"));
        ///         htContributeur2.Add("contributorType", new SimpleControlledVocabularyNode("contributorType", "Editor"));
        ///
        ///         // je crée les dates des collections du dataset
        ///         Hashtable htDates1 = new Hashtable();
        ///         htDates1.Add("dateOfCollectionStart", new SimplePrimitiveNode("dateOfCollectionStart", "2000-01-01"));
        ///         htDates1.Add("dateOfCollectionEnd", new SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-02"));
        ///
        ///         Hashtable htDates2 = new Hashtable();
        ///         htDates2.Add("dateOfCollectionStart", new SimplePrimitiveNode("dateOfCollectionStart", "2000-01-03"));
        ///         htDates2.Add("dateOfCollectionEnd", new SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-04"));
        ///
        ///         // je crée les descriptions du dataset
        ///         Hashtable htDescription1 = new Hashtable();
        ///         htDescription1.Add("dsDescriptionDate", new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01"));
        ///         htDescription1.Add("dsDescriptionValue", new SimplePrimitiveNode("dsDescriptionValue", "descriptionText1"));
        ///
        ///         Hashtable htDescription2 = new Hashtable();
        ///         htDescription2.Add("dsDescriptionDate", new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01"));
        ///         htDescription2.Add("dsDescriptionValue", new SimplePrimitiveNode("dsDescriptionValue", "descriptionText2"));
        ///
        ///         // je crée les distributeurs du dataset
        ///         Hashtable htDistributeur1 = new Hashtable();
        ///         htDistributeur1.Add("distributorAbbreviation", new SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation1"));
        ///         htDistributeur1.Add("distributorAffiliation", new SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation1"));
        ///         htDistributeur1.Add("distributorLogoURL", new SimplePrimitiveNode("distributorLogoURL", "distributorLogoURL1"));
        ///         htDistributeur1.Add("distributorName", new SimplePrimitiveNode("distributorName", "distributorName1"));
        ///         htDistributeur1.Add("distributorURL", new SimplePrimitiveNode("distributorURL", "distributorURL1"));
        ///
        ///         Hashtable htDistributeur2 = new Hashtable();
        ///         htDistributeur2.Add("distributorAbbreviation", new SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation2"));
        ///         htDistributeur2.Add("distributorAffiliation", new SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation2"));
        ///         htDistributeur2.Add("distributorLogoURL", new SimplePrimitiveNode("distributorLogoURL", "distributorLogoURL2"));
        ///         htDistributeur2.Add("distributorName", new SimplePrimitiveNode("distributorName", "distributorName2"));
        ///         htDistributeur2.Add("distributorURL", new SimplePrimitiveNode("distributorURL", "distributorURL2"));
        ///
        ///         // je crée les grant number du dataset
        ///         Hashtable htGrantNumber1 = new Hashtable();
        ///         htGrantNumber1.Add("grantNumberAgency", new SimplePrimitiveNode("grantNumberAgency", "grantAgency1"));
        ///         htGrantNumber1.Add("grantNumberValue", new SimplePrimitiveNode("grantNumberValue", "grantNumber1"));
        ///
        ///         Hashtable htGrantNumber2 = new Hashtable();
        ///         htGrantNumber2.Add("grantNumberAgency", new SimplePrimitiveNode("grantNumberAgency", "grantAgency2"));
        ///         htGrantNumber2.Add("grantNumberValue", new SimplePrimitiveNode("grantNumberValue", "grantNumber2"));
        ///
        ///         // je crée les logiciels du dataset
        ///         Hashtable htLogiciel1 = new Hashtable();
        ///         htLogiciel1.Add("softwareName", new SimplePrimitiveNode("softwareName", "softwareName1"));
        ///         htLogiciel1.Add("softwareVersion", new SimplePrimitiveNode("softwareVersion", "softwareVersion1"));
        ///
        ///         Hashtable htLogiciel2 = new Hashtable();
        ///         htLogiciel2.Add("softwareName", new SimplePrimitiveNode("softwareName", "softwareName2"));
        ///         htLogiciel2.Add("softwareVersion", new SimplePrimitiveNode("softwareVersion", "softwareVersion2"));
        ///
        ///         // je crée les mots clés du dataset
        ///         Hashtable htMotCle1 = new Hashtable();
        ///         htMotCle1.Add("keywordTermURI", new SimplePrimitiveNode("keywordTermURI", "keywordTermURI1"));
        ///         htMotCle1.Add("keywordValue", new SimplePrimitiveNode("keywordValue", "keywordValue1"));
        ///         htMotCle1.Add("keywordVocabulary", new SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary1"));
        ///         htMotCle1.Add("keywordVocabularyURI", new SimplePrimitiveNode("keywordVocabularyURI", "keywordVocabularyURI1"));
        ///
        ///         Hashtable htMotCle2 = new Hashtable();
        ///         htMotCle2.Add("keywordTermURI", new SimplePrimitiveNode("keywordTermURI", "keywordTermURI2"));
        ///         htMotCle2.Add("keywordValue", new SimplePrimitiveNode("keywordValue", "keywordValue2"));
        ///         htMotCle2.Add("keywordVocabulary", new SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary2"));
        ///         htMotCle2.Add("keywordVocabularyURI", new SimplePrimitiveNode("keywordVocabularyURI", "keywordVocabularyURI2"));
        ///
        ///         // je crée les producteurs du dataset
        ///         Hashtable htProducteur1 = new Hashtable();
        ///         htProducteur1.Add("producerAbbreviation", new SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation1"));
        ///         htProducteur1.Add("producerAffiliation", new SimplePrimitiveNode("producerAffiliation", "producerAffiliation1"));
        ///         htProducteur1.Add("producerLogoURL", new SimplePrimitiveNode("producerLogoURL", "producerLogoURL1"));
        ///         htProducteur1.Add("producerName", new SimplePrimitiveNode("producerName", "producerName1"));
        ///         htProducteur1.Add("producerURL", new SimplePrimitiveNode("producerURL", "producerURL1"));
        ///
        ///         Hashtable htProducteur2 = new Hashtable();
        ///         htProducteur2.Add("producerAbbreviation", new SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation2"));
        ///         htProducteur2.Add("producerAffiliation", new SimplePrimitiveNode("producerAffiliation", "producerAffiliation2"));
        ///         htProducteur2.Add("producerLogoURL", new SimplePrimitiveNode("producerLogoURL", "producerLogoURL2"));
        ///         htProducteur2.Add("producerName", new SimplePrimitiveNode("producerName", "producerName2"));
        ///         htProducteur2.Add("producerURL", new SimplePrimitiveNode("producerURL", "producerURL2"));
        ///
        ///         // je crée la période du dataset
        ///         Hashtable htPeriode1 = new Hashtable();
        ///         htPeriode1.Add("timePeriodCoveredStart", new SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-01"));
        ///         htPeriode1.Add("timePeriodCoveredEnd", new SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-02"));
        ///
        ///         Hashtable htPeriode2 = new Hashtable();
        ///         htPeriode2.Add("timePeriodCoveredStart", new SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-03"));
        ///         htPeriode2.Add("timePeriodCoveredEnd", new SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-04"));
        ///
        ///         // je crée le projet du dataset
        ///         Hashtable htProjet = new Hashtable();
        ///         htProjet.Add("projectAcronym", new SimplePrimitiveNode("projectAcronym", "projectAcronym"));
        ///         htProjet.Add("projectId", new SimplePrimitiveNode("projectId", "projectId"));
        ///         htProjet.Add("projectTask", new SimplePrimitiveNode("projectTask", "projectTask"));
        ///         htProjet.Add("projectTitle", new SimplePrimitiveNode("projectTitle", "projectTitle"));
        ///         htProjet.Add("projectURL", new SimplePrimitiveNode("projectURL", "http://project.url"));
        ///         htProjet.Add("projectWorkPackage", new SimplePrimitiveNode("projectWorkPackage", "projectWP"));
        ///
        ///         // je crée les producteurs du dataset
        ///         Hashtable htPublication1 = new Hashtable();
        ///         htPublication1.Add("publicationCitation", new SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation1"));
        ///         htPublication1.Add("publicationIDNumber", new SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber1"));
        ///         htPublication1.Add("publicationIDType", new SimpleControlledVocabularyNode("publicationIDType", "ark"));
        ///         htPublication1.Add("publicationURL", new SimplePrimitiveNode("publicationURL", "http://related.publication.url1"));
        ///
        ///         Hashtable htPublication2 = new Hashtable();
        ///         htPublication2.Add("publicationCitation", new SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation2"));
        ///         htPublication2.Add("publicationIDNumber", new SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber2"));
        ///         htPublication2.Add("publicationIDType", new SimpleControlledVocabularyNode("publicationIDType", "arXiv"));
        ///         htPublication2.Add("publicationURL", new SimplePrimitiveNode("publicationURL", "http://related.publication.url2"));
        ///
        ///         // je crée les relatedDataset du dataset
        ///         Hashtable htRelatedDataset1 = new Hashtable();
        ///         htRelatedDataset1.Add("relatedDatasetCitation", new SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation1"));
        ///         htRelatedDataset1.Add("relatedDatasetIDNumber", new SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber1"));
        ///         htRelatedDataset1.Add("relatedDatasetIDType", new SimpleControlledVocabularyNode("relatedDatasetIDType", "ark"));
        ///         htRelatedDataset1.Add("relatedDatasetURL", new SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url1"));
        ///
        ///         Hashtable htRelatedDataset2 = new Hashtable();
        ///         htRelatedDataset2.Add("relatedDatasetCitation", new SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation2"));
        ///         htRelatedDataset2.Add("relatedDatasetIDNumber", new SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber2"));
        ///         htRelatedDataset2.Add("relatedDatasetIDType", new SimpleControlledVocabularyNode("relatedDatasetIDType", "doi"));
        ///         htRelatedDataset2.Add("relatedDatasetURL", new SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url2"));
        ///
        ///         // je crée la série du dataset
        ///         Hashtable htSerie = new Hashtable();
        ///         htSerie.Add("seriesInformation", new SimplePrimitiveNode("seriesInformation", "seriesInformation"));
        ///         htSerie.Add("seriesName", new SimplePrimitiveNode("seriesName", "seriesName"));
        ///
        ///         // je crée les topics classifications du dataset
        ///         Hashtable htTopicClassification1 = new Hashtable();
        ///         htTopicClassification1.Add("topicClassValue", new SimplePrimitiveNode("topicClassValue", "topicClassValue1"));
        ///         htTopicClassification1.Add("topicClassVocab", new SimplePrimitiveNode("topicClassVocab", "topicClassVocab1"));
        ///         htTopicClassification1.Add("topicClassVocabURI", new SimplePrimitiveNode("topicClassVocabURI", "topicClassVocabURI1"));
        ///
        ///         Hashtable htTopicClassification2 = new Hashtable();
        ///         htTopicClassification2.Add("topicClassValue", new SimplePrimitiveNode("topicClassValue", "topicClassValue2"));
        ///         htTopicClassification2.Add("topicClassVocab", new SimplePrimitiveNode("topicClassVocab", "topicClassVocab2"));
        ///         htTopicClassification2.Add("topicClassVocabURI", new SimplePrimitiveNode("topicClassVocabURI", "topicClassVocabURI2"));
        /// 
        ///         // je crée un node citation
        ///         CitationNode cn = new CitationNode();
        ///         cn.AddField(new SimplePrimitiveNode("title", "title"));
        ///         cn.AddField(new SimplePrimitiveNode("subtitle", "subtitle"));
        ///         cn.AddField(new SimplePrimitiveNode("alternativeTitle", "alternativeTitle"));
        ///         cn.AddField(new SimplePrimitiveNode("alternativeURL", "http://link.to.data"));
        ///         cn.AddField(new MultipleCompoundNode("otherId", new ArrayList() { htAutreId1, htAutreId2 }));
        ///         cn.AddField(new MultipleCompoundNode("datasetContact", new ArrayList() { htContact1, htContact2 }));
        ///         cn.AddField(new MultipleCompoundNode("author", new ArrayList() { htAuteur1, htAuteur2 }));
        ///         cn.AddField(new MultipleCompoundNode("contributor", new ArrayList() { htContributeur1, htContributeur2 }));
        ///         cn.AddField(new MultipleCompoundNode("producer", new ArrayList() { htProducteur1, htProducteur2 }));
        ///         cn.AddField(new SimplePrimitiveNode("productionDate", "2000-01-01"));
        ///         cn.AddField(new SimplePrimitiveNode("productionPlace", "productionPlace"));
        ///         cn.AddField(new MultipleCompoundNode("distributor", new ArrayList() { htDistributeur1, htDistributeur2 }));
        ///         cn.AddField(new SimplePrimitiveNode("distributionDate", "2000-01-01"));
        ///         cn.AddField(new MultipleCompoundNode("dsDescription", new ArrayList() { htDescription1, htDescription2 }));
        ///         cn.AddField(new MultipleControlledVocabularyNode("language", new List<String>() { "Abkhaz", "Afar" }));
        ///         cn.AddField(new MultipleControlledVocabularyNode("subject", new List<String>() { "Animal Breeding and Animal Products", "Animal Health and Pathology" }));
        ///         cn.AddField(new MultipleCompoundNode("keyword", new ArrayList() { htMotCle1, htMotCle1 }));
        ///         cn.AddField(new MultipleCompoundNode("topicClassification", new ArrayList() { htTopicClassification1, htTopicClassification2 }));
        ///         cn.AddField(new MultipleControlledVocabularyNode("kindOfData", new List<String>() { "Audiovisual", "Collection" }));
        ///         cn.AddField(new MultiplePrimitiveNode("kindOfDataOther", new List<String>() { "otherKindOfData1", "otherKindOfData2" }));
        ///         cn.AddField(new MultipleControlledVocabularyNode("dataOrigin", new List<String>() { "observational data", "experimental data" }));
        ///         cn.AddField(new MultiplePrimitiveNode("dataSources", new List<String>() { "dataSource1", "dataSource2" }));
        ///         cn.AddField(new SimplePrimitiveNode("originOfSources", "originOfSource"));
        ///         cn.AddField(new SimplePrimitiveNode("characteristicOfSources", "characteristicOfSource"));
        ///         cn.AddField(new SimplePrimitiveNode("accessToSources", "accessToSource"));
        ///         cn.AddField(new MultipleCompoundNode("software", new ArrayList() { htLogiciel1, htLogiciel2 }));
        ///         cn.AddField(new SimpleCompoundNode("series", htSerie));
        ///         cn.AddField(new MultipleControlledVocabularyNode("lifeCycleStep", new List<String>() { "Study proposal", "Funding" }));
        ///         cn.AddField(new SimplePrimitiveNode("notesText", "notes"));
        ///         cn.AddField(new MultipleCompoundNode("publication", new ArrayList() { htPublication1, htPublication2 }));
        ///         cn.AddField(new MultiplePrimitiveNode("relatedMaterial", new List<String>() { "relatedMaterial1", "relatedMaterial2" }));
        ///         cn.AddField(new MultipleCompoundNode("relatedDataset", new ArrayList() { htRelatedDataset1, htRelatedDataset2 }));
        ///         cn.AddField(new MultiplePrimitiveNode("otherReferences", new List<String>() { "otherReference1", "otherReference2" }));
        ///         cn.AddField(new MultipleCompoundNode("grantNumber", new ArrayList() { htGrantNumber1, htGrantNumber2 }));
        ///         cn.AddField(new SimpleCompoundNode("project", htProjet));
        ///         cn.AddField(new MultipleCompoundNode("timePeriodCovered", new ArrayList() { htPeriode1, htPeriode2 }));
        ///         cn.AddField(new MultipleCompoundNode("dateOfCollection", new ArrayList() { htDates1, htDates2 }));
        ///         cn.AddField(new SimplePrimitiveNode("depositor", "depositor"));
        ///         cn.AddField(new SimplePrimitiveNode("dateOfDeposit", "2000-01-01"));
        ///
        ///         Console.Write(cn.toJSON());
        ///     </code>
        /// </example>
        public static void HowToBuildACitationNode()
        {
            Console.Write
            (
                Environment.NewLine + "// je crée les auteurs du dataset" +
                Environment.NewLine + "Hashtable htAuteur1 = new Hashtable();" +
                Environment.NewLine + "htAuteur1.Add(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"authorAffiliation1\"));" +
                Environment.NewLine + "htAuteur1.Add(\"authorIdentifier\", new SimplePrimitiveNode(\"authorIdentifier\", \"authorIdentifier1\"));" +
                Environment.NewLine + "htAuteur1.Add(\"authorIdentifierScheme\", new SimpleControlledVocabularyNode(\"authorIdentifierScheme\", \"ORCID\"));" +
                Environment.NewLine + "htAuteur1.Add(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"authorName1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htAuteur2 = new Hashtable();" +
                Environment.NewLine + "htAuteur2.Add(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"authorAffiliation2\"));" +
                Environment.NewLine + "htAuteur2.Add(\"authorIdentifier\", new SimplePrimitiveNode(\"authorIdentifier\", \"authorIdentifier2\"));" +
                Environment.NewLine + "htAuteur2.Add(\"authorIdentifierScheme\", new SimpleControlledVocabularyNode(\"authorIdentifierScheme\", \"idHAL\"));" +
                Environment.NewLine + "htAuteur2.Add(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"authorName2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les autres identifiants" +
                Environment.NewLine + "Hashtable htAutreId1 = new Hashtable();" +
                Environment.NewLine + "htAutreId1.Add(\"otherIdAgency\", new SimplePrimitiveNode(\"otherIdAgency\", \"otherIdAgency1\"));" +
                Environment.NewLine + "htAutreId1.Add(\"otherIdValue\", new SimplePrimitiveNode(\"otherIdValue\", \"otherIdAgencyIdentifier1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htAutreId2 = new Hashtable();" +
                Environment.NewLine + "htAutreId2.Add(\"otherIdAgency\", new SimplePrimitiveNode(\"otherIdAgency\", \"otherIdAgency2\"));" +
                Environment.NewLine + "htAutreId2.Add(\"otherIdValue\", new SimplePrimitiveNode(\"otherIdValue\", \"otherIdAgencyIdentifier2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les contacts du dataset" +
                Environment.NewLine + "Hashtable htContact1 = new Hashtable();" +
                Environment.NewLine + "htContact1.Add(\"datasetContactAffiliation\", new SimplePrimitiveNode(\"datasetContactAffiliation\", \"contactAffiliation1\"));" +
                Environment.NewLine + "htContact1.Add(\"datasetContactEmail\", new SimplePrimitiveNode(\"datasetContactEmail\", \"contact1@mail.com\"));" +
                Environment.NewLine + "htContact1.Add(\"datasetContactName\", new SimplePrimitiveNode(\"datasetContactName\", \"contactName1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htContact2 = new Hashtable();" +
                Environment.NewLine + "htContact2.Add(\"datasetContactAffiliation\", new SimplePrimitiveNode(\"datasetContactAffiliation\", \"contactAffiliation2\"));" +
                Environment.NewLine + "htContact2.Add(\"datasetContactEmail\", new SimplePrimitiveNode(\"datasetContactEmail\", \"contact2@mail.com\"));" +
                Environment.NewLine + "htContact2.Add(\"datasetContactName\", new SimplePrimitiveNode(\"datasetContactName\", \"contactName2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les contributeurs du dataset" +
                Environment.NewLine + "Hashtable htContributeur1 = new Hashtable();" +
                Environment.NewLine + "htContributeur1.Add(\"contributorAffiliation\", new SimplePrimitiveNode(\"contributorAffiliation\", \"contributorAffiliation1\"));" +
                Environment.NewLine + "htContributeur1.Add(\"contributorIdentifier\", new SimplePrimitiveNode(\"contributorIdentifier\", \"contributorIdentifier1\"));" +
                Environment.NewLine + "htContributeur1.Add(\"contributorIdentifierScheme\", new SimpleControlledVocabularyNode(\"contributorIdentifierScheme\", \"ORCID\"));" +
                Environment.NewLine + "htContributeur1.Add(\"contributorName\", new SimplePrimitiveNode(\"contributorName\", \"contributorName1\"));" +
                Environment.NewLine + "htContributeur1.Add(\"contributorType\", new SimpleControlledVocabularyNode(\"contributorType\", \"Data collector\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htContributeur2 = new Hashtable();" +
                Environment.NewLine + "htContributeur2.Add(\"contributorAffiliation\", new SimplePrimitiveNode(\"contributorAffiliation\", \"contributorAffiliation2\"));" +
                Environment.NewLine + "htContributeur2.Add(\"contributorIdentifier\", new SimplePrimitiveNode(\"contributorIdentifier\", \"contributorIdentifier2\"));" +
                Environment.NewLine + "htContributeur2.Add(\"contributorIdentifierScheme\", new SimpleControlledVocabularyNode(\"contributorIdentifierScheme\", \"ORCID\"));" +
                Environment.NewLine + "htContributeur2.Add(\"contributorName\", new SimplePrimitiveNode(\"contributorName\", \"contributorName2\"));" +
                Environment.NewLine + "htContributeur2.Add(\"contributorType\", new SimpleControlledVocabularyNode(\"contributorType\", \"Editor\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les dates des collections du dataset" +
                Environment.NewLine + "Hashtable htDates1 = new Hashtable();" +
                Environment.NewLine + "htDates1.Add(\"dateOfCollectionStart\", new SimplePrimitiveNode(\"dateOfCollectionStart\", \"2000-01-01\"));" +
                Environment.NewLine + "htDates1.Add(\"dateOfCollectionEnd\", new SimplePrimitiveNode(\"dateOfCollectionEnd\", \"2000-01-02\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htDates2 = new Hashtable();" +
                Environment.NewLine + "htDates2.Add(\"dateOfCollectionStart\", new SimplePrimitiveNode(\"dateOfCollectionStart\", \"2000-01-03\"));" +
                Environment.NewLine + "htDates2.Add(\"dateOfCollectionEnd\", new SimplePrimitiveNode(\"dateOfCollectionEnd\", \"2000-01-04\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les descriptions du dataset" +
                Environment.NewLine + "Hashtable htDescription1 = new Hashtable();" +
                Environment.NewLine + "htDescription1.Add(\"dsDescriptionDate\", new SimplePrimitiveNode(\"dsDescriptionDate\", \"2000-01-01\"));" +
                Environment.NewLine + "htDescription1.Add(\"dsDescriptionValue\", new SimplePrimitiveNode(\"dsDescriptionValue\", \"descriptionText1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htDescription2 = new Hashtable();" +
                Environment.NewLine + "htDescription2.Add(\"dsDescriptionDate\", new SimplePrimitiveNode(\"dsDescriptionDate\", \"2000-01-01\"));" +
                Environment.NewLine + "htDescription2.Add(\"dsDescriptionValue\", new SimplePrimitiveNode(\"dsDescriptionValue\", \"descriptionText2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les distributeurs du dataset" +
                Environment.NewLine + "Hashtable htDistributeur1 = new Hashtable();" +
                Environment.NewLine + "htDistributeur1.Add(\"distributorAbbreviation\", new SimplePrimitiveNode(\"distributorAbbreviation\", \"distributorAbbreviation1\"));" +
                Environment.NewLine + "htDistributeur1.Add(\"distributorAffiliation\", new SimplePrimitiveNode(\"distributorAffiliation\", \"distributorAffiliation1\"));" +
                Environment.NewLine + "htDistributeur1.Add(\"distributorLogoURL\", new SimplePrimitiveNode(\"distributorLogoURL\", \"distributorLogoURL1\"));" +
                Environment.NewLine + "htDistributeur1.Add(\"distributorName\", new SimplePrimitiveNode(\"distributorName\", \"distributorName1\"));" +
                Environment.NewLine + "htDistributeur1.Add(\"distributorURL\", new SimplePrimitiveNode(\"distributorURL\", \"distributorURL1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htDistributeur2 = new Hashtable();" +
                Environment.NewLine + "htDistributeur2.Add(\"distributorAbbreviation\", new SimplePrimitiveNode(\"distributorAbbreviation\", \"distributorAbbreviation2\"));" +
                Environment.NewLine + "htDistributeur2.Add(\"distributorAffiliation\", new SimplePrimitiveNode(\"distributorAffiliation\", \"distributorAffiliation2\"));" +
                Environment.NewLine + "htDistributeur2.Add(\"distributorLogoURL\", new SimplePrimitiveNode(\"distributorLogoURL\", \"distributorLogoURL2\"));" +
                Environment.NewLine + "htDistributeur2.Add(\"distributorName\", new SimplePrimitiveNode(\"distributorName\", \"distributorName2\"));" +
                Environment.NewLine + "htDistributeur2.Add(\"distributorURL\", new SimplePrimitiveNode(\"distributorURL\", \"distributorURL2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les grant number du dataset" +
                Environment.NewLine + "Hashtable htGrantNumber1 = new Hashtable();" +
                Environment.NewLine + "htGrantNumber1.Add(\"grantNumberAgency\", new SimplePrimitiveNode(\"grantNumberAgency\", \"grantAgency1\"));" +
                Environment.NewLine + "htGrantNumber1.Add(\"grantNumberValue\", new SimplePrimitiveNode(\"grantNumberValue\", \"grantNumber1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htGrantNumber2 = new Hashtable();" +
                Environment.NewLine + "htGrantNumber2.Add(\"grantNumberAgency\", new SimplePrimitiveNode(\"grantNumberAgency\", \"grantAgency2\"));" +
                Environment.NewLine + "htGrantNumber2.Add(\"grantNumberValue\", new SimplePrimitiveNode(\"grantNumberValue\", \"grantNumber2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les logiciels du dataset" +
                Environment.NewLine + "Hashtable htLogiciel1 = new Hashtable();" +
                Environment.NewLine + "htLogiciel1.Add(\"softwareName\", new SimplePrimitiveNode(\"softwareName\", \"softwareName1\"));" +
                Environment.NewLine + "htLogiciel1.Add(\"softwareVersion\", new SimplePrimitiveNode(\"softwareVersion\", \"softwareVersion1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htLogiciel2 = new Hashtable();" +
                Environment.NewLine + "htLogiciel2.Add(\"softwareName\", new SimplePrimitiveNode(\"softwareName\", \"softwareName2\"));" +
                Environment.NewLine + "htLogiciel2.Add(\"softwareVersion\", new SimplePrimitiveNode(\"softwareVersion\", \"softwareVersion2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les mots clés du dataset" +
                Environment.NewLine + "Hashtable htMotCle1 = new Hashtable();" +
                Environment.NewLine + "htMotCle1.Add(\"keywordTermURI\", new SimplePrimitiveNode(\"keywordTermURI\", \"keywordTermURI1\"));" +
                Environment.NewLine + "htMotCle1.Add(\"keywordValue\", new SimplePrimitiveNode(\"keywordValue\", \"keywordValue1\"));" +
                Environment.NewLine + "htMotCle1.Add(\"keywordVocabulary\", new SimplePrimitiveNode(\"keywordVocabulary\", \"keywordVocabulary1\"));" +
                Environment.NewLine + "htMotCle1.Add(\"keywordVocabularyURI\", new SimplePrimitiveNode(\"keywordVocabularyURI\", \"keywordVocabularyURI1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htMotCle2 = new Hashtable();" +
                Environment.NewLine + "htMotCle2.Add(\"keywordTermURI\", new SimplePrimitiveNode(\"keywordTermURI\", \"keywordTermURI2\"));" +
                Environment.NewLine + "htMotCle2.Add(\"keywordValue\", new SimplePrimitiveNode(\"keywordValue\", \"keywordValue2\"));" +
                Environment.NewLine + "htMotCle2.Add(\"keywordVocabulary\", new SimplePrimitiveNode(\"keywordVocabulary\", \"keywordVocabulary2\"));" +
                Environment.NewLine + "htMotCle2.Add(\"keywordVocabularyURI\", new SimplePrimitiveNode(\"keywordVocabularyURI\", \"keywordVocabularyURI2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les producteurs du dataset" +
                Environment.NewLine + "Hashtable htProducteur1 = new Hashtable();" +
                Environment.NewLine + "htProducteur1.Add(\"producerAbbreviation\", new SimplePrimitiveNode(\"producerAbbreviation\", \"producerAbbreviation1\"));" +
                Environment.NewLine + "htProducteur1.Add(\"producerAffiliation\", new SimplePrimitiveNode(\"producerAffiliation\", \"producerAffiliation1\"));" +
                Environment.NewLine + "htProducteur1.Add(\"producerLogoURL\", new SimplePrimitiveNode(\"producerLogoURL\", \"producerLogoURL1\"));" +
                Environment.NewLine + "htProducteur1.Add(\"producerName\", new SimplePrimitiveNode(\"producerName\", \"producerName1\"));" +
                Environment.NewLine + "htProducteur1.Add(\"producerURL\", new SimplePrimitiveNode(\"producerURL\", \"producerURL1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htProducteur2 = new Hashtable();" +
                Environment.NewLine + "htProducteur2.Add(\"producerAbbreviation\", new SimplePrimitiveNode(\"producerAbbreviation\", \"producerAbbreviation2\"));" +
                Environment.NewLine + "htProducteur2.Add(\"producerAffiliation\", new SimplePrimitiveNode(\"producerAffiliation\", \"producerAffiliation2\"));" +
                Environment.NewLine + "htProducteur2.Add(\"producerLogoURL\", new SimplePrimitiveNode(\"producerLogoURL\", \"producerLogoURL2\"));" +
                Environment.NewLine + "htProducteur2.Add(\"producerName\", new SimplePrimitiveNode(\"producerName\", \"producerName2\"));" +
                Environment.NewLine + "htProducteur2.Add(\"producerURL\", new SimplePrimitiveNode(\"producerURL\", \"producerURL2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée la période du dataset" +
                Environment.NewLine + "Hashtable htPeriode1 = new Hashtable();" +
                Environment.NewLine + "htPeriode1.Add(\"timePeriodCoveredStart\", new SimplePrimitiveNode(\"timePeriodCoveredStart\", \"2000-01-01\"));" +
                Environment.NewLine + "htPeriode1.Add(\"timePeriodCoveredEnd\", new SimplePrimitiveNode(\"timePeriodCoveredEnd\", \"2000-01-02\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htPeriode2 = new Hashtable();" +
                Environment.NewLine + "htPeriode2.Add(\"timePeriodCoveredStart\", new SimplePrimitiveNode(\"timePeriodCoveredStart\", \"2000-01-03\"));" +
                Environment.NewLine + "htPeriode2.Add(\"timePeriodCoveredEnd\", new SimplePrimitiveNode(\"timePeriodCoveredEnd\", \"2000-01-04\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée le projet du dataset" +
                Environment.NewLine + "Hashtable htProjet = new Hashtable();" +
                Environment.NewLine + "htProjet.Add(\"projectAcronym\", new SimplePrimitiveNode(\"projectAcronym\", \"projectAcronym\"));" +
                Environment.NewLine + "htProjet.Add(\"projectId\", new SimplePrimitiveNode(\"projectId\", \"projectId\"));" +
                Environment.NewLine + "htProjet.Add(\"projectTask\", new SimplePrimitiveNode(\"projectTask\", \"projectTask\"));" +
                Environment.NewLine + "htProjet.Add(\"projectTitle\", new SimplePrimitiveNode(\"projectTitle\", \"projectTitle\"));" +
                Environment.NewLine + "htProjet.Add(\"projectURL\", new SimplePrimitiveNode(\"projectURL\", \"http://project.url\"));" +
                Environment.NewLine + "htProjet.Add(\"projectWorkPackage\", new SimplePrimitiveNode(\"projectWorkPackage\", \"projectWP\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les producteurs du dataset" +
                Environment.NewLine + "Hashtable htPublication1 = new Hashtable();" +
                Environment.NewLine + "htPublication1.Add(\"publicationCitation\", new SimplePrimitiveNode(\"publicationCitation\", \"relatedPublicationCitation1\"));" +
                Environment.NewLine + "htPublication1.Add(\"publicationIDNumber\", new SimplePrimitiveNode(\"publicationIDNumber\", \"relatedPublicationIDNumber1\"));" +
                Environment.NewLine + "htPublication1.Add(\"publicationIDType\", new SimpleControlledVocabularyNode(\"publicationIDType\", \"ark\"));" +
                Environment.NewLine + "htPublication1.Add(\"publicationURL\", new SimplePrimitiveNode(\"publicationURL\", \"http://related.publication.url1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htPublication2 = new Hashtable();" +
                Environment.NewLine + "htPublication2.Add(\"publicationCitation\", new SimplePrimitiveNode(\"publicationCitation\", \"relatedPublicationCitation2\"));" +
                Environment.NewLine + "htPublication2.Add(\"publicationIDNumber\", new SimplePrimitiveNode(\"publicationIDNumber\", \"relatedPublicationIDNumber2\"));" +
                Environment.NewLine + "htPublication2.Add(\"publicationIDType\", new SimpleControlledVocabularyNode(\"publicationIDType\", \"arXiv\"));" +
                Environment.NewLine + "htPublication2.Add(\"publicationURL\", new SimplePrimitiveNode(\"publicationURL\", \"http://related.publication.url2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les relatedDataset du dataset" +
                Environment.NewLine + "Hashtable htRelatedDataset1 = new Hashtable();" +
                Environment.NewLine + "htRelatedDataset1.Add(\"relatedDatasetCitation\", new SimplePrimitiveNode(\"relatedDatasetCitation\", \"relatedDatasetCitation1\"));" +
                Environment.NewLine + "htRelatedDataset1.Add(\"relatedDatasetIDNumber\", new SimplePrimitiveNode(\"relatedDatasetIDNumber\", \"relatedDatasetIDNumber1\"));" +
                Environment.NewLine + "htRelatedDataset1.Add(\"relatedDatasetIDType\", new SimpleControlledVocabularyNode(\"relatedDatasetIDType\", \"ark\"));" +
                Environment.NewLine + "htRelatedDataset1.Add(\"relatedDatasetURL\", new SimplePrimitiveNode(\"relatedDatasetURL\", \"http://related.dataset.url1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htRelatedDataset2 = new Hashtable();" +
                Environment.NewLine + "htRelatedDataset2.Add(\"relatedDatasetCitation\", new SimplePrimitiveNode(\"relatedDatasetCitation\", \"relatedDatasetCitation2\"));" +
                Environment.NewLine + "htRelatedDataset2.Add(\"relatedDatasetIDNumber\", new SimplePrimitiveNode(\"relatedDatasetIDNumber\", \"relatedDatasetIDNumber2\"));" +
                Environment.NewLine + "htRelatedDataset2.Add(\"relatedDatasetIDType\", new SimpleControlledVocabularyNode(\"relatedDatasetIDType\", \"doi\"));" +
                Environment.NewLine + "htRelatedDataset2.Add(\"relatedDatasetURL\", new SimplePrimitiveNode(\"relatedDatasetURL\", \"http://related.dataset.url2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée la série du dataset" +
                Environment.NewLine + "Hashtable htSerie = new Hashtable();" +
                Environment.NewLine + "htSerie.Add(\"seriesInformation\", new SimplePrimitiveNode(\"seriesInformation\", \"seriesInformation\"));" +
                Environment.NewLine + "htSerie.Add(\"seriesName\", new SimplePrimitiveNode(\"seriesName\", \"seriesName\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée les topics classifications du dataset" +
                Environment.NewLine + "Hashtable htTopicClassification1 = new Hashtable();" +
                Environment.NewLine + "htTopicClassification1.Add(\"topicClassValue\", new SimplePrimitiveNode(\"topicClassValue\", \"topicClassValue1\"));" +
                Environment.NewLine + "htTopicClassification1.Add(\"topicClassVocab\", new SimplePrimitiveNode(\"topicClassVocab\", \"topicClassVocab1\"));" +
                Environment.NewLine + "htTopicClassification1.Add(\"topicClassVocabURI\", new SimplePrimitiveNode(\"topicClassVocabURI\", \"topicClassVocabURI1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htTopicClassification2 = new Hashtable();" +
                Environment.NewLine + "htTopicClassification2.Add(\"topicClassValue\", new SimplePrimitiveNode(\"topicClassValue\", \"topicClassValue2\"));" +
                Environment.NewLine + "htTopicClassification2.Add(\"topicClassVocab\", new SimplePrimitiveNode(\"topicClassVocab\", \"topicClassVocab2\"));" +
                Environment.NewLine + "htTopicClassification2.Add(\"topicClassVocabURI\", new SimplePrimitiveNode(\"topicClassVocabURI\", \"topicClassVocabURI2\"));" +
                Environment.NewLine +
                Environment.NewLine +
                Environment.NewLine +
                Environment.NewLine + "// je crée un node citation" +
                Environment.NewLine + "CitationNode cn = new CitationNode();" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"title\", \"title\"));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"subtitle\", \"subtitle\"));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"alternativeTitle\", \"alternativeTitle\"));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"alternativeURL\", \"http://link.to.data\"));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"otherId\", new ArrayList() { htAutreId1, htAutreId2 }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"datasetContact\", new ArrayList() { htContact1, htContact2 }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"author\", new ArrayList() { htAuteur1, htAuteur2 }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"contributor\", new ArrayList() { htContributeur1, htContributeur2 }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"producer\", new ArrayList() { htProducteur1, htProducteur2 }));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"productionDate\", \"2000-01-01\"));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"productionPlace\", \"productionPlace\"));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"distributor\", new ArrayList() { htDistributeur1, htDistributeur2 }));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"distributionDate\", \"2000-01-01\"));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"dsDescription\", new ArrayList() { htDescription1, htDescription2 }));" +
                Environment.NewLine + "cn.AddField(new MultipleControlledVocabularyNode(\"language\", new List<String>() { \"Abkhaz\", \"Afar\" }));" +
                Environment.NewLine + "cn.AddField(new MultipleControlledVocabularyNode(\"subject\", new List<String>() { \"Animal Breeding and Animal Products\", \"Animal Health and Pathology\" }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"keyword\", new ArrayList() { htMotCle1, htMotCle1 }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"topicClassification\", new ArrayList() { htTopicClassification1, htTopicClassification2 }));" +
                Environment.NewLine + "cn.AddField(new MultipleControlledVocabularyNode(\"kindOfData\", new List<String>() { \"Audiovisual\", \"Collection\" }));" +
                Environment.NewLine + "cn.AddField(new MultiplePrimitiveNode(\"kindOfDataOther\", new List<String>() { \"otherKindOfData1\", \"otherKindOfData2\" }));" +
                Environment.NewLine + "cn.AddField(new MultipleControlledVocabularyNode(\"dataOrigin\", new List<String>() { \"observational data\", \"experimental data\" }));" +
                Environment.NewLine + "cn.AddField(new MultiplePrimitiveNode(\"dataSources\", new List<String>() { \"dataSource1\", \"dataSource2\" }));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"originOfSources\", \"originOfSource\"));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"characteristicOfSources\", \"characteristicOfSource\"));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"accessToSources\", \"accessToSource\"));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"software\", new ArrayList() { htLogiciel1, htLogiciel2 }));" +
                Environment.NewLine + "cn.AddField(new SimpleCompoundNode(\"series\", htSerie));" +
                Environment.NewLine + "cn.AddField(new MultipleControlledVocabularyNode(\"lifeCycleStep\", new List<String>() { \"Study proposal\", \"Funding\" }));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"notesText\", \"notes\"));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"publication\", new ArrayList() { htPublication1, htPublication2 }));" +
                Environment.NewLine + "cn.AddField(new MultiplePrimitiveNode(\"relatedMaterial\", new List<String>() { \"relatedMaterial1\", \"relatedMaterial2\" }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"relatedDataset\", new ArrayList() { htRelatedDataset1, htRelatedDataset2 }));" +
                Environment.NewLine + "cn.AddField(new MultiplePrimitiveNode(\"otherReferences\", new List<String>() { \"otherReference1\", \"otherReference2\" }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"grantNumber\", new ArrayList() { htGrantNumber1, htGrantNumber2 }));" +
                Environment.NewLine + "cn.AddField(new SimpleCompoundNode(\"project\", htProjet));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"timePeriodCovered\", new ArrayList() { htPeriode1, htPeriode2 }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"dateOfCollection\", new ArrayList() { htDates1, htDates2 }));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"depositor\", \"depositor\"));" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"dateOfDeposit\", \"2000-01-01\"));" +
                Environment.NewLine +
                Environment.NewLine + "Console.Write(cn.toJSON());"
            );
        }

        /// <summary><c>HowToBuildADerivedTextNode</c> montre la manière de construire un <c>DerivedTextNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         // je crée les sources du dataset
        ///         Hashtable htSource1 = new Hashtable();
        ///         htSource1.Add("ageOfSource", new MultiplePrimitiveNode("ageOfSource", new List<String>() { "sourceAge1" }));
        ///         htSource1.Add("citations", new MultiplePrimitiveNode("citations", new List<String>() { "1" }));
        ///         htSource1.Add("experimentNumber", new MultiplePrimitiveNode("experimentNumber", new List<String>() { "1" }));
        ///         htSource1.Add("typeOfSource", new MultiplePrimitiveNode("typeOfSource", new List<String>() { "sourceType1" }));
        ///         
        ///         Hashtable htSource2 = new Hashtable();
        ///         htSource2.Add("ageOfSource", new MultiplePrimitiveNode("ageOfSource", new List<String>() { "sourceAge2" }));
        ///         htSource2.Add("citations", new MultiplePrimitiveNode("citations", new List<String>() { "2" }));
        ///         htSource2.Add("experimentNumber", new MultiplePrimitiveNode("experimentNumber", new List<String>() { "2" }));
        ///         htSource2.Add("typeOfSource", new MultiplePrimitiveNode("typeOfSource", new List<String>() { "sourceType2" }));
        ///         
        ///         // je crée un node derived-text
        ///         DerivedTextNode dtn = new DerivedTextNode();
        ///         dtn.AddField(new MultipleCompoundNode("source", new ArrayList() { htSource1, htSource2 }));
        ///         
        ///         Console.Write(dtn.ToJSON();
        ///     </code>
        /// </example>
        public static void HowToBuildADerivedTextNode()
        {
            Console.Write
            (
                Environment.NewLine + "// je crée les sources du dataset" +
                Environment.NewLine + "Hashtable htSource1 = new Hashtable();" +
                Environment.NewLine + "htSource1.Add(\"ageOfSource\", new MultiplePrimitiveNode(\"ageOfSource\", new List<String>() { \"sourceAge1\" }));" +
                Environment.NewLine + "htSource1.Add(\"citations\", new MultiplePrimitiveNode(\"citations\", new List<String>() { \"1\" }));" +
                Environment.NewLine + "htSource1.Add(\"experimentNumber\", new MultiplePrimitiveNode(\"experimentNumber\", new List<String>() { \"1\" }));" +
                Environment.NewLine + "htSource1.Add(\"typeOfSource\", new MultiplePrimitiveNode(\"typeOfSource\", new List<String>() { \"sourceType1\" }));" +
                Environment.NewLine + "" +
                Environment.NewLine + "Hashtable htSource2 = new Hashtable();" +
                Environment.NewLine + "htSource2.Add(\"ageOfSource\", new MultiplePrimitiveNode(\"ageOfSource\", new List<String>() { \"sourceAge2\" }));" +
                Environment.NewLine + "htSource2.Add(\"citations\", new MultiplePrimitiveNode(\"citations\", new List<String>() { \"2\" }));" +
                Environment.NewLine + "htSource2.Add(\"experimentNumber\", new MultiplePrimitiveNode(\"experimentNumber\", new List<String>() { \"2\" }));" +
                Environment.NewLine + "htSource2.Add(\"typeOfSource\", new MultiplePrimitiveNode(\"typeOfSource\", new List<String>() { \"sourceType2\" }));" +
                Environment.NewLine + "" +
                Environment.NewLine + "// je crée un node derived-text" +
                Environment.NewLine + "DerivedTextNode dtn = new DerivedTextNode();" +
                Environment.NewLine + "dtn.AddField(new MultipleCompoundNode(\"source\", new ArrayList() { htSource1, htSource2 }));" +
                Environment.NewLine + "" +
                Environment.NewLine + "Console.Write(dtn.ToJSON();"
            );
        }

        /// <summary><c>HowToBuildAGeospatialNode</c> montre la manière de construire un <c>GeospatialNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         // je crée la conformite du dataset
        ///         Hashtable htConformite1 = new Hashtable();
        ///         htConformite1.Add("specification", new MultiplePrimitiveNode("specification", new List<String>() { "conformitySpecification1" }));
        ///         htConformite1.Add("degree", new SimpleControlledVocabularyNode("degree", "Conformant"));
        ///
        ///         Hashtable htConformite2 = new Hashtable();
        ///         htConformite2.Add("specification", new MultiplePrimitiveNode("specification", new List<String>() { "conformitySpecification2" }));
        ///         htConformite2.Add("degree", new SimpleControlledVocabularyNode("degree", "Not Conformant"));
        ///
        ///         // je crée la boite geographique du dataset
        ///         Hashtable htBoite1 = new Hashtable();
        ///         htBoite1.Add("eastLongitude", new SimplePrimitiveNode("eastLongitude", "0"));
        ///         htBoite1.Add("northLongitude", new SimplePrimitiveNode("northLongitude", "1"));
        ///         htBoite1.Add("southLongitude", new SimplePrimitiveNode("southLongitude", "2"));
        ///         htBoite1.Add("westLongitude", new SimplePrimitiveNode("westLongitude", "3"));
        ///
        ///         Hashtable htBoite2 = new Hashtable();
        ///         htBoite2.Add("eastLongitude", new SimplePrimitiveNode("eastLongitude", "4"));
        ///         htBoite2.Add("northLongitude", new SimplePrimitiveNode("northLongitude", "5"));
        ///         htBoite2.Add("southLongitude", new SimplePrimitiveNode("southLongitude", "6"));
        ///         htBoite2.Add("westLongitude", new SimplePrimitiveNode("westLongitude", "7"));
        ///
        ///         // je crée la couverture geographique du dataset
        ///         Hashtable htCouverture1 = new Hashtable();
        ///         htCouverture1.Add("city", new SimplePrimitiveNode("city", "geographicCoverageCity1"));
        ///         htCouverture1.Add("country", new SimpleControlledVocabularyNode("country", "Afghanistan"));
        ///         htCouverture1.Add("otherGeographicCoverage", new SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther1"));
        ///         htCouverture1.Add("state", new SimplePrimitiveNode("state", "geographicCoverageProvince1"));
        ///
        ///         Hashtable htCouverture2 = new Hashtable();
        ///         htCouverture2.Add("city", new SimplePrimitiveNode("city", "geographicCoverageCity2"));
        ///         htCouverture2.Add("country", new SimpleControlledVocabularyNode("country", "Albania"));
        ///         htCouverture2.Add("otherGeographicCoverage", new SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther2"));
        ///         htCouverture2.Add("state", new SimplePrimitiveNode("state", "geographicCoverageProvince2"));
        ///
        ///         // je crée la qualité du dataset
        ///         Hashtable htQualite1 = new Hashtable();
        ///         htQualite1.Add("lineage", new MultiplePrimitiveNode("lineage", new List<String>() { "QualityAndValidityLineage1" }));
        ///         htQualite1.Add("spatialResolution", new MultiplePrimitiveNode("lineage", new List<String>() { "QualityAndValiditySpatialResolution1" }));
        ///
        ///         Hashtable htQualite2 = new Hashtable();
        ///         htQualite2.Add("lineage", new MultiplePrimitiveNode("lineage", new List<String>() { "QualityAndValidityLineage2" }));
        ///         htQualite2.Add("spatialResolution", new MultiplePrimitiveNode("lineage", new List<String>() { "QualityAndValiditySpatialResolution2" }));
        ///         
        ///         // je crée un node geospatial
        ///         GeospatialNode gn = new GeospatialNode();
        ///         gn.AddField(new MultipleCompoundNode("geographicCoverage", new ArrayList() { htCouverture1, htCouverture2 }));
        ///         gn.AddField(new MultiplePrimitiveNode("geographicUnit", new List<String>() { "geographicUnit1", "geographicUnit2" }));
        ///         gn.AddField(new MultipleCompoundNode("geographicBoundingBox", new ArrayList() { htBoite1, htBoite2 }));
        ///         gn.AddField(new MultipleCompoundNode("qualityValidity", new ArrayList() { htQualite1, htQualite2 }));
        ///         gn.AddField(new MultipleCompoundNode("conformity", new ArrayList() { htConformite1, htConformite2 }));
        ///
        ///         Console.Write(gn.ToJSON();
        ///     </code>
        /// </example>
        public static void HowToBuildAGeospatialNode()
        {
            Console.Write
            (
                Environment.NewLine + "// je crée la conformite du dataset" +
                Environment.NewLine + "Hashtable htConformite1 = new Hashtable();" +
                Environment.NewLine + "htConformite1.Add(\"specification\", new MultiplePrimitiveNode(\"specification\", new List<String>() { \"conformitySpecification1\" }));" +
                Environment.NewLine + "htConformite1.Add(\"degree\", new SimpleControlledVocabularyNode(\"degree\", \"Conformant\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htConformite2 = new Hashtable();" +
                Environment.NewLine + "htConformite2.Add(\"specification\", new MultiplePrimitiveNode(\"specification\", new List<String>() { \"conformitySpecification2\" }));" +
                Environment.NewLine + "htConformite2.Add(\"degree\", new SimpleControlledVocabularyNode(\"degree\", \"Not Conformant\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée la boite geographique du dataset" +
                Environment.NewLine + "Hashtable htBoite1 = new Hashtable();" +
                Environment.NewLine + "htBoite1.Add(\"eastLongitude\", new SimplePrimitiveNode(\"eastLongitude\", \"0\"));" +
                Environment.NewLine + "htBoite1.Add(\"northLongitude\", new SimplePrimitiveNode(\"northLongitude\", \"1\"));" +
                Environment.NewLine + "htBoite1.Add(\"southLongitude\", new SimplePrimitiveNode(\"southLongitude\", \"2\"));" +
                Environment.NewLine + "htBoite1.Add(\"westLongitude\", new SimplePrimitiveNode(\"westLongitude\", \"3\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htBoite2 = new Hashtable();" +
                Environment.NewLine + "htBoite2.Add(\"eastLongitude\", new SimplePrimitiveNode(\"eastLongitude\", \"4\"));" +
                Environment.NewLine + "htBoite2.Add(\"northLongitude\", new SimplePrimitiveNode(\"northLongitude\", \"5\"));" +
                Environment.NewLine + "htBoite2.Add(\"southLongitude\", new SimplePrimitiveNode(\"southLongitude\", \"6\"));" +
                Environment.NewLine + "htBoite2.Add(\"westLongitude\", new SimplePrimitiveNode(\"westLongitude\", \"7\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée la couverture geographique du dataset" +
                Environment.NewLine + "Hashtable htCouverture1 = new Hashtable();" +
                Environment.NewLine + "htCouverture1.Add(\"city\", new SimplePrimitiveNode(\"city\", \"geographicCoverageCity1\"));" +
                Environment.NewLine + "htCouverture1.Add(\"country\", new SimpleControlledVocabularyNode(\"country\", \"Afghanistan\"));" +
                Environment.NewLine + "htCouverture1.Add(\"otherGeographicCoverage\", new SimplePrimitiveNode(\"otherGeographicCoverage\", \"geographicCoverageOther1\"));" +
                Environment.NewLine + "htCouverture1.Add(\"state\", new SimplePrimitiveNode(\"state\", \"geographicCoverageProvince1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htCouverture2 = new Hashtable();" +
                Environment.NewLine + "htCouverture2.Add(\"city\", new SimplePrimitiveNode(\"city\", \"geographicCoverageCity2\"));" +
                Environment.NewLine + "htCouverture2.Add(\"country\", new SimpleControlledVocabularyNode(\"country\", \"Albania\"));" +
                Environment.NewLine + "htCouverture2.Add(\"otherGeographicCoverage\", new SimplePrimitiveNode(\"otherGeographicCoverage\", \"geographicCoverageOther2\"));" +
                Environment.NewLine + "htCouverture2.Add(\"state\", new SimplePrimitiveNode(\"state\", \"geographicCoverageProvince2\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée la qualité du dataset" +
                Environment.NewLine + "Hashtable htQualite1 = new Hashtable();" +
                Environment.NewLine + "htQualite1.Add(\"lineage\", new MultiplePrimitiveNode(\"lineage\", new List<String>() { \"QualityAndValidityLineage1\" }));" +
                Environment.NewLine + "htQualite1.Add(\"spatialResolution\", new MultiplePrimitiveNode(\"lineage\", new List<String>() { \"QualityAndValiditySpatialResolution1\" }));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htQualite2 = new Hashtable();" +
                Environment.NewLine + "htQualite2.Add(\"lineage\", new MultiplePrimitiveNode(\"lineage\", new List<String>() { \"QualityAndValidityLineage2\" }));" +
                Environment.NewLine + "htQualite2.Add(\"spatialResolution\", new MultiplePrimitiveNode(\"lineage\", new List<String>() { \"QualityAndValiditySpatialResolution2\" }));" +
                Environment.NewLine +
                Environment.NewLine +
                Environment.NewLine +
                Environment.NewLine + "// je crée un node geospatial" +
                Environment.NewLine + "GeospatialNode gn = new GeospatialNode();" +
                Environment.NewLine + "gn.AddField(new MultipleCompoundNode(\"geographicCoverage\", new ArrayList() { htCouverture1, htCouverture2 }));" +
                Environment.NewLine + "gn.AddField(new MultiplePrimitiveNode(\"geographicUnit\", new List<String>() { \"geographicUnit1\", \"geographicUnit2\" }));" +
                Environment.NewLine + "gn.AddField(new MultipleCompoundNode(\"geographicBoundingBox\", new ArrayList() { htBoite1, htBoite2 }));" +
                Environment.NewLine + "gn.AddField(new MultipleCompoundNode(\"qualityValidity\", new ArrayList() { htQualite1, htQualite2 }));" +
                Environment.NewLine + "gn.AddField(new MultipleCompoundNode(\"conformity\", new ArrayList() { htConformite1, htConformite2 }));" +
                Environment.NewLine +
                Environment.NewLine + "Console.Write(gn.ToJSON();"
            );
        }

        /// <summary><c>HowToBuildAJournalNode</c> montre la manière de construire un <c>JournalNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         // je crée les support de publication du dataset
        ///         Hashtable htJournalVolumeIssue1 = new Hashtable();
        ///         htJournalVolumeIssue1.Add("journalIssue", new SimplePrimitiveNode("journalIssue", "journalIssue1"));
        ///         htJournalVolumeIssue1.Add("journalPubDate", new SimplePrimitiveNode("journalPubDate", "2000-01-01"));
        ///         htJournalVolumeIssue1.Add("journalVolume", new SimplePrimitiveNode("journalVolume", "journalVolume1"));
        ///         
        ///         Hashtable htJournalVolumeIssue2 = new Hashtable();
        ///         htJournalVolumeIssue2.Add("journalIssue", new SimplePrimitiveNode("journalIssue", "journalIssue2"));
        ///         htJournalVolumeIssue2.Add("journalPubDate", new SimplePrimitiveNode("journalPubDate", "2000-01-02"));
        ///         htJournalVolumeIssue2.Add("journalVolume", new SimplePrimitiveNode("journalVolume", "journalVolume2"));
        ///         
        ///         // je crée un node journal
        ///         JournalNode jn = new JournalNode();
        ///         jn.AddField(new MultipleCompoundNode("journalVolumeIssue", new ArrayList() { htJournalVolumeIssue1, htJournalVolumeIssue2 }));
        ///         jn.AddField(new SimpleControlledVocabularyNode("journalArticleType", "abstract"));
        ///         
        ///         Console.Write(jn.ToJSON();
        ///     </code>
        /// </example>
        public static void HowToBuildAJournalNode()
        {
            Console.Write
            (
                Environment.NewLine + "// je crée les support de publication du dataset" +
                Environment.NewLine + "Hashtable htJournalVolumeIssue1 = new Hashtable();" +
                Environment.NewLine + "htJournalVolumeIssue1.Add(\"journalIssue\", new SimplePrimitiveNode(\"journalIssue\", \"journalIssue1\"));" +
                Environment.NewLine + "htJournalVolumeIssue1.Add(\"journalPubDate\", new SimplePrimitiveNode(\"journalPubDate\", \"2000-01-01\"));" +
                Environment.NewLine + "htJournalVolumeIssue1.Add(\"journalVolume\", new SimplePrimitiveNode(\"journalVolume\", \"journalVolume1\"));" +
                Environment.NewLine + "" +
                Environment.NewLine + "Hashtable htJournalVolumeIssue2 = new Hashtable();" +
                Environment.NewLine + "htJournalVolumeIssue2.Add(\"journalIssue\", new SimplePrimitiveNode(\"journalIssue\", \"journalIssue2\"));" +
                Environment.NewLine + "htJournalVolumeIssue2.Add(\"journalPubDate\", new SimplePrimitiveNode(\"journalPubDate\", \"2000-01-02\"));" +
                Environment.NewLine + "htJournalVolumeIssue2.Add(\"journalVolume\", new SimplePrimitiveNode(\"journalVolume\", \"journalVolume2\"));" +
                Environment.NewLine + "" +
                Environment.NewLine + "// je crée un node journal" +
                Environment.NewLine + "JournalNode jn = new JournalNode();" +
                Environment.NewLine + "jn.AddField(new MultipleCompoundNode(\"journalVolumeIssue\", new ArrayList() { htJournalVolumeIssue1, htJournalVolumeIssue2 }));" +
                Environment.NewLine + "jn.AddField(new SimpleControlledVocabularyNode(\"journalArticleType\", \"abstract\"));" +
                Environment.NewLine + "" +
                Environment.NewLine + "Console.Write(jn.ToJSON();"
            );
        }

        /// <summary><c>HowToBuildAMetadataDocument</c> montre la manière de construire un <c>MetadataDocument</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         BiomedicalNode bn =     new BiomedicalNode();           // ... puis configurer bn  comme ici : @see HowToBuildABiomedicalNode
        ///         CitationNode cn =       new CitationNode();             // ... puis configurer cn  comme ici : @see HowToBuildACitationNode
        ///         DerivedTextNode dtn =   new DerivedTextNode();          // ... puis configurer dtn comme ici : @see HowToBuildADerivedTextNode
        ///         GeospatialNode gn =     new GeospatialNode();           // ... puis configurer gn  comme ici : @see HowToBuildAGeospatialNode
        ///         JournalNode jn =        new JournalNode();              // ... puis configurer jn  comme ici : @see HowToBuildAJournalNode
        ///         SemanticsNode sn =      new SemanticsNode();            // ... puis configurer sn  comme ici : @see HowToBuildASemanticsNode
        ///         SocialScienceNode ssn = new SocialScienceNode();        // ... puis configurer ssn comme ici : @see HowToBuildASocialScienceNode
        ///     
        ///         MetadataDocument md = new MetadataDocument();
        ///         md.AddCitationNode(cn);
        ///         md.AddGeospatialNode(gn);
        ///         md.AddSocialScienceNode(ssn);
        ///         md.AddBiomedicalNode(bn);
        ///         md.AddJournalNode(jn);
        ///         md.AddDerivedTextNode(dtn);
        ///         md.AddSemanticsNode(sn);
        ///     
        ///         Console.Write(md.ToJSON());
        ///     </code>
        /// </example>
        public static void HowToBuildAMetadataDocument()
        {
            Console.Write
            (
                Environment.NewLine + "BiomedicalNode bn =     new BiomedicalNode();           // ... puis configurer bn  comme ici : @see HowToBuildABiomedicalNode" +
                Environment.NewLine + "CitationNode cn =       new CitationNode();             // ... puis configurer cn  comme ici : @see HowToBuildACitationNode" +
                Environment.NewLine + "DerivedTextNode dtn =   new DerivedTextNode();          // ... puis configurer dtn comme ici : @see HowToBuildADerivedTextNode" +
                Environment.NewLine + "GeospatialNode gn =     new GeospatialNode();           // ... puis configurer gn  comme ici : @see HowToBuildAGeospatialNode" +
                Environment.NewLine + "JournalNode jn =        new JournalNode();              // ... puis configurer jn  comme ici : @see HowToBuildAJournalNode" +
                Environment.NewLine + "SemanticsNode sn =      new SemanticsNode();            // ... puis configurer sn  comme ici : @see HowToBuildASemanticsNode" +
                Environment.NewLine + "SocialScienceNode ssn = new SocialScienceNode();        // ... puis configurer ssn comme ici : @see HowToBuildASocialScienceNode" +
                Environment.NewLine +
                Environment.NewLine + "MetadataDocument md = new MetadataDocument();" +
                Environment.NewLine + "md.AddCitationNode(cn);" +
                Environment.NewLine + "md.AddGeospatialNode(gn);" +
                Environment.NewLine + "md.AddSocialScienceNode(ssn);" +
                Environment.NewLine + "md.AddBiomedicalNode(bn);" +
                Environment.NewLine + "md.AddJournalNode(jn);" +
                Environment.NewLine + "md.AddDerivedTextNode(dtn);" +
                Environment.NewLine + "md.AddSemanticsNode(sn);" +
                Environment.NewLine +
                Environment.NewLine + "Console.Write(md.ToJSON());"
            );
        }

        /// <summary><c>HowToBuildAMinimalCitationNode</c> montre la manière de construire un <c>CitationNode</c> minimal</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable htAuteur1 = new Hashtable();
        ///         htAuteur1.Add("authorAffiliation", new SimplePrimitiveNode("authorAffiliation", "authorAffiliation1"));
        ///         htAuteur1.Add("authorIdentifier", new SimplePrimitiveNode("authorIdentifier", "authorIdentifier1"));
        ///         htAuteur1.Add("authorIdentifierScheme", new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"));
        ///         htAuteur1.Add("authorName", new SimplePrimitiveNode("authorName", "authorName1"));
        ///
        ///         Hashtable htAuteur2 = new Hashtable();
        ///         htAuteur2.Add("authorAffiliation", new SimplePrimitiveNode("authorAffiliation", "authorAffiliation2"));
        ///         htAuteur2.Add("authorIdentifier", new SimplePrimitiveNode("authorIdentifier", "authorIdentifier2"));
        ///         htAuteur2.Add("authorIdentifierScheme", new SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL"));
        ///         htAuteur2.Add("authorName", new SimplePrimitiveNode("authorName", "authorName2"));
        ///
        ///         Hashtable htContact1 = new Hashtable();
        ///         htContact1.Add("datasetContactAffiliation", new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1"));
        ///         htContact1.Add("datasetContactEmail", new SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com"));
        ///         htContact1.Add("datasetContactName", new SimplePrimitiveNode("datasetContactName", "contactName1"));
        ///
        ///         Hashtable htContact2 = new Hashtable();
        ///         htContact2.Add("datasetContactAffiliation", new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2"));
        ///         htContact2.Add("datasetContactEmail", new SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com"));
        ///         htContact2.Add("datasetContactName", new SimplePrimitiveNode("datasetContactName", "contactName2"));
        ///
        ///         Hashtable htDescription1 = new Hashtable();
        ///         htDescription1.Add("dsDescriptionDate", new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01"));
        ///         htDescription1.Add("dsDescriptionValue", new SimplePrimitiveNode("dsDescriptionValue", "descriptionText1"));
        ///
        ///         Hashtable htDescription2 = new Hashtable();
        ///         htDescription2.Add("dsDescriptionDate", new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01"));
        ///         htDescription2.Add("dsDescriptionValue", new SimplePrimitiveNode("dsDescriptionValue", "descriptionText2"));
        ///
        ///         List<String> kindOfData = new List<String>();
        ///         kindOfData.Add("Audiovisual");
        ///         kindOfData.Add("Collection");
        ///
        ///         List<String> sujets = new List<String>();
        ///         sujets.Add("Animal Breeding and Animal Products");
        ///         sujets.Add("Animal Health and Pathology");
        ///
        ///         
        ///         
        ///         CitationNode cn = new CitationNode();
        ///         cn.AddField(new SimplePrimitiveNode("title", "title"));
        ///         cn.AddField(new MultipleCompoundNode("datasetContact", new ArrayList() { htContact1, htContact2 }));
        ///         cn.AddField(new MultipleCompoundNode("author", new ArrayList() { htAuteur1, htAuteur2 }));
        ///         cn.AddField(new MultipleCompoundNode("dsDescription", new ArrayList() { htDescription1, htDescription2 }));
        ///         cn.AddField(new MultipleControlledVocabularyNode("subject", sujets));
        ///         cn.AddField(new MultipleControlledVocabularyNode("kindOfData", kindOfData));
        ///
        ///         Console.Write(cn.toJSON());
        ///     </code>
        /// </example>
        public static void HowToBuildAMinimalCitationNode()
        {
            Console.Write
            (
                Environment.NewLine + "Hashtable htAuteur1 = new Hashtable();" +
                Environment.NewLine + "htAuteur1.Add(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"authorAffiliation1\"));" +
                Environment.NewLine + "htAuteur1.Add(\"authorIdentifier\", new SimplePrimitiveNode(\"authorIdentifier\", \"authorIdentifier1\"));" +
                Environment.NewLine + "htAuteur1.Add(\"authorIdentifierScheme\", new SimpleControlledVocabularyNode(\"authorIdentifierScheme\", \"ORCID\"));" +
                Environment.NewLine + "htAuteur1.Add(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"authorName1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htAuteur2 = new Hashtable();" +
                Environment.NewLine + "htAuteur2.Add(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"authorAffiliation2\"));" +
                Environment.NewLine + "htAuteur2.Add(\"authorIdentifier\", new SimplePrimitiveNode(\"authorIdentifier\", \"authorIdentifier2\"));" +
                Environment.NewLine + "htAuteur2.Add(\"authorIdentifierScheme\", new SimpleControlledVocabularyNode(\"authorIdentifierScheme\", \"idHAL\"));" +
                Environment.NewLine + "htAuteur2.Add(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"authorName2\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htContact1 = new Hashtable();" +
                Environment.NewLine + "htContact1.Add(\"datasetContactAffiliation\", new SimplePrimitiveNode(\"datasetContactAffiliation\", \"contactAffiliation1\"));" +
                Environment.NewLine + "htContact1.Add(\"datasetContactEmail\", new SimplePrimitiveNode(\"datasetContactEmail\", \"contact1@mail.com\"));" +
                Environment.NewLine + "htContact1.Add(\"datasetContactName\", new SimplePrimitiveNode(\"datasetContactName\", \"contactName1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htContact2 = new Hashtable();" +
                Environment.NewLine + "htContact2.Add(\"datasetContactAffiliation\", new SimplePrimitiveNode(\"datasetContactAffiliation\", \"contactAffiliation2\"));" +
                Environment.NewLine + "htContact2.Add(\"datasetContactEmail\", new SimplePrimitiveNode(\"datasetContactEmail\", \"contact2@mail.com\"));" +
                Environment.NewLine + "htContact2.Add(\"datasetContactName\", new SimplePrimitiveNode(\"datasetContactName\", \"contactName2\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htDescription1 = new Hashtable();" +
                Environment.NewLine + "htDescription1.Add(\"dsDescriptionDate\", new SimplePrimitiveNode(\"dsDescriptionDate\", \"2000-01-01\"));" +
                Environment.NewLine + "htDescription1.Add(\"dsDescriptionValue\", new SimplePrimitiveNode(\"dsDescriptionValue\", \"descriptionText1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htDescription2 = new Hashtable();" +
                Environment.NewLine + "htDescription2.Add(\"dsDescriptionDate\", new SimplePrimitiveNode(\"dsDescriptionDate\", \"2000-01-01\"));" +
                Environment.NewLine + "htDescription2.Add(\"dsDescriptionValue\", new SimplePrimitiveNode(\"dsDescriptionValue\", \"descriptionText2\"));" +
                Environment.NewLine +
                Environment.NewLine + "List<String> kindOfData = new List<String>();" +
                Environment.NewLine + "kindOfData.Add(\"Audiovisual\");" +
                Environment.NewLine + "kindOfData.Add(\"Collection\");" +
                Environment.NewLine +
                Environment.NewLine + "List<String> sujets = new List<String>();" +
                Environment.NewLine + "sujets.Add(\"Animal Breeding and Animal Products\");" +
                Environment.NewLine + "sujets.Add(\"Animal Health and Pathology\");" +
                Environment.NewLine +
                Environment.NewLine + "CitationNode cn = new CitationNode();" +
                Environment.NewLine + "cn.AddField(new SimplePrimitiveNode(\"title\", \"title\"));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"datasetContact\", new ArrayList() { htContact1, htContact2 }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"author\", new ArrayList() { htAuteur1, htAuteur2 }));" +
                Environment.NewLine + "cn.AddField(new MultipleCompoundNode(\"dsDescription\", new ArrayList() { htDescription1, htDescription2 }));" +
                Environment.NewLine + "cn.AddField(new MultipleControlledVocabularyNode(\"subject\", sujets));" +
                Environment.NewLine + "cn.AddField(new MultipleControlledVocabularyNode(\"kindOfData\", kindOfData));" +
                Environment.NewLine +
                Environment.NewLine +
                Environment.NewLine +
                Environment.NewLine + "Console.Write(cn.toJSON());"
            );
        }

        /// <summary><c>HowToBuildAMultipleCompoundNode</c> montre la manière de construire un <c>MultipleCompoundNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable auteur1 = new Hashtable();
        ///         auteur1.Add("authorName",               new SimplePrimitiveNode("authorName", "Heirman, Thierry"));
        ///         auteur1.Add("authorAffiliation",        new SimplePrimitiveNode("authorAffiliation", "INRAE"));
        ///         auteur1.Add("authorIdentifier",         new SimplePrimitiveNode("authorIdentifier", "https://orcid.org/0000-0002-3466-1099"));
        ///         auteur1.Add("authorIdentifierScheme",   new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"));
        ///
        ///         MultipleCompoundNode mcn = new MultipleCompoundNode("author", new ArrayList() { auteur1 });
        ///         Console.Write(mcn.ToJSON());
        ///     </code>
        /// </example>
        public static void HowToBuildAMultipleCompoundNode()
        {
            Console.Write(
                Environment.NewLine + "Hashtable auteur1 = new Hashtable();" +
                Environment.NewLine + "auteur1.Add(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"Heirman, Thierry\"));" +
                Environment.NewLine + "auteur1.Add(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"INRAE\"));" +
                Environment.NewLine + "auteur1.Add(\"authorIdentifier\", new SimplePrimitiveNode(\"authorIdentifier\", \"https://orcid.org/0000-0002-3466-1099\"));" +
                Environment.NewLine + "auteur1.Add(\"authorIdentifierScheme\", new SimpleControlledVocabularyNode(\"authorIdentifierScheme\", \"ORCID\"));" +
                Environment.NewLine +
                Environment.NewLine + "MultipleCompoundNode mcn = new MultipleCompoundNode(\"author\", new ArrayList() { auteur1 });" +
                Environment.NewLine + "Console.Write(mcn.ToJSON());"
            );
        }

        /// <summary><c>HowToBuildAMultipleControlledVocabularyNode</c> montre la manière de construire un <c>MultipleControlledVocabularyNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         List<String> values = new List<String>();
        ///         values.Add("French");
        ///         values.Add("English");
        ///         values.Add("Occitan");
        ///
        ///         MultipleControlledVocabularyNode mcvn = new MultipleControlledVocabularyNode("language", values);
        ///         Console.Write(mcvn.ToJSON();
        ///     </code>
        /// </example>
        public static void HowToBuildAMultipleControlledVocabularyNode()
        {
            Console.Write
            (
                Environment.NewLine + "List<String> values = new List<String>();" +
                Environment.NewLine + "values.Add(\"French\");" +
                Environment.NewLine + "values.Add(\"English\");" +
                Environment.NewLine + "values.Add(\"Occitan\");" +
                Environment.NewLine +
                Environment.NewLine + "MultipleControlledVocabularyNode mcvn = new MultipleControlledVocabularyNode(\"language\", values);" +
                Environment.NewLine + "Console.Write(mcvn.ToJSON();"
            );
        }

        /// <summary><c>HowToBuildAMultiplePrimitiveNode</c> montre la manière de construire un <c>MultiplePrimitiveNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         List<String> values = new List<String>();
        ///         values.Add("dataSource1");
        ///         values.Add("dataSource2");
        ///         values.Add("dataSource3");
        /// 
        ///         MultiplePrimitiveNode mpn = new MultiplePrimitiveNode("dataSources", values);
        ///         Console.Write(mpn.ToJSON());
        ///     </code>
        /// </example>
        public static void HowToBuildAMultiplePrimitiveNode()
        {
            Console.Write
            (
                Environment.NewLine + "List<String> values = new List<String>();" +
                Environment.NewLine + "values.Add(\"dataSource1\");" +
                Environment.NewLine + "values.Add(\"dataSource2\");" +
                Environment.NewLine + "values.Add(\"dataSource3\");" +
                Environment.NewLine + 
                Environment.NewLine + "MultiplePrimitiveNode mpn = new MultiplePrimitiveNode(\"dataSources\", values);" +
                Environment.NewLine + "Console.Write(mpn.ToJSON();"
            );
        }

        /// <summary><c>HowToBuildASemanticsNode</c> montre la manière de construire un <c>SemanticsNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         // je crée la version de ressource du dataset
        ///         Hashtable htResourceVersion = new Hashtable();
        ///         htResourceVersion.Add("versionStatus", new MultipleControlledVocabularyNode("versionStatus", new List<String>() { "alpha", "beta" }));
        ///         htResourceVersion.Add("versionInfo", new SimplePrimitiveNode("versionInfo", "version"));
        ///         htResourceVersion.Add("priorVersion", new MultiplePrimitiveNode("priorVersion", new List<String>() { "http://prior.version" }));
        ///         htResourceVersion.Add("modificationDate", new SimplePrimitiveNode("modificationDate", "2000-01-01"));
        ///         htResourceVersion.Add("changes", new SimplePrimitiveNode("changes", "http://semantic.version.changes"));
        ///         
        ///         // je crée un node semantics
        ///         SemanticsNode sn = new SemanticsNode();
        ///         sn.AddField(new SimpleControlledVocabularyNode("hasFormalityLevel", "Classification scheme"));
        ///         sn.AddField(new SimpleControlledVocabularyNode("hasOntologyLanguage", "JSON-LD"));
        ///         sn.AddField(new SimpleControlledVocabularyNode("typeOfSR", "Application Ontology"));
        ///         sn.AddField(new MultipleControlledVocabularyNode("designedForOntologyTask", new List<String>() { "Annotation Task", "Configuration Task" }));
        ///         sn.AddField(new SimplePrimitiveNode("knownUsage", "knownUsage"));
        ///         sn.AddField(new SimpleCompoundNode("resourceVersion", htResourceVersion));
        ///         sn.AddField(new MultiplePrimitiveNode("imports", new List<String>() { "http://semantic.imports1", "http://semantic.imports2" }));
        ///         sn.AddField(new SimplePrimitiveNode("bugDatabase", "http://semantic.bug.tracker"));
        ///         sn.AddField(new SimplePrimitiveNode("endpoint", "http://semantic.sparql.endpoint"));
        ///         sn.AddField(new MultiplePrimitiveNode("imports", new List<String>() { "http://semantic.uri1", "http://semantic.uri2" }));
        ///         
        ///         Console.Write(sn.ToJSON();
        ///     </code>
        /// </example>
        public static void HowToBuildASemanticsNode()
        {
            Console.Write
            (
                Environment.NewLine + "// je crée la version de ressource du dataset" +
                Environment.NewLine + "Hashtable htResourceVersion = new Hashtable();" +
                Environment.NewLine + "htResourceVersion.Add(\"versionStatus\", new MultipleControlledVocabularyNode(\"versionStatus\", new List<String>() { \"alpha\", \"beta\" }));" +
                Environment.NewLine + "htResourceVersion.Add(\"versionInfo\", new SimplePrimitiveNode(\"versionInfo\", \"version\"));" +
                Environment.NewLine + "htResourceVersion.Add(\"priorVersion\", new MultiplePrimitiveNode(\"priorVersion\", new List<String>() { \"http://prior.version\" }));" +
                Environment.NewLine + "htResourceVersion.Add(\"modificationDate\", new SimplePrimitiveNode(\"modificationDate\", \"2000-01-01\"));" +
                Environment.NewLine + "htResourceVersion.Add(\"changes\", new SimplePrimitiveNode(\"changes\", \"http://semantic.version.changes\"));" +

                Environment.NewLine + "// je crée un node semantics" +
                Environment.NewLine + "SemanticsNode sn = new SemanticsNode();" +
                Environment.NewLine + "sn.AddField(new SimpleControlledVocabularyNode(\"hasFormalityLevel\", \"Classification scheme\"));" +
                Environment.NewLine + "sn.AddField(new SimpleControlledVocabularyNode(\"hasOntologyLanguage\", \"JSON-LD\"));" +
                Environment.NewLine + "sn.AddField(new SimpleControlledVocabularyNode(\"typeOfSR\", \"Application Ontology\"));" +
                Environment.NewLine + "sn.AddField(new MultipleControlledVocabularyNode(\"designedForOntologyTask\", new List<String>() { \"Annotation Task\", \"Configuration Task\" }));" +
                Environment.NewLine + "sn.AddField(new SimplePrimitiveNode(\"knownUsage\", \"knownUsage\"));" +
                Environment.NewLine + "sn.AddField(new SimpleCompoundNode(\"resourceVersion\", htResourceVersion));" +
                Environment.NewLine + "sn.AddField(new MultiplePrimitiveNode(\"imports\", new List<String>() { \"http://semantic.imports1\", \"http://semantic.imports2\" }));" +
                Environment.NewLine + "sn.AddField(new SimplePrimitiveNode(\"bugDatabase\", \"http://semantic.bug.tracker\"));" +
                Environment.NewLine + "sn.AddField(new SimplePrimitiveNode(\"endpoint\", \"http://semantic.sparql.endpoint\"));" +
                Environment.NewLine + "sn.AddField(new MultiplePrimitiveNode(\"imports\", new List<String>() { \"http://semantic.uri1\", \"http://semantic.uri2\" }));" +
                Environment.NewLine + "" +
                Environment.NewLine + "Console.Write(sn.ToJSON();"
            );
        }

        /// <summary><c>HowToBuildASimpleCompoundNode</c> montre la manière de construire un <c>SimpleCompoundNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable project = new Hashtable();
        ///         project.Add("projectAcronym",       new SimplePrimitiveNode("projectAcronym", "projectAcronym"));
        ///         project.Add("projectId",            new SimplePrimitiveNode("projectId", "projectId"));
        ///         project.Add("projectTitle",         new SimplePrimitiveNode("projectTitle", "projectTitle"));
        ///         project.Add("projectTask",          new SimplePrimitiveNode("projectTask", "projectTask"));
        ///         project.Add("projectURL",           new SimplePrimitiveNode("projectURL", "projectURL"));
        ///         project.Add("projectWorkPackage",   new SimplePrimitiveNode("projectWorkPackage", "projectWorkPackage"));
        /// 
        ///         SimpleCompoundNode scn = new SimpleCompoundNode("project", project);
        ///         Console.Write(scn.ToJSON();
        ///     </code>
        /// </example>
        public static void HowToBuildASimpleCompoundNode()
        {
            Console.Write
            (
                Environment.NewLine + "Hashtable project = new Hashtable();" +
                Environment.NewLine + "project.Add(\"projectAcronym\", new SimplePrimitiveNode(\"projectAcronym\", \"projectAcronym\"));" +
                Environment.NewLine + "project.Add(\"projectId\", new SimplePrimitiveNode(\"projectId\", \"projectId\"));" +
                Environment.NewLine + "project.Add(\"projectTitle\", new SimplePrimitiveNode(\"projectTitle\", \"projectTitle\"));" +
                Environment.NewLine + "project.Add(\"projectTask\", new SimplePrimitiveNode(\"projectTask\", \"projectTask\"));" +
                Environment.NewLine + "project.Add(\"projectURL\", new SimplePrimitiveNode(\"projectURL\", \"projectURL\"));" +
                Environment.NewLine + "project.Add(\"projectWorkPackage\", new SimplePrimitiveNode(\"projectWorkPackage\", \"projectWorkPackage\"));" +
                Environment.NewLine +
                Environment.NewLine + "SimpleCompoundNode scn = new SimpleCompoundNode(\"project\", project);" +
                Environment.NewLine + "Console.Write(scn.ToJSON();"
            );
        }

        /// <summary><c>HowToBuildASimpleControlledVocabularyNode</c> montre la manière de construire un <c>SimpleControlledVocabularyNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SimpleControlledVocabularyNode scvn = new SimpleControlledVocabularyNode("journalArticleType", "news");" +
        ///         Console.Write(scvn.ToJSON();
        ///     </code>
        /// </example>
        public static void HowToBuildASimpleControlledVocabularyNode()
        {
            Console.Write
            (
                Environment.NewLine + "SimpleControlledVocabularyNode scvn = new SimpleControlledVocabularyNode(\"journalArticleType\", \"news\");" +
                Environment.NewLine + "Console.Write(scvn.ToJSON();"
            );
        }

        /// <summary><c>HowToBuildASimplePrimitiveNode</c> montre la manière de construire un <c>SimplePrimitiveNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SimplePrimitiveNode spn = new SimplePrimitiveNode("softwareName", "softwareName");
        ///         Console.Write(spn.ToJSON();
        ///     </code>
        /// </example>
        public static void HowToBuildASimplePrimitiveNode()
        {
            Console.Write
            (
                Environment.NewLine + "SimplePrimitiveNode spn = new SimplePrimitiveNode(\"softwareName\", \"softwareName\");" +
                Environment.NewLine + "Console.Write(spn.ToJSON();"
            );
        }

        /// <summary><c>HowToBuildASocialScienceNode</c> montre la manière de construire un <c>SocialScienceNode</c></summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         // je crée la taille de l'échantillon 
        ///         Hashtable htTargetSampleSize = new Hashtable();
        ///         htTargetSampleSize.Add("targetSampleActualSize", new SimplePrimitiveNode("targetSampleActualSize", "42"));
        ///         htTargetSampleSize.Add("targetSampleSizeFormula", new SimplePrimitiveNode("targetSampleSizeFormula", "targetSampleSizeFormula"));
        ///         
        ///         // je crée notes du node
        ///         Hashtable htSocialScienceNotes = new Hashtable();
        ///         htSocialScienceNotes.Add("socialScienceNotesSubject", new SimplePrimitiveNode("socialScienceNotesSubject", "socialScienceNotesSubject"));
        ///         htSocialScienceNotes.Add("socialScienceNotesText", new SimplePrimitiveNode("socialScienceNotesText", "socialScienceNotesText"));
        ///         htSocialScienceNotes.Add("socialScienceNotesType", new SimplePrimitiveNode("socialScienceNotesType", "socialScienceNotesType"));
        ///         
        ///         // je crée notes du node
        ///         Hashtable htGeographicalReferential1 = new Hashtable();
        ///         htGeographicalReferential1.Add("level", new SimplePrimitiveNode("level", "geographicalReferentialLevel1"));
        ///         htGeographicalReferential1.Add("version", new SimplePrimitiveNode("version", "geographicalReferentialVersion1"));
        ///         
        ///         Hashtable htGeographicalReferential2 = new Hashtable();
        ///         htGeographicalReferential2.Add("level", new SimplePrimitiveNode("level", "geographicalReferentialLevel2"));
        ///         htGeographicalReferential2.Add("version", new SimplePrimitiveNode("version", "geographicalReferentialVersion2"));
        ///         
        ///         
        ///         
        ///         // je crée un node socialscience
        ///         SocialScienceNode ssn = new SocialScienceNode();
        ///         ssn.AddField(new MultipleControlledVocabularyNode("unitOfAnalysis", new List<String>() { "Individual", "Organization" }));
        ///         ssn.AddField(new MultiplePrimitiveNode("universe", new List<String>() { "universe1", "universe2" }));
        ///         ssn.AddField(new SimpleControlledVocabularyNode("timeMethod", "Longitudinal"));
        ///         ssn.AddField(new SimplePrimitiveNode("timeMethodOther", "timeMethodOther"));
        ///         ssn.AddField(new SimplePrimitiveNode("dataCollector", "dataCollector"));
        ///         ssn.AddField(new SimplePrimitiveNode("collectorTraining", "collectorTraining"));
        ///         ssn.AddField(new SimplePrimitiveNode("frequencyOfDataCollection", "frequency"));
        ///         ssn.AddField(new SimpleControlledVocabularyNode("samplingProcedure", "Total universe/Complete enumeration"));
        ///         ssn.AddField(new SimpleControlledVocabularyNode("samplingProcedureOther", "samplingProcedureOther"));
        ///         ssn.AddField(new SimpleCompoundNode("targetSampleSize", htTargetSampleSize));
        ///         ssn.AddField(new SimplePrimitiveNode("deviationsFromSampleDesign", "deviationsFromSampleDesign"));
        ///         ssn.AddField(new SimpleControlledVocabularyNode("collectionMode", "Interview"));
        ///         ssn.AddField(new SimplePrimitiveNode("collectionModeOther", "collectionModeOther"));
        ///         ssn.AddField(new SimplePrimitiveNode("researchInstrument", "researchInstrument"));
        ///         ssn.AddField(new SimplePrimitiveNode("dataCollectionSituation", "dataCollectionSituation"));
        ///         ssn.AddField(new SimplePrimitiveNode("actionsToMinimizeLoss", "actionsToMinimizeLoss"));
        ///         ssn.AddField(new SimplePrimitiveNode("controlOperations", "controlOperations"));
        ///         ssn.AddField(new SimplePrimitiveNode("weighting", "weighting"));
        ///         ssn.AddField(new SimplePrimitiveNode("cleaningOperations", "cleaningOperations"));
        ///         ssn.AddField(new SimplePrimitiveNode("datasetLevelErrorNotes", "datasetLevelErrorNotes"));
        ///         ssn.AddField(new SimplePrimitiveNode("responseRate", "responseRate"));
        ///         ssn.AddField(new SimplePrimitiveNode("samplingErrorEstimates", "samplingErrorEstimates"));
        ///         ssn.AddField(new SimplePrimitiveNode("otherDataAppraisal", "otherDataAppraisal"));
        ///         ssn.AddField(new SimpleCompoundNode("socialScienceNotes", htSocialScienceNotes));
        ///         ssn.AddField(new MultipleCompoundNode("geographicalReferential", new ArrayList() { htGeographicalReferential1, htGeographicalReferential2 }));
        ///         
        ///         Console.Write(ssn.ToJSON();
        ///     </code>
        /// </example>
        public static void HowToBuildASocialScienceNode()
        {
            Console.Write
            (
                Environment.NewLine + "// je crée la taille de l'échantillon " +
                Environment.NewLine + "Hashtable htTargetSampleSize = new Hashtable();" +
                Environment.NewLine + "htTargetSampleSize.Add(\"targetSampleActualSize\", new SimplePrimitiveNode(\"targetSampleActualSize\", \"42\"));" +
                Environment.NewLine + "htTargetSampleSize.Add(\"targetSampleSizeFormula\", new SimplePrimitiveNode(\"targetSampleSizeFormula\", \"targetSampleSizeFormula\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée notes du node" +
                Environment.NewLine + "Hashtable htSocialScienceNotes = new Hashtable();" +
                Environment.NewLine + "htSocialScienceNotes.Add(\"socialScienceNotesSubject\", new SimplePrimitiveNode(\"socialScienceNotesSubject\", \"socialScienceNotesSubject\"));" +
                Environment.NewLine + "htSocialScienceNotes.Add(\"socialScienceNotesText\", new SimplePrimitiveNode(\"socialScienceNotesText\", \"socialScienceNotesText\"));" +
                Environment.NewLine + "htSocialScienceNotes.Add(\"socialScienceNotesType\", new SimplePrimitiveNode(\"socialScienceNotesType\", \"socialScienceNotesType\"));" +
                Environment.NewLine +
                Environment.NewLine + "// je crée notes du node" +
                Environment.NewLine + "Hashtable htGeographicalReferential1 = new Hashtable();" +
                Environment.NewLine + "htGeographicalReferential1.Add(\"level\", new SimplePrimitiveNode(\"level\", \"geographicalReferentialLevel1\"));" +
                Environment.NewLine + "htGeographicalReferential1.Add(\"version\", new SimplePrimitiveNode(\"version\", \"geographicalReferentialVersion1\"));" +
                Environment.NewLine +
                Environment.NewLine + "Hashtable htGeographicalReferential2 = new Hashtable();" +
                Environment.NewLine + "htGeographicalReferential2.Add(\"level\", new SimplePrimitiveNode(\"level\", \"geographicalReferentialLevel2\"));" +
                Environment.NewLine + "htGeographicalReferential2.Add(\"version\", new SimplePrimitiveNode(\"version\", \"geographicalReferentialVersion2\"));" +
                Environment.NewLine +
                Environment.NewLine +
                Environment.NewLine +
                Environment.NewLine + "// je crée un node socialscience" +
                Environment.NewLine + "SocialScienceNode ssn = new SocialScienceNode();" +
                Environment.NewLine + "ssn.AddField(new MultipleControlledVocabularyNode(\"unitOfAnalysis\", new List<String>() { \"Individual\", \"Organization\" }));" +
                Environment.NewLine + "ssn.AddField(new MultiplePrimitiveNode(\"universe\", new List<String>() { \"universe1\", \"universe2\" }));" +
                Environment.NewLine + "ssn.AddField(new SimpleControlledVocabularyNode(\"timeMethod\", \"Longitudinal\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"timeMethodOther\", \"timeMethodOther\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"dataCollector\", \"dataCollector\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"collectorTraining\", \"collectorTraining\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"frequencyOfDataCollection\", \"frequency\"));" +
                Environment.NewLine + "ssn.AddField(new SimpleControlledVocabularyNode(\"samplingProcedure\", \"Total universe/Complete enumeration\"));" +
                Environment.NewLine + "ssn.AddField(new SimpleControlledVocabularyNode(\"samplingProcedureOther\", \"samplingProcedureOther\"));" +
                Environment.NewLine + "ssn.AddField(new SimpleCompoundNode(\"targetSampleSize\", htTargetSampleSize));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"deviationsFromSampleDesign\", \"deviationsFromSampleDesign\"));" +
                Environment.NewLine + "ssn.AddField(new SimpleControlledVocabularyNode(\"collectionMode\", \"Interview\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"collectionModeOther\", \"collectionModeOther\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"researchInstrument\", \"researchInstrument\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"dataCollectionSituation\", \"dataCollectionSituation\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"actionsToMinimizeLoss\", \"actionsToMinimizeLoss\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"controlOperations\", \"controlOperations\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"weighting\", \"weighting\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"cleaningOperations\", \"cleaningOperations\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"datasetLevelErrorNotes\", \"datasetLevelErrorNotes\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"responseRate\", \"responseRate\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"samplingErrorEstimates\", \"samplingErrorEstimates\"));" +
                Environment.NewLine + "ssn.AddField(new SimplePrimitiveNode(\"otherDataAppraisal\", \"otherDataAppraisal\"));" +
                Environment.NewLine + "ssn.AddField(new SimpleCompoundNode(\"socialScienceNotes\", htSocialScienceNotes));" +
                Environment.NewLine + "ssn.AddField(new MultipleCompoundNode(\"geographicalReferential\", new ArrayList() { htGeographicalReferential1, htGeographicalReferential2 }));" +
                Environment.NewLine +
                Environment.NewLine + "Console.Write(n.ToJSON();"
            );
        }

        /// <summary><c>ShowMetadataMap</c> détaille la carte des nodes du document de métadonnées</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         metadata
        ///         |  citation
        ///         |  |  accessToSources                       SimplePrimitiveNode
        ///         |  |  alternativeTitle                      SimplePrimitiveNode
        ///         |  |  alternativeURL                        SimplePrimitiveNode
        ///         |  |  author                                MultipleCompoundNode
        ///         |  |  |  authorAffiliation                  SimplePrimitiveNode
        ///         |  |  |  authorIdentifier                   SimplePrimitiveNode
        ///         |  |  |  authorIdentifierScheme             SimpleControlledVocabularyNode
        ///         |  |  |  authorName                         SimplePrimitiveNode
        ///         |  |  characteristicOfSources               SimplePrimitiveNode
        ///         |  |  contributor                           MultipleCompoundNode
        ///         |  |  |  contributorAffiliation             SimplePrimitiveNode
        ///         |  |  |  contributorIdentifier              SimplePrimitiveNode
        ///         |  |  |  contributorIdentifierScheme        SimpleControlledVocabularyNode
        ///         |  |  |  contributorName                    SimplePrimitiveNode
        ///         |  |  |  contributorType                    SimpleControlledVocabularyNode
        ///         |  |  dataOrigin                            MultipleControlledVocabularyNode
        ///         |  |  datasetContact                        MultipleCompoundNode
        ///         |  |  |  datasetContactAffiliation          SimplePrimitiveNode
        ///         |  |  |  datasetContactEmail                SimplePrimitiveNode
        ///         |  |  |  datasetContactName                 SimplePrimitiveNode
        ///         |  |  dataSources                           MultiplePrimitiveNode
        ///         |  |  dateOfCollection                      MultipleCompoundNode
        ///         |  |  |  dateOfCollectionStart              SimplePrimitiveNode
        ///         |  |  |  dateOfCollectionEnd                SimplePrimitiveNode
        ///         |  |  dateOfDeposit                         SimplePrimitiveNode
        ///         |  |  depositor                             SimplePrimitiveNode
        ///         |  |  distributionDate                      SimplePrimitiveNode
        ///         |  |  distributor                           MultipleCompoundNode
        ///         |  |  |  distributorAbbreviation            SimplePrimitiveNode
        ///         |  |  |  distributorAffiliation             SimplePrimitiveNode
        ///         |  |  |  distributorLogoURL                 SimplePrimitiveNode
        ///         |  |  |  distributorName                    SimplePrimitiveNode
        ///         |  |  |  distributorURL                     SimplePrimitiveNode
        ///         |  |  dsDescription                         MultipleCompoundNode
        ///         |  |  |  dsDescriptionDate                  SimplePrimitiveNode
        ///         |  |  |  dsDescriptionValue                 SimplePrimitiveNode
        ///         |  |  grantNumber                           MultipleCompoundNode
        ///         |  |  |  grantNumberAgency                  SimplePrimitiveNode
        ///         |  |  |  grantNumberValue                   SimplePrimitiveNode
        ///         |  |  keyword                               MultipleCompoundNode
        ///         |  |  |  keywordTermURI                     SimplePrimitiveNode
        ///         |  |  |  keywordValue                       SimplePrimitiveNode
        ///         |  |  |  keywordVocabulary                  SimplePrimitiveNode
        ///         |  |  |  keywordVocabularyURI               SimplePrimitiveNode
        ///         |  |  kindOfData                            MultipleControlledVocabularyNode
        ///         |  |  kindOfDataOther                       MultiplePrimitiveNode
        ///         |  |  language                              MultipleControlledVocabularyNode
        ///         |  |  lifeCycleStep                         MultipleControlledVocabularyNode
        ///         |  |  notesText                             SimplePrimitiveNode
        ///         |  |  originOfSources                       SimplePrimitiveNode
        ///         |  |  otherId                               MultipleCompoundNode
        ///         |  |  |  otherIdAgency                      SimplePrimitiveNode
        ///         |  |  |  otherIdValue                       SimplePrimitiveNode
        ///         |  |  otherReferences                       MultiplePrimitiveNode
        ///         |  |  producer                              MultipleCompoundNode
        ///         |  |  |  producerAbbreviation               SimplePrimitiveNode
        ///         |  |  |  producerAffiliation                SimplePrimitiveNode
        ///         |  |  |  producerLogoURL                    SimplePrimitiveNode
        ///         |  |  |  producerName                       SimplePrimitiveNode
        ///         |  |  |  producerURL                        SimplePrimitiveNode
        ///         |  |  productionDate                        SimplePrimitiveNode
        ///         |  |  productionPlace                       SimplePrimitiveNode
        ///         |  |  project                               SimpleCompoundNode
        ///         |  |  |  projectAcronym                     SimplePrimitiveNode
        ///         |  |  |  projectId                          SimplePrimitiveNode
        ///         |  |  |  projectTask                        SimplePrimitiveNode
        ///         |  |  |  projectTitle                       SimplePrimitiveNode
        ///         |  |  |  projectURL                         SimplePrimitiveNode
        ///         |  |  |  projectWorkPackage                 SimplePrimitiveNode
        ///         |  |  publication                           MultipleCompoundNode
        ///         |  |  |  publicationCitation                SimplePrimitiveNode
        ///         |  |  |  publicationIDNumber                SimplePrimitiveNode
        ///         |  |  |  publicationIDType                  SimpleControlledVocabularyNode
        ///         |  |  |  publicationURL                     SimplePrimitiveNode
        ///         |  |  relatedDataset                        MultipleCompoundNode
        ///         |  |  |  relatedDatasetCitation             SimplePrimitiveNode
        ///         |  |  |  relatedDatasetIDNumber             SimplePrimitiveNode
        ///         |  |  |  relatedDatasetIDType               SimpleControlledVocabularyNode
        ///         |  |  |  relatedDatasetURL                  SimplePrimitiveNode
        ///         |  |  relatedMaterial                       MultiplePrimitiveNode
        ///         |  |  series                                SimpleCompoundNode
        ///         |  |  |  seriesInformation                  SimplePrimitiveNode
        ///         |  |  |  seriesName                         SimplePrimitiveNode
        ///         |  |  software                              MultipleCompoundNode
        ///         |  |  |  softwareName                       SimplePrimitiveNode
        ///         |  |  |  softwareVersion                    SimplePrimitiveNode
        ///         |  |  subject                               MultipleControlledVocabularyNode
        ///         |  |  subtitle                              SimplePrimitiveNode
        ///         |  |  timePeriodCovered                     MultipleCompoundNode
        ///         |  |  |  timePeriodCoveredStart             SimplePrimitiveNode
        ///         |  |  |  timePeriodCoveredEnd               SimplePrimitiveNode
        ///         |  |  title                                 SimplePrimitiveNode
        ///         |  |  topicClassification                   MultipleCompoundNode
        ///         |  |  |  topicClassValue                    SimplePrimitiveNode
        ///         |  |  |  topicClassVocab                    SimplePrimitiveNode
        ///         |  |  |  topicClassVocabURI                 SimplePrimitiveNode
        ///         |  geospatial
        ///         |  |  conformity                            MultipleCompoundNode
        ///         |  |  |  degree                             SimpleControlledVocabularyNode
        ///         |  |  |  specification                      MultiplePrimitiveNode
        ///         |  |  geographicBoundingBox                 MultipleCompoundNode
        ///         |  |  |  eastLongitude                      SimplePrimitiveNode
        ///         |  |  |  northLongitude                     SimplePrimitiveNode
        ///         |  |  |  southLongitude                     SimplePrimitiveNode
        ///         |  |  |  westLongitude                      SimplePrimitiveNode
        ///         |  |  geographicCoverage                    MultipleCompoundNode
        ///         |  |  |  city                               SimplePrimitiveNode
        ///         |  |  |  country                            SimpleControlledVocabularyNode
        ///         |  |  |  otherGeographicCoverage            SimplePrimitiveNode
        ///         |  |  |  state                              SimplePrimitiveNode
        ///         |  |  geographicUnit                        MultiplePrimitiveNode
        ///         |  |  qualityValidity                       MultipleCompoundNode
        ///         |  |  |  lineage                            MultiplePrimitiveNode
        ///         |  |  |  spatialResolution                  MultiplePrimitiveNode
        ///         |  socialscience
        ///         |  |  actionsToMinimizeLoss                 SimplePrimitiveNode
        ///         |  |  cleaningOperations                    SimplePrimitiveNode
        ///         |  |  collectionMode                        SimpleControlledVocabularyNode
        ///         |  |  collectionModeOther                   SimplePrimitiveNode
        ///         |  |  collectorTraining                     SimplePrimitiveNode
        ///         |  |  controlOperations                     SimplePrimitiveNode
        ///         |  |  dataCollectionSituation               SimplePrimitiveNode
        ///         |  |  dataCollector                         SimplePrimitiveNode
        ///         |  |  datasetLevelErrorNotes                SimplePrimitiveNode
        ///         |  |  deviationsFromSampleDesign            SimplePrimitiveNode
        ///         |  |  frequencyOfDataCollection             SimplePrimitiveNode
        ///         |  |  geographicalReferential               MultipleCompoundNode
        ///         |  |  |  level                              SimplePrimitiveNode
        ///         |  |  |  version                            SimplePrimitiveNode
        ///         |  |  otherDataAppraisal                    SimplePrimitiveNode
        ///         |  |  researchInstrument                    SimplePrimitiveNode
        ///         |  |  responseRate                          SimplePrimitiveNode
        ///         |  |  samplingErrorEstimates                SimplePrimitiveNode
        ///         |  |  samplingProcedure                     SimpleControlledVocabularyNode
        ///         |  |  samplingProcedureOther                SimplePrimitiveNode
        ///         |  |  socialScienceNotes                    SimpleCompoundNode
        ///         |  |  |  socialScienceNotesSubject          SimplePrimitiveNode
        ///         |  |  |  socialScienceNotesText             SimplePrimitiveNode
        ///         |  |  |  socialScienceNotesType             SimplePrimitiveNode
        ///         |  |  targetSampleSize                      SimpleCompoundNode
        ///         |  |  |  targetSampleActualSize             SimplePrimitiveNode
        ///         |  |  |  targetSampleSizeFormula            SimplePrimitiveNode
        ///         |  |  timeMethod                            SimpleControlledVocabularyNode
        ///         |  |  timeMethodOther                       SimplePrimitiveNode
        ///         |  |  unitOfAnalysis                        MultipleControlledVocabularyNode
        ///         |  |  unitOfAnalysisOther                   SimplePrimitiveNode
        ///         |  |  universe                              MultiplePrimitiveNode
        ///         |  |  weighting                             SimplePrimitiveNode
        ///         |  biomedical
        ///         |  |  studyAssayCellType                    MultiplePrimitiveNode
        ///         |  |  studyAssayMeasurementType             MultipleControlledVocabularyNode
        ///         |  |  studyAssayOrganism                    MultipleControlledVocabularyNode
        ///         |  |  studyAssayOtherOrganism               MultiplePrimitiveNode
        ///         |  |  studyAssayOtherMeasurmentType         MultiplePrimitiveNode
        ///         |  |  studyAssayPlatform                    MultipleControlledVocabularyNode
        ///         |  |  studyAssayPlatformOther               MultiplePrimitiveNode
        ///         |  |  studyAssayTechnologyType              MultipleControlledVocabularyNode
        ///         |  |  studyAssayTechnologyTypeOther         MultiplePrimitiveNode
        ///         |  |  studyDesignType                       MultipleControlledVocabularyNode
        ///         |  |  studyDesignTypeOther                  MultiplePrimitiveNode
        ///         |  |  studyFactorType                       MultipleControlledVocabularyNode
        ///         |  |  studyFactorTypeOther                  MultiplePrimitiveNode
        ///         |  |  studySampleType                       MultiplePrimitiveNode
        ///         |  |  studyProtocolType                     MultiplePrimitiveNode
        ///         |  journal
        ///         |  |  journalArticleType                    SimpleControlledVocabularyNode
        ///         |  |  journalVolumeIssue                    MultipleCompoundNode
        ///         |  |  |  journalIssue                       SimplePrimitiveNode
        ///         |  |  |  journalPubDate                     SimplePrimitiveNode
        ///         |  |  |  journalVolume                      SimplePrimitiveNode
        ///         |  Derived-text
        ///         |  |  source                                MultipleCompoundNode
        ///         |  |  |  ageOfSource                        MultiplePrimitiveNode
        ///         |  |  |  citations                          MultiplePrimitiveNode
        ///         |  |  |  experimentNumber                   MultiplePrimitiveNode
        ///         |  |  |  typeOfSource                       MultiplePrimitiveNode
        ///         |  semantics
        ///         |  |  bugDatabase                           SimplePrimitiveNode
        ///         |  |  designedForOntologyTask               MultipleControlledVocabularyNode
        ///         |  |  endpoint                              SimplePrimitiveNode
        ///         |  |  hasFormalityLevel                     SimpleControlledVocabularyNode
        ///         |  |  hasOntologyLanguage                   SimpleControlledVocabularyNode
        ///         |  |  knownUsage                            SimplePrimitiveNode
        ///         |  |  imports                               MultiplePrimitiveNode
        ///         |  |  resourceVersion                       SimpleCompoundNode
        ///         |  |  |  changes                            SimplePrimitiveNode
        ///         |  |  |  modificationDate                   SimplePrimitiveNode
        ///         |  |  |  priorVersion                       MultiplePrimitiveNode
        ///         |  |  |  versionInfo                        SimplePrimitiveNode
        ///         |  |  |  versionStatus                      MultipleControlledVocabularyNode
        ///         |  |  typeOfSR                              SimpleControlledVocabularyNode
        ///         |  |  URI                                   MultiplePrimitiveNode
        ///     </code>
        /// </example>
        public static void ShowMetadataMap()
        {
            Console.Write
            (
                Environment.NewLine + "metadata" + 
                Environment.NewLine + "|  citation" + 
                Environment.NewLine + "|  |  accessToSources                       SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  alternativeTitle                      SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  alternativeURL                        SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  author                                MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  authorAffiliation                  SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  authorIdentifier                   SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  authorIdentifierScheme             SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  |  authorName                         SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  characteristicOfSources               SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  contributor                           MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  contributorAffiliation             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  contributorIdentifier              SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  contributorIdentifierScheme        SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  |  contributorName                    SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  contributorType                    SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  dataOrigin                            MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  datasetContact                        MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  datasetContactAffiliation          SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  datasetContactEmail                SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  datasetContactName                 SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  dataSources                           MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  dateOfCollection                      MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  dateOfCollectionStart              SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  dateOfCollectionEnd                SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  dateOfDeposit                         SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  depositor                             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  distributionDate                      SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  distributor                           MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  distributorAbbreviation            SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  distributorAffiliation             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  distributorLogoURL                 SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  distributorName                    SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  distributorURL                     SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  dsDescription                         MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  dsDescriptionDate                  SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  dsDescriptionValue                 SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  grantNumber                           MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  grantNumberAgency                  SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  grantNumberValue                   SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  keyword                               MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  keywordTermURI                     SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  keywordValue                       SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  keywordVocabulary                  SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  keywordVocabularyURI               SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  kindOfData                            MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  kindOfDataOther                       MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  language                              MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  lifeCycleStep                         MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  notesText                             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  originOfSources                       SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  otherId                               MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  otherIdAgency                      SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  otherIdValue                       SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  otherReferences                       MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  producer                              MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  producerAbbreviation               SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  producerAffiliation                SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  producerLogoURL                    SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  producerName                       SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  producerURL                        SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  productionDate                        SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  productionPlace                       SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  project                               SimpleCompoundNode" + 
                Environment.NewLine + "|  |  |  projectAcronym                     SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  projectId                          SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  projectTask                        SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  projectTitle                       SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  projectURL                         SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  projectWorkPackage                 SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  publication                           MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  publicationCitation                SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  publicationIDNumber                SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  publicationIDType                  SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  |  publicationURL                     SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  relatedDataset                        MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  relatedDatasetCitation             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  relatedDatasetIDNumber             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  relatedDatasetIDType               SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  |  relatedDatasetURL                  SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  relatedMaterial                       MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  series                                SimpleCompoundNode" + 
                Environment.NewLine + "|  |  |  seriesInformation                  SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  seriesName                         SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  software                              MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  softwareName                       SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  softwareVersion                    SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  subject                               MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  subtitle                              SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  timePeriodCovered                     MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  timePeriodCoveredStart             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  timePeriodCoveredEnd               SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  title                                 SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  topicClassification                   MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  topicClassValue                    SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  topicClassVocab                    SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  topicClassVocabURI                 SimplePrimitiveNode" + 
                Environment.NewLine + "|  geospatial" + 
                Environment.NewLine + "|  |  conformity                            MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  degree                             SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  |  specification                      MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  geographicBoundingBox                 MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  eastLongitude                      SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  northLongitude                     SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  southLongitude                     SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  westLongitude                      SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  geographicCoverage                    MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  city                               SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  country                            SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  |  otherGeographicCoverage            SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  state                              SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  geographicUnit                        MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  qualityValidity                       MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  lineage                            MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  spatialResolution                  MultiplePrimitiveNode" + 
                Environment.NewLine + "|  socialscience" + 
                Environment.NewLine + "|  |  actionsToMinimizeLoss                 SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  cleaningOperations                    SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  collectionMode                        SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  collectionModeOther                   SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  collectorTraining                     SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  controlOperations                     SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  dataCollectionSituation               SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  dataCollector                         SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  datasetLevelErrorNotes                SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  deviationsFromSampleDesign            SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  frequencyOfDataCollection             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  geographicalReferential               MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  level                              SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  version                            SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  otherDataAppraisal                    SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  researchInstrument                    SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  responseRate                          SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  samplingErrorEstimates                SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  samplingProcedure                     SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  samplingProcedureOther                SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  socialScienceNotes                    SimpleCompoundNode" + 
                Environment.NewLine + "|  |  |  socialScienceNotesSubject          SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  socialScienceNotesText             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  socialScienceNotesType             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  targetSampleSize                      SimpleCompoundNode" + 
                Environment.NewLine + "|  |  |  targetSampleActualSize             SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  targetSampleSizeFormula            SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  timeMethod                            SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  timeMethodOther                       SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  unitOfAnalysis                        MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  unitOfAnalysisOther                   SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  universe                              MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  weighting                             SimplePrimitiveNode" + 
                Environment.NewLine + "|  biomedical" + 
                Environment.NewLine + "|  |  studyAssayCellType                    MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  studyAssayMeasurementType             MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  studyAssayOrganism                    MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  studyAssayOtherOrganism               MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  studyAssayOtherMeasurmentType         MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  studyAssayPlatform                    MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  studyAssayPlatformOther               MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  studyAssayTechnologyType              MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  studyAssayTechnologyTypeOther         MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  studyDesignType                       MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  studyDesignTypeOther                  MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  studyFactorType                       MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  studyFactorTypeOther                  MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  studySampleType                       MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  studyProtocolType                     MultiplePrimitiveNode" + 
                Environment.NewLine + "|  journal" + 
                Environment.NewLine + "|  |  journalArticleType                    SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  journalVolumeIssue                    MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  journalIssue                       SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  journalPubDate                     SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  journalVolume                      SimplePrimitiveNode" + 
                Environment.NewLine + "|  Derived-text" + 
                Environment.NewLine + "|  |  source                                MultipleCompoundNode" + 
                Environment.NewLine + "|  |  |  ageOfSource                        MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  citations                          MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  experimentNumber                   MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  typeOfSource                       MultiplePrimitiveNode" + 
                Environment.NewLine + "|  semantics" + 
                Environment.NewLine + "|  |  bugDatabase                           SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  designedForOntologyTask               MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  endpoint                              SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  hasFormalityLevel                     SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  hasOntologyLanguage                   SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  knownUsage                            SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  imports                               MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  resourceVersion                       SimpleCompoundNode" + 
                Environment.NewLine + "|  |  |  changes                            SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  modificationDate                   SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  priorVersion                       MultiplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  versionInfo                        SimplePrimitiveNode" + 
                Environment.NewLine + "|  |  |  versionStatus                      MultipleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  typeOfSR                              SimpleControlledVocabularyNode" + 
                Environment.NewLine + "|  |  URI                                   MultiplePrimitiveNode"
            );
        }
    }
}
