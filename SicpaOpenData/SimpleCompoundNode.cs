﻿using Newtonsoft.Json;
using System;
using System.Collections;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Mars 2021</li>
    ///         <li>Description : Classe implémentant les nodes à valeur composée unique</li>
    ///     </ul>
    /// </summary>
    public class SimpleCompoundNode : BaseNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /// <summary>Cette propriété contient la valeur du node</summary>
        /// <value>Hashtable</value>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SimpleCompoundNode scn = new SimpleCompoundNode();
        ///
        ///         Hashtable ht = new Hashtable();
        ///         ht.Add("field1", new SimplePrimitiveNode("field1", "value1"));
        ///         ht.Add("field2", new SimpleControlledVocabularyNode("field2", "value2"));
        ///         ht.Add("field3", new SimplePrimitiveNode("field3", "value3"));
        ///
        ///         scn.TypeName = "name";
        ///         scn.Value = ht;
        ///     </code>
        /// </example>
        public Hashtable Value { get; protected set; }





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /// <summary>Constructeur ne nécessitant pas de paramètres</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SimpleCompoundNode scn = new SimpleCompoundNode();
        ///     </code>
        /// </example>
        public SimpleCompoundNode()
        {
            this.Multiple   = false;
            this.TypeClass  = "compound";
            this.TypeName   = String.Empty;
            this.Value      = new Hashtable();
        }

        /// <summary>Constructeur nécessitant deux paramètres : typeName et value</summary>
        /// <param name="typeName">nom du node</param>
        /// <param name="value" >valeur du node</param>
        /// Voir <see cref="SicpaOpenData.Metadata.SimplePrimitiveNode"/>, 
        ///      <see cref="SicpaOpenData.Metadata.SimpleControlledVocabularyNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultiplePrimitiveNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultipleControlledVocabularyNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable ht = new Hashtable();
        ///         ht.Add("field1", new SimplePrimitiveNode("field1", "value1"));
        ///         ht.Add("field2", new SimpleControlledVocabularyNode("field2", "value2"));
        ///         ht.Add("field3", new SimplePrimitiveNode("field3", "value3"));
        ///         
        ///         SimpleCompoundNode scn = new SimpleCompoundNode("name", ht);
        ///     </code>
        /// </example>
        public SimpleCompoundNode(String typeName, Hashtable value)
        {
            this.Multiple = false;
            this.TypeClass = "compound";
            this.TypeName = typeName;
            this.SetValue(value);
        }





        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /// <summary><c>AreSubnodesValid</c> est une méthode qui permet de s'assurer de la validité de tous les subnodes</summary>
        /// <returns>true si les subnodes sont valides, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(scn.AreSubnodesValid())
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private Boolean AreSubnodesValid()
        {
            foreach (DictionaryEntry de in this.Value)
            {
                BaseNode subnode = (BaseNode)de.Value;

                if (!AuthorizedFields.ListAuthorizedFieldsForCategoryNode(this.TypeName).ContainsKey(subnode.TypeName))
                    return false;

                if (!subnode.GetType().ToString().Contains(AuthorizedFields.GetNodeType(subnode.TypeName).ToString()))
                    return false;
            }

            return true;
        }

        /// <summary><c>IsValid</c> est une méthode qui permet de s'assurer de la validité du node</summary>
        /// <returns>true si le node est valide, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(scn.IsValid())
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        public Boolean IsValid()
        {
            return this.Multiple.GetType()  == typeof(Boolean)      && !this.Multiple
                && this.TypeClass.GetType() == typeof(String)       && this.TypeClass == "compound"
                && this.TypeName.GetType()  == typeof(String)
                && this.Value.GetType()     == typeof(Hashtable)    
                && this.IsValueValid()
                && this.AreSubnodesValid();
        }

        /// <summary>
        ///     <c>IsValueValid</c> est une méthode qui permet de s'assurer de la validité de la valeur <br/>
        ///     => Chaque <c>DictionaryEntry</c> de la <c>Hashtable</c> doit être une instance de <c>SimplePrimitiveNode</c>, <c>SimpleControlledVocabularyNode</c>, <c>MultiplePrimitiveNode</c> ou <c>MultipleControlledVocabularyNode</c>
        /// </summary>
        /// <returns>true si la valeur est valide, false sinon</returns>
        /// Voir <see cref="SicpaOpenData.Metadata.SimplePrimitiveNode"/>, 
        ///      <see cref="SicpaOpenData.Metadata.SimpleControlledVocabularyNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultiplePrimitiveNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultipleControlledVocabularyNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(this.IsValueValid())
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private Boolean IsValueValid()
        {
            Boolean valid = true;
            
            foreach (DictionaryEntry item in this.Value)
            {
                if (item.Value.GetType() == typeof(SimplePrimitiveNode)
                 || item.Value.GetType() == typeof(SimpleControlledVocabularyNode)
                 || item.Value.GetType() == typeof(MultiplePrimitiveNode)
                 || item.Value.GetType() == typeof(MultipleControlledVocabularyNode))
                    continue;
                else
                {
                    valid = false;
                    break;
                }
            }

            return valid;
        }

        /// <summary><c>SetValue</c> est une méthode qui permet de mettre à jour la valeur du node</summary>
        /// <param name="value" >valeur du node</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable ht = new Hashtable();
        ///         ht.Add("field1", new SimplePrimitiveNode("field1", "value1"));
        ///         ht.Add("field2", new SimpleControlledVocabularyNode("field2", "value2"));
        ///         ht.Add("field3", new SimplePrimitiveNode("field3", "value3"));
        ///         
        ///         scn.SetValue(ht);
        ///     </code>
        /// </example>
        public void SetValue(Hashtable value)
        {
            this.Value = value;

            if (!this.IsValueValid())
            {
                this.Value = new Hashtable();
                return;
            }

            if (!this.AreSubnodesValid())
            {
                this.Value = new Hashtable();
                return;
            }
        }

        /// <summary><c>ToJSON</c> est une méthode qui permet d'obtenir une réprésentation JSON du node</summary>
        /// <returns>la représentation du node sous forme de chaine JSON</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String json = scn.ToJSON();
        ///     </code>
        /// </example>
        public String ToJSON()
        {
            if (!this.IsValid())
                return String.Empty;

            return JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }

        /// <summary><c>ToString</c> est une méthode qui permet d'obtenir une réprésentation textuelle du node (JSON minifié)</summary>
        /// <returns>la représentation du node sous forme de chaine JSON minifiée</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String node = scn.ToString();
        ///     </code>
        /// </example>
        override public String ToString()
        {
            if (!this.IsValid())
                return String.Empty;

            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }




        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //





    }
}
