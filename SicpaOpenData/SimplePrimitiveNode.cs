﻿using Newtonsoft.Json;
using System;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Mars 2021</li>
    ///         <li>Description : Classe implémentant les nodes à valeur textuelle unique</li>
    ///     </ul>
    /// </summary>

    public class SimplePrimitiveNode : BaseNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /// <summary>Cette propriété contient la valeur du node</summary>
        /// <value>String</value>
        public String   Value       { get; protected set; }





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /// <summary>Constructeur ne nécessitant pas de paramètres</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SimplePrimitiveNode spn = new SimplePrimitiveNode();
        ///     </code>
        /// </example>
        public SimplePrimitiveNode()
        {
            this.Multiple   = false;
            this.TypeClass  = "primitive";
            this.TypeName = String.Empty;
            this.Value = String.Empty;
        }

        /// <summary>Constructeur nécessitant deux paramètres : typeName et value</summary>
        /// <param name="typeName">nom du node</param>
        /// <param name="value" >valeur du node</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         SimplePrimitiveNode spn = new SimplePrimitiveNode("name", "value");
        ///     </code>
        /// </example>
        public SimplePrimitiveNode(String typeName, String value)
        {
            this.Multiple   = false;
            this.TypeClass  = "primitive";
            this.TypeName   = typeName;

            this.SetValue(value);
        }






        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /// <summary><c>IsValid</c> est une méthode qui permet de s'assurer de la validité du node</summary>
        /// <returns>true si le node est valide, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(spn.IsValid())
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        public Boolean IsValid()
        {
            return this.Multiple.GetType() == typeof(Boolean) && !this.Multiple
                && this.TypeClass.GetType() == typeof(String) && this.TypeClass == "primitive"
                && this.TypeName.GetType() == typeof(String)
                && this.Value.GetType() == typeof(String);
        }

        /// <summary><c>SetValue</c> est une méthode qui permet de mettre à jour la valeur du node</summary>
        /// <param name="value" >valeur du node</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         spn.SetValue("value");
        ///     </code>
        /// </example>
        public void SetValue(String value)
        {
            this.Value = value;
        }

        /// <summary><c>ToJSON</c> est une méthode qui permet d'obtenir une réprésentation JSON du node</summary>
        /// <returns>la représentation du node sous forme de chaine JSON</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String json = spn.ToJSON();
        ///     </code>
        /// </example>
        public String ToJSON()
        {
            if (!this.IsValid())
                return String.Empty;

            return JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }

        /// <summary><c>ToString</c> est une méthode qui permet d'obtenir une réprésentation textuelle du node (JSON minifié)</summary>
        /// <returns>la représentation du node sous forme de chaine JSON minifiée</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String node = spn.ToString();
        ///     </code>
        /// </example>
        override public String ToString()
        {
            if (!this.IsValid())
                return String.Empty;

            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }




        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //





    }
}
