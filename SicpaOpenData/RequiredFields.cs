﻿using System;
using System.Collections;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Mars 2021</li>
    ///         <li>Description : Classe détaillant les valeurs requises par nodes</li>
    ///     </ul>
    /// </summary>
    public static class RequiredFields
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          





        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             





        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //

        /// <summary><c>ListRequiredFields</c> est une méthode qui permet de récupérer la liste de tous les fields requis dans le document des métadonnées</summary>
        /// <returns>la liste des fields requis</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable requiredFields = RequiredFields.ListRequiredFields();
        ///     </code>
        /// </example>
        public static Hashtable ListRequiredFields()
        {
            Hashtable requiredFields = new Hashtable();
            requiredFields = Helper.MergeHashtables(requiredFields, RequiredFields.ListRequiredFieldsForBiomedicalNode());
            requiredFields = Helper.MergeHashtables(requiredFields, RequiredFields.ListRequiredFieldsForCitationNode());
            requiredFields = Helper.MergeHashtables(requiredFields, RequiredFields.ListRequiredFieldsForDerivedTextNode());
            requiredFields = Helper.MergeHashtables(requiredFields, RequiredFields.ListRequiredFieldsForGeospatialNode());
            requiredFields = Helper.MergeHashtables(requiredFields, RequiredFields.ListRequiredFieldsForJournalNode());
            requiredFields = Helper.MergeHashtables(requiredFields, RequiredFields.ListRequiredFieldsForSemanticsNode());
            requiredFields = Helper.MergeHashtables(requiredFields, RequiredFields.ListRequiredFieldsForSocialScienceNode());
            return requiredFields;
        }

        /// <summary><c>ListRequiredFields</c> est une méthode qui permet de récupérer la liste de tous les fields requis pour un node donné</summary>
        /// <param name="node">le node pour lequel lister les champs requis</param>
        /// <returns>la liste des fields requis pour le node passé en paramètre</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable requiredFields = RequiredFields.ListRequiredFields("citation");
        ///     </code>
        /// </example>
        public static Hashtable ListRequiredFields(String node)
        {
            switch (node)
            {
                case "biomedical":
                    return RequiredFields.ListRequiredFieldsForBiomedicalNode();

                case "citation":
                    return RequiredFields.ListRequiredFieldsForCitationNode();

                case "Derived-text":
                    return RequiredFields.ListRequiredFieldsForDerivedTextNode();

                case "geospatial":
                    return RequiredFields.ListRequiredFieldsForGeospatialNode();

                case "journal":
                    return RequiredFields.ListRequiredFieldsForJournalNode();

                case "semantics":
                    return RequiredFields.ListRequiredFieldsForSemanticsNode();

                case "socialscience":
                    return RequiredFields.ListRequiredFieldsForSocialScienceNode();

                default:
                    return RequiredFields.ListRequiredFields();
            }
        }

        /// <summary><c>ListRequiredFieldsForBiomedicalNode</c> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "biomedical"</summary>
        /// <returns>la liste des fields requis</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable requiredFields = RequiredFields.ListRequiredFieldsForBiomedicalNode();
        ///     </code>
        /// </example>
        public static Hashtable ListRequiredFieldsForBiomedicalNode()
        {
            return new Hashtable();
        }

        /// <summary><c>ListRequiredFieldsForCitationNode</c> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "citation"</summary>
        /// <returns>la liste des fields requis</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable requiredFields = RequiredFields.ListRequiredFieldsForCitationNode();
        ///     </code>
        /// </example>
        public static Hashtable ListRequiredFieldsForCitationNode()
        {
            return new Hashtable()
            {
                { "title",                      "SimplePrimitiveNode"               },
                { "author",                     "MultipleCompoundNode"              },
                { "datasetContact",             "MultipleCompoundNode"              },
                { "dsDescription",              "MultipleCompoundNode"              },
                { "subject",                    "MultipleControlledVocabularyNode"  },
                { "kindOfData",                 "MultipleControlledVocabularyNode"  },
            };
        }

        /// <summary><c>ListRequiredFieldsForDerivedTextNode</c> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "Derived-text"</summary>
        /// <returns>la liste des fields requis</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable requiredFields = RequiredFields.ListRequiredFieldsForDerivedTextNode();
        ///     </code>
        /// </example>
        public static Hashtable ListRequiredFieldsForDerivedTextNode()
        {
            return new Hashtable();
        }

        /// <summary><c>ListRequiredFieldsForGeospatialNode</c> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "geospatial"</summary>
        /// <returns>la liste des fields requis</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable requiredFields = RequiredFields.ListRequiredFieldsForGeospatialNode();
        ///     </code>
        /// </example>
        public static Hashtable ListRequiredFieldsForGeospatialNode()
        {
            return new Hashtable();
        }

        /// <summary><c>ListRequiredFieldsForJournalNode</c> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "journal"</summary>
        /// <returns>la liste des fields requis</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable requiredFields = RequiredFields.ListRequiredFieldsForJournalNode();
        ///     </code>
        /// </example>
        public static Hashtable ListRequiredFieldsForJournalNode()
        {
            return new Hashtable();
        }

        /// <summary><c>ListRequiredFieldsForSemanticsNode</c> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "semantics"</summary>
        /// <returns>la liste des fields requis</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable requiredFields = RequiredFields.ListRequiredFieldsForSemanticsNode();
        ///     </code>
        /// </example>
        public static Hashtable ListRequiredFieldsForSemanticsNode()
        {
            return new Hashtable();
        }

        /// <summary><c>ListRequiredFieldsForSocialScienceNode</c> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "socialscience"</summary>
        /// <returns>la liste des fields requis</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable requiredFields = RequiredFields.ListRequiredFieldsForSocialScienceNode();
        ///     </code>
        /// </example>
        public static Hashtable ListRequiredFieldsForSocialScienceNode()
        {
            return new Hashtable();
        }
    }
}
