﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Avril 2021</li>
    ///         <li>Description : Classe implémentant les nodes catégorie</li>
    ///     </ul>
    /// </summary>
    public abstract class CategoryNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /// <summary>Cette propriété indique la valeur à afficher dans le fichier de métadonnées pour ce node</summary>
        /// <value>String</value>
        public String DisplayName { get; protected set; }

        /// <summary>Cette propriété permet de gérer la liste des valeurs pour le node</summary>
        /// <value>ArrayList</value>
        public ArrayList Fields { get; protected set; }




        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /// <summary>Constructeur ne nécessitant pas de paramètres</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         CategoryNode cn = new CategoryNode();
        ///     </code>
        /// </example>
        public CategoryNode()
        {
            this.DisplayName = String.Empty;
            this.Fields = new ArrayList();
        }




        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /// <summary><c>ClearFields</c> est une méthode qui permet de réinitialiser la liste des fields du node</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         cn.ClearFields();
        ///     </code>
        /// </example>
        public void ClearFields()
        {
            this.Fields.Clear();
        }

        /// <summary><c>ContainsField</c> est une méthode qui permet de vérifier si un field existe dans la liste des fields du node</summary>
        /// <param name="fieldName">valeur à vérifier</param>
        /// <returns>true si la valeur appartient à la liste de valeurs du node, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Boolean contains = cn.ContainsFields("value");
        ///     </code>
        /// </example>
        public Boolean ContainsField(String fieldName)
        {
            return this.IndexOfField(fieldName) >= 0;
        }

        /// <summary><c>ContainsField</c> est une méthode qui permet de récupérer la liste des noms des fields du node</summary>
        /// <returns>la liste des noms des fields</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         List<String> fnList = cn.FieldNames();
        ///     </code>
        /// </example>
        public List<String> FieldNames()
        {
            List<String> fieldNames = new List<String>();

            foreach (BaseNode node in this.Fields)
                fieldNames.Add(node.TypeName);

            return fieldNames;
        }

        /// <summary><c>IndexOfField</c> est une méthode qui permet de récupérer l'index d'un field</summary>
        /// <param name="fieldName">valeur à rechercher</param>
        /// <returns>index de la valeur</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Int32 index = cn.IndexOfField("fieldName");
        ///     </code>
        /// </example>
        public Int32 IndexOfField(String fieldName)
        {
            for(Int32 index=0; index < this.Fields.Count; index++)
            {
                BaseNode node = (BaseNode)this.Fields[index];

                if (node.TypeName == fieldName)
                    return index;
            }

            return -1;
        }

        /// <summary><c>InitFields</c> est une méthode qui permet d'initialiser la liste des fields du node</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         cn.InitFields();
        ///     </code>
        /// </example>
        public void InitFields()
        {
            this.Fields = new ArrayList();
        }

        /// <summary><c>RemoveField</c> est une méthode qui permet de supprimer un field de la liste des fields</summary>
        /// <param name="fieldName">field à supprimer</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         cn.RemoveField("fieldName");
        ///     </code>
        /// </example>
        public void RemoveField(String fieldName)
        {
            try
            {
                this.Fields.RemoveAt(this.IndexOfField(fieldName));
            }
            catch
            {
            }
        }

        /// <summary><c>RemoveFieldAt</c> est une méthode qui permet de supprimer le field située à un index précis</summary>
        /// <param name="index">index de la valeur à supprimer</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         cn.RemoveFieldAt(1);
        ///     </code>
        /// </example>
        public void RemoveFieldAt(Int32 index)
        {
            try
            {
                this.Fields.RemoveAt(index);
            }
            catch
            {
            }
        }

        /// <summary><c>RemoveAllFields</c> est une méthode qui permet de supprimer tous les fields de la liste des fields</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         cn.RemoveAllFields();
        ///     </code>
        /// </example>
        public void RemoveAllFields()
        {
            this.Fields.Clear();
        }





        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //



    }
}