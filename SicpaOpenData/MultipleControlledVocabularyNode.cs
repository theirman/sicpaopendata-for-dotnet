﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Avril 2021</li>
    ///         <li>Description : Classe implémentant les nodes à valeurs textuelles multiples, valeurs contraintes par un vocabulaire contrôlé</li>
    ///     </ul>
    /// </summary>
    public class MultipleControlledVocabularyNode : BaseNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /// <summary>Cette propriété contient la valeur du node</summary>
        /// <value>List<String></value>
        public List<String> Value { get; protected set; }





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /// <summary>Constructeur ne nécessitant pas de paramètres</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         MultipleControlledVocabularyNode mcvn = new MultipleControlledVocabularyNode();
        ///     </code>
        /// </example>
        public MultipleControlledVocabularyNode()
        {
            this.Multiple           = true;
            this.TypeClass          = "controlledVocabulary";
            this.TypeName           = String.Empty;
            this.Value              = new List<String>();
        }

        /// <summary>Constructeur nécessitant deux paramètres : typeName et values</summary>
        /// <param name="typeName">nom du node</param>
        /// <param name="values" >valeur du node</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         List<String> values = new List<String>();
        ///         values.Add("controlledValue1");
        ///         values.Add("controlledValue2");
        ///         values.Add("controlledValue3");
        ///
        ///         MultipleControlledVocabularyNode mcvn = new MultipleControlledVocabularyNode("name", values);
        ///     </code>
        /// </example>
        public MultipleControlledVocabularyNode(String typeName, List<String> values)
        {
            this.Multiple           = true;
            this.TypeClass          = "controlledVocabulary";
            this.TypeName           = typeName;
            this.Value              = new List<String>();

            this.AddValues(values);
        }





        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /// <summary><c>AddValue</c> est une méthode qui permet d'ajouter une valeur à la liste des valeurs du node</summary>
        /// <param name="value">valeur à ajouter</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         mcvn.AddValue("controlledValue");
        ///     </code>
        /// </example>
        public void AddValue(String value)
        {
            if (!this.IsAuthorizedValue(value))
                return;

            this.Value.Add(value);
        }

        /// <summary><c>AddValues</c> est une méthode qui permet d'ajouter une liste de valeurs à la liste des valeurs du node</summary>
        /// <param name="values">valeur à ajouter</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         List<String> values = new List<String>();
        ///         values.Add("controlledValue1");
        ///         values.Add("controlledValue2");
        ///         values.Add("controlledValue3");
        ///     
        ///         mcvn.AddValues(values);
        ///     </code>
        /// </example>
        public void AddValues(List<String> values)
        {
            foreach (String value in values)
                this.AddValue(value);
        }

        /// <summary><c>ClearValues</c> est une méthode qui permet de réinitialiser la liste des valeurs du node</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         mcvn.ClearValues();
        ///     </code>
        /// </example>
        public void ClearValues()
        {
            this.Value.Clear();
        }

        /// <summary><c>ContainsValue</c> est une méthode qui permet de vérifier si une valeur existe dans la liste des valeurs du node</summary>
        /// <param name="value">valeur à vérifier</param>
        /// <returns>true si la valeur appartient à la liste de valeurs du node, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Boolean contains = mcvn.ContainsValue("controlledValue");
        ///     </code>
        /// </example>
        public Boolean ContainsValue(String value)
        {
            return this.Value.Contains(value);
        }

        /// <summary><c>GetValueAt</c> est une méthode qui permet de récupérer la valeur située à un index précis</summary>
        /// <param name="index">index de la valeur à récupérer</param>
        /// <returns>valeur situé à l'index</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String controlledValue = mcvn.GetValueAt(2);
        ///     </code>
        /// </example>
        public String GetValueAt(Int32 index)
        {
            try
            {
                return this.Value[index].ToString();
            }
            catch
            {
                return String.Empty;
            }
        }

        /// <summary><c>IndexOf</c> est une méthode qui permet de récupérer l'index d'une valeur</summary>
        /// <param name="value">valeur à rechercher</param>
        /// <returns>index de la valeur</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Int32 index = mcvn.IndexOf("controlledValue");
        ///     </code>
        /// </example>
        public Int32 IndexOf(String value)
        {
            return this.Value.IndexOf(value);
        }

        /// <summary><c>IsAuthorizedValue</c> est une méthode qui permet de s'assurer que la valeur en paramètre est bien une valeur autorisée par le vocabulaire contrôlé</summary>
        /// <param name="value">valeur à tester</param>
        /// <returns>true si le node est valide, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(mcvn.IsAuthorizedValue("value"))
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        public Boolean IsAuthorizedValue(String value)
        {
            ArrayList controlledVocabulary = ControlledVocabulary.ListValuesFor(this.TypeName);
            return controlledVocabulary.Contains(value);
        }

        /// <summary><c>IsValid</c> est une méthode qui permet de s'assurer de la validité du node</summary>
        /// <returns>true si le node est valide, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(mcvn.IsValid())
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        public Boolean IsValid()
        {
            return this.Multiple.GetType()  == typeof(Boolean)      && this.Multiple
                && this.TypeClass.GetType() == typeof(String)       && this.TypeClass == "controlledVocabulary"
                && this.TypeName.GetType()  == typeof(String)
                && this.Value.GetType()     == typeof(List<String>) && this.IsValueValid();
        }

        /// <summary>
        ///     <c>IsValueValid</c> est une méthode qui permet de s'assurer de la validité de la valeur <br/>
        ///     => Chaque élément de la liste de valeurs du node doit être une chaîne de caractères
        /// </summary>
        /// <returns>true si le node est valide, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(this.IsValueValid())
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private Boolean IsValueValid()
        {
            foreach (String item in this.Value)
            {
                if (item.GetType() != typeof(String))
                    return false;

                if (!this.IsAuthorizedValue(item))
                    return false;
            }

            return true;
        }

        /// <summary><c>RemoveAll</c> est une méthode qui permet de supprimer toutes les valeurs de la liste des valeurs</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         mcvn.RemoveAll();
        ///     </code>
        /// </example>
        public void RemoveAll()
        {
            this.ClearValues();
        }

        /// <summary><c>RemoveValue</c> est une méthode qui permet de supprimer une valeur de la liste des valeurs</summary>
        /// <param name="value">valeur à rechercher</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         mcvn.RemoveValue("controlledValue");
        ///     </code>
        /// </example>
        public void RemoveValue(String value)
        {
            try
            {
                this.Value.Remove(value);
            }
            catch
            {
            }
        }

        /// <summary><c>RemoveValueAt</c> est une méthode qui permet de supprimer la valeur située à un index précis</summary>
        /// <param name="index">index de la valeur à supprimer</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         mcvn.RemoveValueAt(1);
        ///     </code>
        /// </example>
        public void RemoveValueAt(Int32 index)
        {
            try
            {
                this.Value.RemoveAt(index);
            }
            catch
            {
            }
        }

        /// <summary><c>ToJSON</c> est une méthode qui permet d'obtenir une réprésentation JSON du node</summary>
        /// <returns>la représentation du node sous forme de chaine JSON</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String json = spn.ToJSON();
        ///     </code>
        /// </example>
        public String ToJSON()
        {
            if (!this.IsValid())
                return String.Empty;

            return JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }

        /// <summary><c>ToString</c> est une méthode qui permet d'obtenir une réprésentation textuelle du node (JSON minifié)</summary>
        /// <returns>la représentation du node sous forme de chaine JSON minifiée</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String node = mcvn.ToString();
        ///     </code>
        /// </example>
        override public String ToString()
        {
            if (!this.IsValid())
                return String.Empty;

            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }




        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //





    }
}

