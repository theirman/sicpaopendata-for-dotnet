﻿using Newtonsoft.Json;
using System;
using System.Collections;



namespace SicpaOpenData.Metadata
{
    /// <summary>
    ///     <ul>
    ///         <li>Auteur      : Thierry HEIRMAN</li>
    ///         <li>Date        : Avril 2021</li>
    ///         <li>Description : Classe implémentant les nodes à valeurs composées multiples</li>
    ///     </ul>
    /// </summary>
    public class MultipleCompoundNode : BaseNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /// <summary>Cette propriété contient la valeur du node</summary>
        /// <value>ArrayList</value>
        public ArrayList Value { get; protected set; }





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /// <summary>Constructeur ne nécessitant pas de paramètres</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         MultipleCompoundNode mcn = new MultipleCompoundNode();
        ///     </code>
        /// </example>
        public MultipleCompoundNode()
        {
            this.Multiple   = true;
            this.TypeClass  = "compound";
            this.TypeName   = String.Empty;
            this.Value      = new ArrayList();
        }

        /// <summary>Constructeur nécessitant deux paramètres : typeName et values</summary>
        /// <param name="typeName">nom du node</param>
        /// <param name="values">valeur du node</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable value1 = new Hashtable();
        ///         value1.Add("name11", new SimplePrimitiveNode("name11", "value11"));
        ///         value1.Add("name12", new SimplePrimitiveNode("name12", "value12"));
        /// 
        ///         Hashtable value2 = new Hashtable();
        ///         value2.Add("name21", new SimplePrimitiveNode("name21", "value21"));
        ///         value2.Add("name22", new SimplePrimitiveNode("name22", "value22"));
        /// 
        ///         MultipleCompoundNode mcn = new MultipleCompoundNode("name", new ArrayList() { value1, value2 });
        ///     </code>
        /// </example>
        public MultipleCompoundNode(String typeName, ArrayList values)
        {
            this.Multiple   = true;
            this.TypeClass  = "compound";
            this.TypeName   = typeName;
            this.Value      = new ArrayList();

            this.AddValues(values);
        }





        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /// <summary><c>AddValue</c> est une méthode qui permet d'ajouter une valeur à la liste des valeurs du node</summary>
        /// <param name="value">valeur à ajouter</param>
        /// Voir <see cref="SicpaOpenData.Metadata.SimplePrimitiveNode"/>, 
        ///      <see cref="SicpaOpenData.Metadata.SimpleControlledVocabularyNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultiplePrimitiveNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultipleControlledVocabularyNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable value = new Hashtable();
        ///         value.Add("name1", new SimplePrimitiveNode("name1", "value1"));
        ///         value.Add("name2", new SimplePrimitiveNode("name2", "value2"));
        /// 
        ///         mcn.AddValue(value);
        ///     </code>
        /// </example>
        public void AddValue(Hashtable value)
        {
            if(!this.IsValueValid(value))
                return;

            if (!MultipleCompoundNode.AreSubnodesValid(this.TypeName, value))
                return;

            this.Value.Add(value);
        }

        /// <summary><c>AddValues</c> est une méthode qui permet d'ajouter une liste de valeurs à la liste des valeurs du node</summary>
        /// <param name="values">valeur à ajouter</param>
        /// Voir <see cref="SicpaOpenData.Metadata.SimplePrimitiveNode"/>, 
        ///      <see cref="SicpaOpenData.Metadata.SimpleControlledVocabularyNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultiplePrimitiveNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultipleControlledVocabularyNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Hashtable value1 = new Hashtable();
        ///         value1.Add("name11", new SimplePrimitiveNode("name11", "value11"));
        ///         value1.Add("name12", new SimplePrimitiveNode("name12", "value12"));
        /// 
        ///         Hashtable value2 = new Hashtable();
        ///         value2.Add("name21", new SimplePrimitiveNode("name21", "value21"));
        ///         value2.Add("name22", new SimplePrimitiveNode("name22", "value22"));
        ///         
        ///         ArrayList values = new ArrayList() { value1, value2 };
        ///     
        ///         mcn.AddValues(values);
        ///     </code>
        /// </example>
        public void AddValues(ArrayList values)
        {
            foreach (Hashtable value in values)
            {
                if (value.GetType() != typeof(Hashtable))
                    continue;

                this.AddValue(value);
            }
        }

        /// <summary><c>AreSubnodesValid</c> est une méthode qui permet de vérifier que les subnodes sont valides</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if( this.AreSubnodesValide() )
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private Boolean AreSubnodesValid()
        {
            foreach (Hashtable value in this.Value)
            {
                foreach (DictionaryEntry de in value)
                {
                    BaseNode subnode = (BaseNode)de.Value;

                    if (!AuthorizedFields.ListAuthorizedFieldsForCategoryNode(this.TypeName).ContainsKey(subnode.TypeName))
                        return false;

                    if (!subnode.GetType().ToString().Contains(AuthorizedFields.GetNodeType(subnode.TypeName).ToString()))
                        return false;
                }
            }

            return true;
        }

        /// <summary><c>ClearValues</c> est une méthode qui permet de réinitialiser la liste des valeurs du node</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         mcn.ClearValues();
        ///     </code>
        /// </example>
        public void ClearValues()
        {
            this.Value.Clear();
        }

        /// <summary><c>ContainsValue</c> est une méthode qui permet de vérifier si une valeur existe dans la liste des valeurs du node</summary>
        /// <param name="value">valeur à vérifier</param>
        /// <returns>true si la valeur appartient à la liste de valeurs du node, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Boolean contains = mcn.ContainsValue(value);
        ///     </code>
        /// </example>
        public Boolean ContainsValue(Hashtable value)
        {
            return this.IndexOf(value) >= 0;
        }

        /// <summary><c>GetValueAt</c> est une méthode qui permet de récupérer la valeur située à un index précis</summary>
        /// <param name="index">index de la valeur à récupérer</param>
        /// <returns>valeur situé à l'index</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String value = mcn.GetValueAt(2);
        ///     </code>
        /// </example>
        public Hashtable GetValueAt(Int32 index)
        {
            try
            {
                return (Hashtable)this.Value[index];
            }
            catch
            {
                return null;
            }
        }

        /// <summary><c>IndexOf</c> est une méthode qui permet de récupérer l'index d'une valeur</summary>
        /// <param name="value">valeur à rechercher</param>
        /// <returns>index de la valeur</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         Int32 index = mcn.IndexOf(value);
        ///     </code>
        /// </example>
        public Int32 IndexOf(Hashtable value)
        {
            foreach (Hashtable item in this.Value)
            {
                Boolean contains = true;
                
                foreach (DictionaryEntry de in item)
                {
                    if (!value.ContainsKey(de.Key))
                        contains = false;

                    if (de.Value.GetType() != value[de.Key].GetType())
                        contains = false;

                    if (de.Value.GetType() == typeof(SimplePrimitiveNode))
                        if (!MultipleCompoundNode.IsEquals((SimplePrimitiveNode)de.Value, (SimplePrimitiveNode)value[de.Key]))
                            contains = false;

                    if (de.Value.GetType() == typeof(SimpleControlledVocabularyNode))
                        if (!MultipleCompoundNode.IsEquals((SimpleControlledVocabularyNode)de.Value, (SimpleControlledVocabularyNode)value[de.Key]))
                            contains = false;

                    if (de.Value.GetType() == typeof(MultiplePrimitiveNode))
                        if (!MultipleCompoundNode.IsEquals((MultiplePrimitiveNode)de.Value, (MultiplePrimitiveNode)value[de.Key]))
                            contains = false;

                    if (de.Value.GetType() == typeof(MultipleControlledVocabularyNode))
                        if (!MultipleCompoundNode.IsEquals((MultipleControlledVocabularyNode)de.Value, (MultipleControlledVocabularyNode)value[de.Key]))
                            contains = false;
                }

                if (contains)
                    return (Int32)this.Value.IndexOf(item);
            }

            return -1;
        }

        /// <summary><c>IsValid</c> est une méthode qui permet de s'assurer de la validité du node</summary>
        /// <returns>true si le node est valide, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(mcn.IsValid())
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        public Boolean IsValid()
        {
            return this.Multiple.GetType()  == typeof(Boolean)      && this.Multiple
                && this.TypeClass.GetType() == typeof(String)       && this.TypeClass == "compound"
                && this.TypeName.GetType()  == typeof(String)
                && this.Value.GetType()     == typeof(ArrayList)
                && this.IsValueValid()
                && this.AreSubnodesValid();
        }

        /// <summary>
        ///     <c>IsValueValid</c> est une méthode qui permet de s'assurer de la validité de la valeur du node<br/>
        ///     => Chaque <c>DictionaryEntry</c> de la <c>Hashtable</c> doit être une instance de <c>SimplePrimitiveNode</c>, <c>SimpleControlledVocabularyNode</c>, <c>MultiplePrimitiveNode</c> ou <c>MultipleControlledVocabularyNode</c>
        /// </summary>
        /// <returns>true si la valeur est valide, false sinon</returns>
        /// Voir <see cref="SicpaOpenData.Metadata.SimplePrimitiveNode"/>, 
        ///      <see cref="SicpaOpenData.Metadata.SimpleControlledVocabularyNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultiplePrimitiveNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultipleControlledVocabularyNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(this.IsValueValid())
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private Boolean IsValueValid()
        {
            foreach (Hashtable value in this.Value)
            {
                foreach (DictionaryEntry item in value)
                {
                    if (item.Value.GetType() != typeof(SimplePrimitiveNode)
                     && item.Value.GetType() != typeof(SimpleControlledVocabularyNode)
                     && item.Value.GetType() != typeof(MultiplePrimitiveNode)
                     && item.Value.GetType() != typeof(MultipleControlledVocabularyNode))
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     <c>IsValueValid</c> est une méthode qui permet de s'assurer de la validité d'une valeur<br/>
        ///     => Chaque <c>DictionaryEntry</c> de la <c>Hashtable</c> doit être une instance de <c>SimplePrimitiveNode</c>, <c>SimpleControlledVocabularyNode</c>, <c>MultiplePrimitiveNode</c> ou <c>MultipleControlledVocabularyNode</c>
        /// </summary>
        /// <param name="value">valeur à tester</param>
        /// <returns>true si la valeur est valide, false sinon</returns>
        /// Voir <see cref="SicpaOpenData.Metadata.SimplePrimitiveNode"/>, 
        ///      <see cref="SicpaOpenData.Metadata.SimpleControlledVocabularyNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultiplePrimitiveNode"/>,
        ///      <see cref="SicpaOpenData.Metadata.MultipleControlledVocabularyNode"/>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(this.IsValueValid(value))
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private Boolean IsValueValid(Hashtable value)
        {
            foreach (DictionaryEntry item in value)
            {
                if (item.Value.GetType() != typeof(SimplePrimitiveNode)
                    && item.Value.GetType() != typeof(SimpleControlledVocabularyNode)
                    && item.Value.GetType() != typeof(MultiplePrimitiveNode)
                    && item.Value.GetType() != typeof(MultipleControlledVocabularyNode))
                    return false;
            }

            return true;
        }

        /// <summary><c>RemoveAll</c> est une méthode qui permet de supprimer toutes les valeurs de la liste des valeurs</summary>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         mcn.RemoveAll();
        ///     </code>
        /// </example>
        public void RemoveAll()
        {
            this.ClearValues();
        }

        /// <summary><c>RemoveValue</c> est une méthode qui permet de supprimer une valeur de la liste des valeurs</summary>
        /// <param name="value">valeur à supprimer</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         mcn.RemoveValue(value);
        ///     </code>
        /// </example>
        public void RemoveValue(Hashtable value)
        {
            try
            {
                this.Value.RemoveAt( this.IndexOf(value) );
            }
            catch
            {
            }
        }

        /// <summary><c>RemoveValueAt</c> est une méthode qui permet de supprimer la valeur située à un index précis</summary>
        /// <param name="index">index de la valeur à supprimer</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         mcn.RemoveValueAt(1);
        ///     </code>
        /// </example>
        public void RemoveValueAt(Int32 index)
        {
            try
            {
                this.Value.RemoveAt(index);
            }
            catch
            {
            }
        }

        /// <summary><c>ToJSON</c> est une méthode qui permet d'obtenir une réprésentation JSON du node</summary>
        /// <returns>la représentation du node sous forme de chaine JSON</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String json = mcn.ToJSON();
        ///     </code>
        /// </example>
        public String ToJSON()
        {
            if (!this.IsValid())
                return String.Empty;

            return JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }

        /// <summary><c>ToString</c> est une méthode qui permet d'obtenir une réprésentation textuelle du node (JSON minifié)</summary>
        /// <returns>la représentation du node sous forme de chaine JSON minifiée</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         String node = mcn.ToString();
        ///     </code>
        /// </example>
        override public String ToString()
        {
            if (!this.IsValid())
                return String.Empty;

            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }





        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //

        /// <summary><c>AreSubnodesValid</c> est une méthode qui permet de vérifier que les subnodes sont valides</summary>
        /// <param name="nodeName">le nom du noeud pour lequel vérifier les subnodes</param>
        /// <param name="value">une hashtable contenant les subnodes à vérifier</param>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if( MultipleCompoundNode.AreSubnodesValide("name", hashtableSubnodes) )
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private static Boolean AreSubnodesValid(String nodeName, Hashtable value)
        {
            foreach (DictionaryEntry de in value)
            {
                BaseNode subnode = (BaseNode)de.Value;

                if (!AuthorizedFields.ListAuthorizedFieldsForCategoryNode(nodeName).ContainsKey(subnode.TypeName))
                    return false;

                if (!subnode.GetType().ToString().Contains(AuthorizedFields.GetNodeType(subnode.TypeName).ToString()))
                    return false;
            }

            return true;
        }

        /// <summary><c>IsEquals</c> est une méthode qui permet de vérifier la similarité de deux SimpleControlledVocabularyNodes</summary>
        /// <param name="node1">premier node à tester</param>
        /// <param name="node2">second node à tester</param>
        /// <returns>true si les deux nodes sont similaires, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(MultipleCompoundNode.IsEquals(node1, node2))
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private static Boolean IsEquals(SimpleControlledVocabularyNode node1, SimpleControlledVocabularyNode node2)
        {
            if (node1.GetType() != node2.GetType())
                return false;

            if (node1.Multiple != node2.Multiple)
                return false;

            if (node1.TypeClass != node2.TypeClass)
                return false;

            if (node1.TypeName != node2.TypeName)
                return false;

            if (node1.Value != node2.Value)
                return false;

            return true;
        }

        /// <summary><c>IsEquals</c> est une méthode qui permet de vérifier la similarité de deux SimplePrimitiveNodes</summary>
        /// <param name="node1">premier node à tester</param>
        /// <param name="node2">second node à tester</param>
        /// <returns>true si les deux nodes sont similaires, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(MultipleCompoundNode.IsEquals(node1, node2))
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private static Boolean IsEquals(SimplePrimitiveNode node1, SimplePrimitiveNode node2)
        {
            if (node1.GetType() != node2.GetType())
                return false;

            if (node1.Multiple != node2.Multiple)
                return false;

            if (node1.TypeClass != node2.TypeClass)
                return false;

            if (node1.TypeName != node2.TypeName)
                return false;

            if (node1.Value != node2.Value)
                return false;

            return true;
        }

        /// <summary><c>IsEquals</c> est une méthode qui permet de vérifier la similarité de deux MultiplePrimitiveNodes</summary>
        /// <param name="node1">premier node à tester</param>
        /// <param name="node2">second node à tester</param>
        /// <returns>true si les deux nodes sont similaires, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(MultipleCompoundNode.IsEquals(node1, node2))
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private static Boolean IsEquals(MultiplePrimitiveNode node1, MultiplePrimitiveNode node2)
        {
            if (node1.GetType() != node2.GetType())
                return false;

            if (node1.Multiple != node2.Multiple)
                return false;

            if (node1.TypeClass != node2.TypeClass)
                return false;

            if (node1.TypeName != node2.TypeName)
                return false;

            if (node1.Value.Count != node2.Value.Count)
                return false;

            foreach (String item in node1.Value)
                if (!node2.Value.Contains(item))
                    return false;

            foreach (String item in node2.Value)
                if (!node1.Value.Contains(item))
                    return false;

            return true;
        }

        /// <summary><c>IsEquals</c> est une méthode qui permet de vérifier la similarité de deux MultipleControlledVocabularyNodes</summary>
        /// <param name="node1">premier node à tester</param>
        /// <param name="node2">second node à tester</param>
        /// <returns>true si les deux nodes sont similaires, false sinon</returns>
        /// <example>
        ///     <strong>Exemple de code : </strong>
        ///     <code>
        ///         if(MultipleCompoundNode.IsEquals(node1, node2))
        ///         {
        ///             ...
        ///         }
        ///     </code>
        /// </example>
        private static Boolean IsEquals(MultipleControlledVocabularyNode node1, MultipleControlledVocabularyNode node2)
        {
            if (node1.GetType() != node2.GetType())
                return false;

            if (node1.Multiple != node2.Multiple)
                return false;

            if (node1.TypeClass != node2.TypeClass)
                return false;

            if (node1.TypeName != node2.TypeName)
                return false;

            if (node1.Value.Count != node2.Value.Count)
                return false;

            foreach (String item in node1.Value)
                if (!node2.Value.Contains(item))
                    return false;

            foreach (String item in node2.Value)
                if (!node1.Value.Contains(item))
                    return false;

            return true;
        }
    }
}
